<?php

use phpDocumentor\Reflection\Types\This;

defined('BASEPATH') or exit('No direct script access allowed');

class Models extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function insert($table, $data)
	{
		$res = $this->db->insert($table, $data);
		return $res;
	}

	public function update($table, $data, $where)
	{
		$res = $this->db->update($table, $data, $where);
		return $res;
	}

	public function delete($table, $where)
	{
		$res = $this->db->delete($table, $where);
		return $res;
	}

	public function cekkaryawan($id)
	{
		$this->db->where('idkartu', $id);
		$hasil = $this->db->get('karyawan');
		return $hasil->result();
	}

	public function cekabsenkaryawan($id, $tgl)
	{
		$this->db->where('idkaryawan', $id);
		$this->db->where('tgl', $tgl);
		$hasil = $this->db->get('absen');
		return $hasil->result();
	}

	public function countpelanggan()
	{
		$this->db->select('Count(idpelanggan) as jum');
		$hasil = $this->db->get('pelanggan');
		return $hasil->result();
	}

	public function sumtagihan()
	{
		$this->db->select('Sum(subharga) as sumtagihan');
		// $this->db->where('bulan', date('m'));
		$this->db->where('tahun', date('Y'));
		$hasil = $this->db->get('transaksi');
		return $hasil->result();
	}

	public function sumbayar()
	{
		$this->db->select('Sum(subharga) sumbayar');
		$this->db->where('tahun', date('Y'));
		// $this->db->where('bulan', date('m'));
		$this->db->where('tglpembayaran <>',  null);
		$hasil = $this->db->get('transaksi');
		return $hasil->result();
	}

	public function sumkurangbayar()
	{
		$this->db->select('Sum(subharga) sumkurangbayar');
		$this->db->where('tahun', date('Y'));
		// $this->db->where('bulan', date('m'));
		$this->db->where('tglpembayaran', null);
		$hasil = $this->db->get('transaksi');
		return $hasil->result();
	}

	public function grafik()
	{
		$hasil = $this->db->get('vw_grafik');
		return $hasil->result();
	}

	public function area()
	{
		$hasil = $this->db->get('area');
		return $hasil->result();
	}

	public function edit_area($id)
	{
		$this->db->where('idarea', $id);
		$hasil = $this->db->get('area');
		return $hasil->result();
	}

	public function paket()
	{
		$hasil = $this->db->get('paketharga');
		return $hasil->result();
	}

	public function paketid($id)
	{
		$this->db->where('idpaket', $id);
		$hasil = $this->db->get('paketharga');
		return $hasil->result();
	}

	public function edit_paket($id)
	{
		$this->db->where('idpaket', $id);
		$hasil = $this->db->get('paketharga');
		return $hasil->result();
	}

	public function pelanggan()
	{
		$hasil = $this->db->get('vw_pelanggan');
		return $hasil->result();
	}

	public function pelanggantag()
	{
		$this->db->where('berhenti', null);
		$hasil = $this->db->get('vw_pelanggan');
		return $hasil->result();
	}

	public function pelangganuser()
	{
		$this->db->where('level', '3');
		$hasil = $this->db->get('vw_users');
		return $hasil->result();
	}

	public function edit_pelanggan_user($id)
	{
		$this->db->where('idusers', $id);
		$hasil = $this->db->get('vw_users');
		return $hasil->result();
	}

	public function pelanggantagihan($id)
	{
		$this->db->where('idpelanggan', $id);
		$hasil = $this->db->get('vw_pelanggan');
		return $hasil->result();
	}
	public function pelangganid($id)
	{
		$this->db->where('idarea', $id);
		$hasil = $this->db->get('vw_pelanggan');
		return $hasil->result();
	}

	public function edit_pelanggan($id)
	{
		$this->db->where('idpelanggan', $id);
		$hasil = $this->db->get('pelanggan');
		return $hasil->result();
	}

	public function pengguna()
	{
		$this->db->where('level <>', '3');
		$hasil = $this->db->get('vw_users');
		return $hasil->result();
	}

	public function edit_pengguna($id)
	{
		$this->db->where('idusers', $id);
		$hasil = $this->db->get('vw_users');
		return $hasil->result();
	}

	public function level()
	{
		$hasil = $this->db->get('level');
		return $hasil->result();
	}

	public function tagihan()
	{
		$hasil = $this->db->get('vw_tagihan');
		return $hasil->result();
	}

	public function edit_tagihan($id)
	{
		$this->db->where('idtransaksi', $id);
		$hasil = $this->db->get('vw_tagihan');
		return $hasil->result();
	}

	public function cetak_tagihan($start, $end)
	{
		$this->db->where('tglpenetapan >=', $start);
		$this->db->where('tglpenetapan <=', $end);
		$hasil = $this->db->get('vw_tagihan');
		return $hasil->result();
	}

	public function cetak_pembayaran($start, $end)
	{
		$this->db->where('tglpembayaran >=', $start);
		$this->db->where('tglpembayaran <=', $end);
		$hasil = $this->db->get('vw_tagihan');
		return $hasil->result();
	}

	public function cetak_blmbayar($tahun, $bulan)
	{
		$this->db->where('tahun', $tahun);
		$this->db->where('bulan', $bulan);
		$this->db->where('tglpembayaran', null);
		$hasil = $this->db->get('vw_tagihan');
		return $hasil->result();
	}

	public function cetak_blmbayar_semua($tahun)
	{
		$this->db->where('tahun', $tahun);
		// $this->db->where('bulan', $bulan);
		$this->db->where('tglpembayaran', null);
		$hasil = $this->db->get('vw_tagihan');
		return $hasil->result();
	}

	public function sum_blmbayar($tahun, $bulan)
	{
		$this->db->select('sum(subharga) as jumlah');
		$this->db->where('tahun', $tahun);
		$this->db->where('bulan', $bulan);
		$this->db->where('tglpembayaran', null);
		$hasil = $this->db->get('vw_tagihan');
		return $hasil->result();
	}

	public function sum_blmbayar_semua($tahun)
	{
		$this->db->select('sum(subharga) as jumlah');
		$this->db->where('tahun', $tahun);
		// $this->db->where('bulan', $bulan);
		$this->db->where('tglpembayaran', null);
		$hasil = $this->db->get('vw_tagihan');
		return $hasil->result();
	}
	public function viewdatapelanggan($id)
	{
		$this->db->where('idpelanggan', $id);
		$hasil = $this->db->get('vw_tagihan');
		return $hasil->result();
	}

	public function karyawan()
	{
		$hasil = $this->db->get('vw_karyawan');
		return $hasil->result();
	}

	public function editkaryawan($id)
	{
		$this->db->where('idkaryawan', $id);
		$hasil = $this->db->get('vw_karyawan');
		return $hasil->result();
	}
	public function absenkaryawan()
	{
		$this->db->where('jeniskaryawan <>', '2');
		$hasil = $this->db->get('vw_karyawan');
		return $hasil->result();
	}

	public function pdfabsen($bulan, $tahun)
	{
		$this->db->where('bulan', $bulan);
		$this->db->where('tahun', $tahun);
		$hasil = $this->db->get('vw_count_absen_pecah');
		return $hasil->result();
	}

	public function getsales()
	{
		$hasil = $this->db->get('vw_sales');
		return $hasil->result();
	}

	public function getkrywn($id)
	{
		$this->db->where('idkaryawan', $id);
		$hasil = $this->db->get('vw_karyawan');
		return $hasil->result_array();
	}

	public function getkaryawan($id)
	{
		$this->db->where('jeniskaryawan', $id);
		$hasil = $this->db->get('vw_karyawan');
		return $hasil->result();
	}

	public function getfee($id)
	{
		$this->db->where('idkaryawan', $id);
		$hasil = $this->db->get('vw_fee');
		return $hasil->result();
	}

	public function jabatan()
	{
		$hasil = $this->db->get('jabatan');
		return $hasil->result();
	}

	public function pendidikan()
	{
		$hasil = $this->db->get('pendidikan');
		return $hasil->result();
	}

	public function jenis()
	{
		$hasil = $this->db->get('jenis');
		return $hasil->result();
	}

	public function gajian()
	{
		$hasil = $this->db->get('vw_gajian');
		return $hasil->result();
	}

	public function editgajian($id)
	{
		$this->db->where('idgaji', $id);
		$hasil = $this->db->get('vw_gajian');
		return $hasil->result();
	}

	public function gajianglobal($start, $end)
	{
		$this->db->where('tahun', $start);
		$this->db->where('bulan', $end);
		$hasil = $this->db->get('vw_gajian');
		return $hasil->result();
	}

	public function barang()
	{
		$hasil = $this->db->get('barang');
		return $hasil->result();
	}

	public function edit_barang($id)
	{
		$this->db->where('idbarang', $id);
		$hasil = $this->db->get('barang');
		return $hasil->result();
	}

	public function pel()
	{
		// $this->db->where('idpaketharga', '6');
		$hasil = $this->db->get('vw_pelanggan');
		return $hasil->result();
	}

	public function vwbarang($id)
	{
		$this->db->where('idpelanggan', $id);
		$hasil = $this->db->get('vw_barang');
		return $hasil->result();
	}

	public function chartbrg()
	{
		$hasil = $this->db->get('vw_selisih');
		return $hasil->result();
	}

	public function multiSave($table, $data)
	{
		$jumlah = count($data);

		if ($jumlah > 0) {
			$res = $this->db->insert_batch($table, $data);
		}

		return $res;
	}

	public function absen()
	{
		$hasil = $this->db->get('vw_absen');
		return $hasil->result();
	}

	public function absenedit($id)
	{
		$this->db->where('id', $id);
		$hasil = $this->db->get('vw_absen');
		return $hasil->result();
	}

	public function pempel($pelanggan, $tahun, $start, $end)
	{
		$this->db->where('idpelanggan', $pelanggan);
		$this->db->where('tahun', $tahun);
		$this->db->where('bulan >=', $start);
		$this->db->where('bulan <=', $end);
		$this->db->order_by('bulan');
		$hasil = $this->db->get('vw_pel_pilih');
		return $hasil->result();
	}

	public function notagihan($bulan)
	{
		$query = $this->db->query("
						SELECT
						pelanggan.namapelanggan, pelanggan.idpelanggan, pelanggan.alamatpelanggan, pelanggan.tgldaftar, pelanggan.hargapelanggan, pelanggan.tglberhenti, area.namaarea
						FROM
						pelanggan
						INNER JOIN area ON pelanggan.idarea = area.idarea
						where 
						pelanggan.idpelanggan not in (Select idpelanggan from transaksi where transaksi.bulan = $bulan)
						and 
						EXTRACT(MONTH from tgldaftar) <= $bulan
						ORDER BY
						pelanggan.tgldaftar,
						area.idarea
				");

		return $query->result();
	}
}
