<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Amc extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('models');
		// $level = 1;
		// $this->simple_login->cek_auth($level);
	}

	public function index()
	{
		$pelanggan = $this->models->countpelanggan();
		$tagihan = $this->models->sumtagihan();
		$pembayaran = $this->models->sumbayar();
		$kurang = $this->models->sumkurangbayar();
		$output = array(
			'pelanggan' => $pelanggan,
			'tagihan' => $tagihan,
			'bayar' => $pembayaran,
			'kurang' => $kurang
		);
		$this->load->view('header');
		$this->load->view('home', $output);
		$this->load->view('footer');
	}

	public function chart()
	{
		$data = $this->models->grafik();
		echo json_encode($data);
	}

	public function chartbarang()
	{
		$data = $this->models->chartbrg();
		echo json_encode($data);
	}

	public function area()
	{
		$this->load->view('header');
		$this->load->view('area');
		$this->load->view('footer');
	}

	public function ajaxarea()
	{
		$list = $this->models->area();
		$data = array();
		$no = 0;
		foreach ($list as $item) {

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $item->namaarea;
			$row[] = '<button class="btn btn-sm btn-warning editarea"  data-kode="' . $item->idarea . '"><i class="nav-icon fas fa-pencil-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div> 
			<button class="btn btn-sm btn-danger hapusarea"  data-kode="' . $item->idarea . '"><i class="nav-icon fas fa-trash-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			$data[] = $row;
		}

		$output = array(
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function get_editarea()
	{
		$id = $this->input->post('id');
		$list = $this->models->edit_area($id);

		echo json_encode($list);
	}

	public function insertarea()
	{
		$namaarea = $_POST['namaarea'];
		$data = array(
			'namaarea' => strtoupper($namaarea)
		);

		$list = $this->models->insert('area', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function updatearea()
	{
		$id = $_POST['idarea'];
		$namaarea = $_POST['namaarea'];
		$data = array(
			'namaarea' => strtoupper($namaarea)
		);

		$where = array(
			'idarea' => $id,
		);

		$list = $this->models->update('area', $data, $where);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function deletearea()
	{
		$id = $_POST['idarea'];
		$data = array('idarea' => $id);
		$list = $this->models->delete('area', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Dihapus', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function paketharga()
	{
		$this->load->view('header');
		$this->load->view('paketharga');
		$this->load->view('footer');
	}

	public function ajaxharga()
	{
		$list = $this->models->paket();
		$data = array();
		$no = 0;
		foreach ($list as $item) {

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $item->namapaket;
			$row[] = number_format($item->hargapaket, 0, ',', '.');
			$row[] = '<button class="btn btn-sm btn-warning editpaket"  data-kode="' . $item->idpaket . '"><i class="nav-icon fas fa-pencil-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div> 
			<button class="btn btn-sm btn-danger hapuspaket"  data-kode="' . $item->idpaket . '"><i class="nav-icon fas fa-trash-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			$data[] = $row;
		}

		$output = array(
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function get_editpaket()
	{
		$id = $this->input->post('id');
		$list = $this->models->edit_paket($id);

		echo json_encode($list);
	}

	public function insertpaket()
	{
		$namapaket = $_POST['namapaket'];
		$hargapaket = $_POST['hargapaket'];
		$data = array(
			'namapaket' => strtoupper($namapaket),
			'hargapaket' => $hargapaket
		);

		$list = $this->models->insert('paketharga', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function updatepaket()
	{
		$id = $_POST['idpaket'];
		$namapaket = $_POST['namapaket'];
		$hargapaket = $_POST['hargapaket'];
		$data = array(
			'namapaket' => strtoupper($namapaket),
			'hargapaket' => $hargapaket
		);

		$where = array(
			'idpaket' => $id,
		);

		$list = $this->models->update('paketharga', $data, $where);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function deletepaket()
	{
		$id = $_POST['idpaket'];
		$data = array('idpaket' => $id);
		$list = $this->models->delete('paketharga', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Dihapus', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function pelanggan()
	{
		$area  = $this->models->area();
		$paket = $this->models->paket();
		$sales = $this->models->getsales();
		$data = array(
			'data' => $area,
			'paket' => $paket,
			'sales' => $sales
		);
		$this->load->view('header');
		$this->load->view('pelanggan', $data);
		$this->load->view('footer');
	}

	public function ajaxpelanggan()
	{
		$list = $this->models->pelanggan();
		$data = array();
		$no = 0;
		foreach ($list as $item) {

			if ($item->berhenti == null) {
				$aksi =
					'<button class="btn btn-sm btn-warning editpelanggan"  data-kode="' . $item->idpelanggan . '"><i class="nav-icon fas fa-pencil-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div> 
			<button class="btn btn-sm btn-danger hapuspelanggan"  data-kode="' . $item->idpelanggan . '"><i class="nav-icon fas fa-trash-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
			<button class="btn btn-sm btn-info berhentipelanggan"  data-kode="' . $item->idpelanggan . '"><i class="nav-icon fas fa-user-times"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			} else {
				$aksi =
					'<button class="btn btn-sm btn-warning editpelanggan"  data-kode="' . $item->idpelanggan . '"><i class="nav-icon fas fa-pencil-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div> 
			<button class="btn btn-sm btn-danger hapuspelanggan"  data-kode="' . $item->idpelanggan . '"><i class="nav-icon fas fa-trash-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
			<button class="btn btn-sm btn-primary bukapelanggan"  data-kode="' . $item->idpelanggan . '"><i class="nav-icon fas fa-user-check"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			}

			if ($item->berhenti == 1) {
				$status = '<span class="badge badge-success">Berhenti</span>';
			} else {
				$status = '<span class="badge badge-primary">Berlanganan</span>';
			}

			if ($item->berhenti == 1) {
				$tanggal = $item->tglberhenti;
			} else {
				$tanggal = '';
			}

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $item->namapelanggan;
			$row[] = $item->alamatpelanggan;
			$row[] = $item->namaarea;
			$row[] = $item->notelp;
			$row[] = $item->namapaket;
			$row[] = $item->namasales;
			$row[] = $status;
			$row[] = $tanggal;
			$row[] = $aksi;
			$data[] = $row;
		}

		$output = array(
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function get_editpelanggan()
	{
		$id = $this->input->post('id');
		$list = $this->models->edit_pelanggan($id);

		echo json_encode($list);
	}

	public function insertpelanggan()
	{

		$namapelanggan = $_POST['namapelanggan'];
		$alamatpelanggan = $_POST['alamatpelanggan'];
		$area = $_POST['areapelanggan'];
		$telp = $_POST['notelp'];
		$paket = $_POST['paket'];
		$harga = $_POST['harga'];
		$sales = $_POST['sales'];
		$data = array(
			'namapelanggan' => strtoupper($namapelanggan),
			'alamatpelanggan' => strtoupper($alamatpelanggan),
			'idarea' => $area,
			'notelp' => $telp,
			'idpaketharga' => $paket,
			'hargapelanggan' => $harga,
			'tgldaftar' => date('Y-m-d'),
			'sales' => $sales
		);

		$list = $this->models->insert('pelanggan', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function updatepelanggan()
	{
		$id = $_POST['idpelanggan'];
		$namapelanggan = $_POST['namapelanggan'];
		$alamatpelanggan = $_POST['alamatpelanggan'];
		$area = $_POST['areapelanggan'];
		$telp = $_POST['notelp'];
		$paket = $_POST['paket'];
		$harga = $_POST['harga'];
		$sales = $_POST['sales'];
		$data = array(
			'namapelanggan' => strtoupper($namapelanggan),
			'alamatpelanggan' => strtoupper($alamatpelanggan),
			'idarea' => $area,
			'notelp' => $telp,
			'idpaketharga' => $paket,
			'hargapelanggan' => $harga,
			'sales' => $sales
		);

		$where = array(
			'idpelanggan' => $id,
		);

		$list = $this->models->update('pelanggan', $data, $where);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function berhenti()
	{
		$id = $_POST['idpelanggan'];
		$data = array(
			'berhenti' => 1,
			'tglberhenti' => date('Y-m-d')
		);

		$where	=	array(
			'idpelanggan' => $id
		);

		$list	=	$this->models->update('pelanggan', $data, $where);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function buka()
	{
		$id = $_POST['idpelanggan'];
		$data = array(
			'berhenti' => null,
			'tgllangganan' => date('Y-m-d')
		);

		$where	=	array(
			'idpelanggan' => $id
		);

		$list	=	$this->models->update('pelanggan', $data, $where);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function deletepelanggan()
	{
		$id = $_POST['idpelanggan'];
		$data = array('idpelanggan' => $id);
		$list = $this->models->delete('pelanggan', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Dihapus', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function pengguna()
	{
		$list['data'] = $this->models->level();
		$this->load->view('header');
		$this->load->view('pengguna', $list);
		$this->load->view('footer');
	}

	public function ajaxpengguna()
	{
		$list = $this->models->pengguna();
		$data = array();
		$no = 0;
		foreach ($list as $item) {

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $item->nama;
			$row[] = $item->username;
			$row[] = $item->namalevel;
			$row[] = '<button class="btn btn-sm btn-warning editpengguna"  data-kode="' . $item->idusers . '"><i class="nav-icon fas fa-pencil-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div> 
			<button class="btn btn-sm btn-danger hapuspengguna"  data-kode="' . $item->idusers . '"><i class="nav-icon fas fa-trash-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			$data[] = $row;
		}

		$output = array(
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function get_editpengguna()
	{
		$id = $this->input->post('id');
		$list = $this->models->edit_pengguna($id);

		echo json_encode($list);
	}

	public function insertpengguna()
	{
		$namapengguna = $_POST['namapengguna'];
		$username = $_POST['username'];
		$password = $_POST['password'];
		$level = $_POST['level'];
		$data = array(
			'nama' => strtoupper($namapengguna),
			'username' => $username,
			'password' => md5($password),
			'level' => $level
		);

		$list = $this->models->insert('users', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function updatepengguna()
	{
		$id = $_POST['idpengguna'];
		$namapengguna = $_POST['editnamapengguna'];
		$username = $_POST['editusername'];
		$password = $_POST['editpassword'];
		$level = $_POST['editlevel'];
		$data = array(
			'nama' => strtoupper($namapengguna),
			'username' => $username,
			'password' => md5($password),
			'level' => $level
		);

		$where = array(
			'idusers' => $id,
		);

		$list = $this->models->update('users', $data, $where);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function deletepengguna()
	{
		$id = $_POST['idpengguna'];
		$data = array('idusers' => $id);
		$list = $this->models->delete('users', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Dihapus', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function tagihan()
	{
		$pelanggan = $this->models->pelanggantag();
		$paket = $this->models->paket();
		$data = array(
			"pelanggan" => $pelanggan,
			"paket" => $paket
		);
		$this->load->view('header');
		$this->load->view('tagihan', $data);
		$this->load->view('footer');
	}

	public function ajaxtagihan()
	{
		$list = $this->models->tagihan();
		$data = array();
		$no = 0;
		foreach ($list as $item) {

			if ($item->bulan == 1) {
				$bulan = 'Januari';
			} elseif ($item->bulan == 2) {
				$bulan = 'Februari';
			} elseif ($item->bulan == 3) {
				$bulan = 'Maret';
			} elseif ($item->bulan == 4) {
				$bulan = 'April';
			} elseif ($item->bulan == 5) {
				$bulan = 'Mei';
			} elseif ($item->bulan == 6) {
				$bulan = 'Juni';
			} elseif ($item->bulan == 7) {
				$bulan = 'Juli';
			} elseif ($item->bulan == 8) {
				$bulan = 'Agustus';
			} elseif ($item->bulan == 9) {
				$bulan = 'September';
			} elseif ($item->bulan == 10) {
				$bulan = 'Oktober';
			} elseif ($item->bulan == 11) {
				$bulan = 'November';
			} else {
				$bulan = 'Desember';
			}

			if ($this->session->userdata('user_level') == 1 && $item->statusbayar == null) {
				$aksi = '<button class="btn btn-sm btn-primary pdftagihan"  data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-file-pdf"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
				<button class="btn btn-sm btn-warning edittagihan"  data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-pencil-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div> 
				<button class="btn btn-sm btn-danger hapustagihan"  data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-trash-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			} elseif ($this->session->userdata('user_level') == 1 && $item->statusbayar == 1) {
				$aksi = '<button class="btn btn-sm btn-primary pdftagihan"  data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-file-pdf"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			} elseif ($this->session->userdata('user_level') == 2 && $item->statusbayar == 1) {

				$aksi = '<button class="btn btn-sm btn-primary pdftagihan"  data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-file-pdf"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			} else {
				$aksi = '<button class="btn btn-sm btn-primary pdftagihan"  data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-file-pdf"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
				<button class="btn btn-sm btn-warning edittagihan"  data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-pencil-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div> ';
			}

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $item->idbilling;
			$row[] = $item->namapelanggan;
			$row[] = $item->alamatpelanggan;
			$row[] = $bulan;
			$row[] = $item->tahun;
			$row[] = $item->namapaket;
			$row[] = number_format($item->subharga, 0, ',', '.');
			$row[] = $aksi;
			$data[] = $row;
		}

		$output = array(
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function deletetagihan()
	{
		$id = $_POST['idtransaksi'];
		$data = array('idtransaksi' => $id);
		$list = $this->models->delete('transaksi', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Dihapus', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}


	public function vwtagihan()
	{
		$id = $_POST['id'];
		$paket = $this->models->paketid($id);

		echo json_encode($paket);
	}

	public function vwpelanggan()
	{
		$id = $_POST['idtagihan'];
		$paket = $this->models->pelanggantagihan($id);

		echo json_encode($paket);
	}

	public function inserttagihan()
	{
		$pelanggan = $_POST['pelanggan'];
		$bulan = $_POST['bulan'];
		$paket = $_POST['paket'];
		$tagihan = $_POST['tagihan'];
		$harga = $_POST['harga'];
		$tambahan = $_POST['tambahan'];

		$cek =	$this->models->edit_pelanggan($pelanggan);

		$data = array(
			'idpelanggan' => $pelanggan,
			'tglpenetapan' => date('Y-m-d'),
			'bulan' => $bulan,
			'tahun' => date('Y'),
			'idpaketharga' => $paket,
			'harga' => $harga,
			'subharga' => $tagihan,
			'penambahan' => $tambahan,
			'usertagihan' => $this->session->userdata('id_user'),
			'namapelanggan' => $cek[0]->namapelanggan
		);

		$list = $this->models->insert('transaksi', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function get_edittagihan()
	{
		$id = $this->input->post('id');
		$list = $this->models->edit_tagihan($id);

		echo json_encode($list);
	}

	public function updatetagihan()
	{
		$id = $_POST['idtagihan'];
		$bulan = $_POST['bulan'];
		$tagihan = $_POST['tagihan'];
		$tambahan = $_POST['tambahan'];
		$data = array(
			// 'idtransaksi' => $id,
			'bulan' => $bulan,
			'penambahan' => $tambahan,
			'subharga' => $tagihan,
			'tgledittagihan' => date('Y-m-d'),
			'useredittagihan' => $this->session->userdata('id_user')
		);

		$where = array(
			'idtransaksi' => $id,
		);

		$list = $this->models->update('transaksi', $data, $where);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function pembayaran()
	{
		$this->load->view('header');
		$this->load->view('pembayaran');
		$this->load->view('footer');
	}

	public function ajaxpembayaran()
	{
		$list = $this->models->tagihan();
		$data = array();
		$no = 0;
		foreach ($list as $item) {

			if ($item->bulan == 1) {
				$bulan = 'Januari';
			} elseif ($item->bulan == 2) {
				$bulan = 'Februari';
			} elseif ($item->bulan == 3) {
				$bulan = 'Maret';
			} elseif ($item->bulan == 4) {
				$bulan = 'April';
			} elseif ($item->bulan == 5) {
				$bulan = 'Mei';
			} elseif ($item->bulan == 6) {
				$bulan = 'Juni';
			} elseif ($item->bulan == 7) {
				$bulan = 'Juli';
			} elseif ($item->bulan == 8) {
				$bulan = 'Agustus';
			} elseif ($item->bulan == 9) {
				$bulan = 'September';
			} elseif ($item->bulan == 10) {
				$bulan = 'Oktober';
			} elseif ($item->bulan == 11) {
				$bulan = 'November';
			} else {
				$bulan = 'Desember';
			}

			if ($item->statusbayar == 1 && $this->session->userdata('user_level') == 1) {
				$aksi = '
				<button class="btn btn-sm btn-primary pdfpembayaran"  data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-file-pdf"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
				<button class="btn btn-sm btn-danger hapuspembayaran"  data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-trash-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			} elseif ($item->statusbayar == 1) {
				$aksi = '
				<button class="btn btn-sm btn-primary pdfpembayaran"  data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-file-pdf"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			} else {
				$aksi = '
				<button class="btn btn-sm btn-warning bayar" data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-money-bill-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			}

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $item->idbilling;
			$row[] = $item->namapelanggan;
			$row[] = $item->alamatpelanggan;
			$row[] = $bulan;
			$row[] = $item->tahun;
			$row[] = $item->namapaket;
			$row[] = number_format($item->subharga, 0, ',', '.');
			$row[] = $aksi;
			$data[] = $row;
		}

		$output = array(
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function get_editpembayaran()
	{
		$id = $this->input->post('id');
		$list = $this->models->edit_tagihan($id);

		echo json_encode($list);
	}

	public function insertpembayaran()
	{
		$id = $_POST['id'];
		$data = array(

			'tglpembayaran' => date('Y-m-d'),
			'statusbayar' => 1,
			'userbayar' => $this->session->userdata('id_user')
		);

		$where = array(
			'idtransaksi' => $id,
		);

		$list = $this->models->update('transaksi', $data, $where);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function updatehapuspembayaran()
	{
		$id = $_POST['id'];
		$data = array(
			'statusbayar' => null,
			'tglpembayaran' => null,
			'tgldelbayar' => date('Y-m-d'),
			'userdelbayar' => $this->session->userdata('id_user')
		);

		$where = array(
			'idtransaksi' => $id,
		);

		$list = $this->models->update('transaksi', $data, $where);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function userpelanggan()
	{
		$pengguna['data'] = $this->models->pelanggan();

		$this->load->view('header');
		$this->load->view('userpelanggan', $pengguna);
		$this->load->view('footer');
	}

	public function ajaxuserpelanggan()
	{
		$list = $this->models->pelangganuser();
		$data = array();
		$no = 0;
		foreach ($list as $item) {

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $item->namapelanggan;
			$row[] = $item->username;
			$row[] = '<button class="btn btn-sm btn-warning edituserpelanggan"  data-kode="' . $item->idusers . '"><i class="nav-icon fas fa-pencil-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div> 
			<button class="btn btn-sm btn-danger hapususerpelanggan"  data-kode="' . $item->idusers . '"><i class="nav-icon fas fa-trash-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			$data[] = $row;
		}

		$output = array(
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function insertuserpelanggan()
	{
		$id = $_POST['pelangganuser'];
		$username = $_POST['usernameuser'];
		$password = $_POST['passworduser'];
		$level = $_POST['leveluser'];
		$data = array(
			'idpelanggan' => $id,
			'username' => $username,
			'password' => md5($password),
			'level' => $level
		);

		$list = $this->models->insert('users', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function updateuserpelanggan()
	{
		$id = $_POST['idpenggunauser'];
		$username = $_POST['editusernameuser'];
		$password = $_POST['editpassworduser'];

		$data = array(
			'username' => $username,
			'password' => md5($password)
		);

		$where = array(
			'idusers' => $id,
		);

		$list = $this->models->update('users', $data, $where);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function deleteuserpelanggan()
	{
		$id = $_POST['idpenggunauser'];
		$data = array('idusers' => $id);
		$list = $this->models->delete('users', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Dihapus', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function laporan()
	{
		$data['data']  = $this->models->area();
		$data['pelanggan']  = $this->models->pelanggan();
		$this->load->view('header');
		$this->load->view('laporane', $data);
		$this->load->view('footer');
	}


	public function pdf($start)
	{
		// $start = $this->input->post('idtransaksi');
		$list = $this->models->edit_tagihan($start);
		// print_r($list);
		$output = array(
			'data' => $list
		);

		$mpdf = new \Mpdf\Mpdf([
			'tempDir' => '/tmp',
			'mode' => 'utf-8',
			'format' => [210, 330],
			'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 10,
			'margin_bottom' => 2,
			'margin_header' => 0,
			'margin_footer' => 0,
		]);
		$file_name = 'Tagihan';
		$html = $this->load->view('pdftagihan', $output, TRUE);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
		// $mpdf->WriteHTML($html, \Mpdf\HTMLParserMode::HTML_BODY);
		// $mpdf->Output($file_name .'.pdf', \Mpdf\Output\Destination::INLINE);
	}

	public function pdfpembayaran($id)
	{
		// $start = $this->input->post('idtransaksi');
		$list = $this->models->edit_tagihan($id);
		print_r($list);
		$output = array(
			'data' => $list
		);

		$mpdf = new \Mpdf\Mpdf([
			'tempDir' => '/tmp',
			'mode' => 'utf-8',
			'format' => [210, 330],
			'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 10,
			'margin_bottom' => 2,
			'margin_header' => 0,
			'margin_footer' => 0,
		]);
		$html = $this->load->view('pdfpembayaran', $output, TRUE);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}

	public function pdftagihanlaporan()
	{
		ini_set("pcre.backtrack_limit", "9000000");
		$start = $this->input->post('datepicker');
		$end = $this->input->post('datepicker1');
		$list = $this->models->cetak_tagihan($start, $end);

		$output = array(
			'data' => $list
		);

		// return print_r($list);

		$mpdf = new \Mpdf\Mpdf([
			'tempDir' => '/tmp',
			'mode' => 'utf-8',
			'format' => [210, 330],
			'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 10,
			'margin_bottom' => 2,
			'margin_header' => 0,
			'margin_footer' => 0,
		]);
		$html = $this->load->view('pdftagihan', $output, TRUE);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}

	public function pdfbayarlaporan()
	{
		$start = $this->input->post('datepicker2');
		$end = $this->input->post('datepicker3');
		$list = $this->models->cetak_pembayaran($start, $end);
		$output = array(
			'data' => $list
		);

		$mpdf = new \Mpdf\Mpdf([
			'tempDir' => '/tmp',
			'mode' => 'utf-8',
			'format' => [210, 330],
			'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 10,
			'margin_bottom' => 2,
			'margin_header' => 0,
			'margin_footer' => 0,
		]);
		$html = $this->load->view('pdfpembayaran', $output, TRUE);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}

	public function pdfblmbayar()
	{
		$start = $_POST['laptahun'];
		$end = $_POST['lapbulan'];
		if ($end == '') {
			$list = $this->models->cetak_blmbayar_semua($start);
			$sum = $this->models->sum_blmbayar_semua($start);
		} else {
			$list = $this->models->cetak_blmbayar($start, $end);
			$sum = $this->models->sum_blmbayar($start, $end);
		}

		$output = array(
			'data' => $list,
			'sum' => $sum
		);

		$mpdf = new \Mpdf\Mpdf([
			'tempDir' => '/tmp',
			'mode' => 'utf-8',
			'format' => [210, 330],
			'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 10,
			'margin_bottom' => 2,
			'margin_header' => 0,
			'margin_footer' => 0,
		]);
		$html = $this->load->view('pdfbelumbayar', $output, TRUE);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}

	public function pdfpelanggan()
	{
		$start = $_POST['arealap'];
		if ($start == '') {
			$list = $this->models->pelanggan();
		} else {
			$list = $this->models->pelangganid($start);
		}

		$output = array(
			'data' => $list
		);

		$mpdf = new \Mpdf\Mpdf([
			'tempDir' => '/tmp',
			'mode' => 'utf-8',
			'format' => [210, 330],
			'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 10,
			'margin_bottom' => 2,
			'margin_header' => 0,
			'margin_footer' => 0,
		]);
		$html = $this->load->view('pdfpelanggan', $output, TRUE);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}

	public function pdfgajian($id)
	{
		$list = $this->models->editgajian($id);

		$output = array(
			'data' => $list
		);

		$mpdf = new \Mpdf\Mpdf([
			'tempDir' => '/tmp',
			'mode' => 'utf-8',
			'format' => [210, 330],
			'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 10,
			'margin_bottom' => 2,
			'margin_header' => 0,
			'margin_footer' => 0,
		]);
		$html = $this->load->view('pdfgajian', $output, TRUE);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}

	public function pdfgajianglobal()
	{
		$start = $_POST['tahungajian'];
		$end = $_POST['bulangajian'];
		$list = $this->models->gajianglobal($start, $end);

		$output = array(
			'data' => $list
		);

		$mpdf = new \Mpdf\Mpdf([
			'tempDir' => '/tmp',
			'mode' => 'utf-8',
			'format' => [210, 330],
			'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 10,
			'margin_bottom' => 2,
			'margin_header' => 0,
			'margin_footer' => 0,
		]);
		$html = $this->load->view('pdfgajian', $output, TRUE);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}

	public function pdftag()
	{
		// $bul = $_POST['tahungajian'];
		$end = $_POST['bulantag'];
		$list = $this->models->notagihan($end);
		$harga = 0;
		foreach($list as $cuk){
			$harga +=$cuk->hargapelanggan;
		}

		if ($end == 1) {
			$bulan = 'Januari';
		} elseif ($end == 2) {
			$bulan = 'Februari';
		} elseif ($end == 3) {
			$bulan = 'Maret';
		} elseif ($end == 4) {
			$bulan = 'April';
		} elseif ($end == 5) {
			$bulan = 'Mei';
		} elseif ($end == 6) {
			$bulan = 'Juni';
		} elseif ($end == 7) {
			$bulan = 'Juli';
		} elseif ($end == 8) {
			$bulan = 'Agustus';
		} elseif ($end == 9) {
			$bulan = 'September';
		} elseif ($end == 10) {
			$bulan = 'Oktober';
		} elseif ($end == 11) {
			$bulan = 'November';
		} else {
			$bulan = 'Desember';
		}

		$output = array(
			'data' => $list,
			'totalharga' => $harga,
			'bulan'		=> $bulan
		);

		$mpdf = new \Mpdf\Mpdf([
			'tempDir' => '/tmp',
			'mode' => 'utf-8',
			'format' => [210, 330],
			'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 10,
			'margin_bottom' => 2,
			'margin_header' => 0,
			'margin_footer' => 0,
			'orientation' => 'L'
		]);
		$html = $this->load->view('pdftag', $output, TRUE);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}

	public function pdfabsen()
	{
		$bulan = $_POST['bulanabsen'];
		$tahun = $_POST['tahunabsen'];
		
		$list = $this->models->pdfabsen($bulan, $tahun);

		if ($bulan == 1) {
			$bulane = 'Januari';
		} elseif ($bulan == 2) {
			$bulane = 'Februari';
		} elseif ($bulan == 3) {
			$bulane = 'Maret';
		} elseif ($bulan == 4) {
			$bulane = 'April';
		} elseif ($bulan == 5) {
			$bulane = 'Mei';
		} elseif ($bulan == 6) {
			$bulane = 'Juni';
		} elseif ($bulan == 7) {
			$bulane = 'Juli';
		} elseif ($bulan == 8) {
			$bulane = 'Agustus';
		} elseif ($bulan == 9) {
			$bulane = 'September';
		} elseif ($bulan == 10) {
			$bulane = 'Oktober';
		} elseif ($bulan == 11) {
			$bulane = 'November';
		} else {
			$bulane = 'Desember';
		}

		$output = array(
			'data' => $list,
			'bulan' => $bulane,
			'tahun' => $tahun
		);

		$mpdf = new \Mpdf\Mpdf([
			'tempDir' => '/tmp',
			'mode' => 'utf-8',
			'format' => [210, 330],
			'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 10,
			'margin_bottom' => 2,
			'margin_header' => 0,
			'margin_footer' => 0,
		]);
		$html = $this->load->view('pdfabsen', $output, TRUE);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}

	public function pdfpempel()
	{
		$pelanggan = $_POST['pelanggan'];
		// $pelanggan = $_POST['status'];
		$tahun = $_POST['tahunpel'];
		$start = $_POST['bulanpelstart'];
		$end = $_POST['bulanpelend'];
		$list = $this->models->pempel($pelanggan, $tahun, $start, $end);
		
		$output = array(
			'data' => $list,
			'tahun' => $tahun,
			'nama' => $list[0]
		);

		$mpdf = new \Mpdf\Mpdf([
			'tempDir' => '/tmp',
			'mode' => 'utf-8',
			'format' => [210, 330],
			'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 10,
			'margin_bottom' => 2,
			'margin_header' => 0,
			'margin_footer' => 0,
		]);
		$html = $this->load->view('pdfpempel', $output, TRUE);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}
	public function karyawan()
	{
		$jabat = $this->models->jabatan();
		$pend = $this->models->pendidikan();
		$jenis = $this->models->jenis();
		$data = array(
			'jabatan' => $jabat,
			'pendidikan' => $pend,
			'jenis' => $jenis
		);
		$this->load->view('header');
		$this->load->view('karyawan', $data);
		$this->load->view('footer');
	}

	public function ajaxkaryawan()
	{
		$list = $this->models->karyawan();
		$data = array();
		$no = 0;
		foreach ($list as $item) {
			$masa = $item->masakerja;
			$tanggal = new DateTime($masa);
			$now = new DateTime();

			$masakerja = $tanggal->diff($now);

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $item->namakaryawan;
			$row[] = $item->alamatkaryawan;
			$row[] = $item->notelp;
			$row[] = $item->namajabatan;
			$row[] = $item->namapendidikan;
			$row[] = $masakerja->y . ' Tahun ' . $masakerja->m . ' Bulan ' . $masakerja->d . ' Hari';
			$row[] = number_format($item->gajipokok, 0, ',', '.');
			$row[] = $item->karyawan;
			$row[] = '<button class="btn btn-sm btn-warning editkaryawan"  data-kode="' . $item->idkaryawan . '"><i class="nav-icon fas fa-pencil-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div> 
			<button class="btn btn-sm btn-danger hapuskaryawan"  data-kode="' . $item->idkaryawan . '"><i class="nav-icon fas fa-trash-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			$data[] = $row;
		}

		$output = array(
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function insertkaryawan()
	{
		$nama = $_POST['nmkaryawan'];
		$alamat = $_POST['alamatkaryawan'];
		$jabatan = $_POST['jabatan'];
		$pendidikan = $_POST['pendidikan'];
		$tglterima = $_POST['tglterima'];
		$gajipokok = $_POST['gajipokok'];
		$notelp = $_POST['notelp'];
		$jeniskaryawan = $_POST['jnskaryawan'];
		$idkartu = $_POST['idkartu'];
		$data = array(
			'namakaryawan' => strtoupper($nama),
			'alamatkaryawan' => strtoupper($alamat),
			'notelp' => $notelp,
			'jabatan' => $jabatan,
			'pendidikan' => $pendidikan,
			'masakerja' => $tglterima,
			'gajipokok' => $gajipokok,
			'jeniskaryawan' => $jeniskaryawan,
			'idkartu'	=>	$idkartu
		);


		$list = $this->models->insert('karyawan', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function get_editkaryawan()
	{
		$id = $this->input->post('id');
		$list = $this->models->editkaryawan($id);

		echo json_encode($list);
	}

	public function updatekaryawan()
	{
		$id = $_POST['idkaryawan'];
		$nama = $_POST['namakaryawan'];
		$alamat = $_POST['alamatkaryawan'];
		$jabatan = $_POST['jabatan'];
		$pendidikan = $_POST['pendidikan'];
		$tglterima = $_POST['tglterima'];
		$gajipokok = $_POST['gajipokok'];
		$notelp = $_POST['notelp'];
		$jeniskaryawan = $_POST['jeniskaryawan'];
		$idkartu = $_POST['idkartu'];

		$data = array(
			'namakaryawan' => strtoupper($nama),
			'alamatkaryawan' => strtoupper($alamat),
			'notelp' => $notelp,
			'jabatan' => $jabatan,
			'pendidikan' => $pendidikan,
			'masakerja' => $tglterima,
			'gajipokok' => $gajipokok,
			'jeniskaryawan' => $jeniskaryawan,
			'idkartu'	=>	$idkartu
		);
		$where = array(
			'idkaryawan' => $id,
		);

		$list = $this->models->update('karyawan', $data, $where);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function deletekaryawan()
	{
		$id = $_POST['idkaryawan'];
		$data = array('idkaryawan' => $id);
		$list = $this->models->delete('karyawan', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Dihapus', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function gajian()
	{
		$jabat = $this->models->karyawan();
		// $pend = $this->models->pendidikan();
		$jenis = $this->models->jenis();
		$data = array(
			'karyawan' => $jabat,
			'jenis' => $jenis
		);
		$this->load->view('header');
		$this->load->view('gajian', $data);
		$this->load->view('footer');
	}

	public function vwkaryawan()
	{
		$id = $_POST['idkaryawan'];
		$paket = $this->models->getkrywn($id);

		if ($paket[0]['jeniskaryawan'] == 2) {
			$data = $this->models->getfee($id);
		} else {
			$data = $this->models->editkaryawan($id);
		}

		echo json_encode($data);
	}

	public function getkaryawan()
	{
		$id = $_POST['id'];
		$paket = $this->models->getkaryawan($id);

		echo json_encode($paket);
	}

	public function ajaxgajian()
	{
		$list = $this->models->gajian();
		$data = array();
		$no = 0;
		foreach ($list as $item) {
			if ($item->bulan == 1) {
				$bulan = 'Januari';
			} elseif ($item->bulan == 2) {
				$bulan = 'Februari';
			} elseif ($item->bulan == 3) {
				$bulan = 'Maret';
			} elseif ($item->bulan == 4) {
				$bulan = 'April';
			} elseif ($item->bulan == 5) {
				$bulan = 'Mei';
			} elseif ($item->bulan == 7) {
				$bulan = 'Juni';
			} elseif ($item->bulan == 8) {
				$bulan = 'Juli';
			} elseif ($item->bulan == 9) {
				$bulan = 'Agustus';
			} elseif ($item->bulan == 10) {
				$bulan = 'September';
			} elseif ($item->bulan == 11) {
				$bulan = 'Oktober';
			} elseif ($item->bulan == 12) {
				$bulan = 'November';
			} else {
				$bulan = 'Desember';
			}


			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $item->namakaryawan;
			$row[] = $bulan;
			$row[] = $item->tahun;
			$row[] = number_format($item->gajipokok, 0, ',', '.');
			$row[] = number_format($item->tunjangan, 0, ',', '.');
			$row[] = number_format($item->potongan, 0, ',', '.');
			$row[] = number_format($item->gajibersih, 0, ',', '.');
			$row[] = '<button class="btn btn-sm btn-primary pdfgajian"  data-kode="' . $item->idgaji . '"><i class="nav-icon fas fa-file-pdf"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
			<button class="btn btn-sm btn-warning editgajian"  data-kode="' . $item->idgaji . '"><i class="nav-icon fas fa-pencil-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div> 
			<button class="btn btn-sm btn-danger hapusgajian"  data-kode="' . $item->idgaji . '"><i class="nav-icon fas fa-trash-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			$data[] = $row;
		}

		$output = array(
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function insertgajian()
	{
		$nama = $_POST['namakaryawan'];
		// $tahun = $_POST['tahun'];
		$bulan = $_POST['bulan'];
		$gajipokok = $_POST['gajipokok'];
		$tunjangan = $_POST['tunjangan'];
		$potongan = $_POST['potongan'];
		$gajibersih = $_POST['gajibersih'];

		$data = array(
			'idkaryawan' => $nama,
			'bulan' => $bulan,
			'tahun' => date('Y'),
			'gajipokok' => $gajipokok,
			'tunjangan' => $tunjangan,
			'potongan' => $potongan,
			'gajibersih' => $gajibersih
		);

		$list = $this->models->insert('gajian', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function get_editgajian()
	{
		$id = $this->input->post('id');
		$list = $this->models->editgajian($id);

		echo json_encode($list);
	}

	public function updategajian()
	{
		$id = $_POST['idgajian'];
		// $tahun = $_POST['tahun'];
		$bulan = $_POST['bulan'];
		$gajipokok = $_POST['gajipokok'];
		$tunjangan = $_POST['tunjangan'];
		$potongan = $_POST['potongan'];
		$gajibersih = $_POST['gajibersih'];

		$data = array(
			'bulan' => $bulan,
			'gajipokok' => $gajipokok,
			'tunjangan' => $tunjangan,
			'potongan' => $potongan,
			'gajibersih' => $gajibersih
		);
		$where = array(
			'idgaji' => $id,
		);

		$list = $this->models->update('gajian', $data, $where);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function deletegajian()
	{
		$id = $_POST['idgaji'];
		$data = array('idgaji' => $id);
		$list = $this->models->delete('gajian', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Dihapus', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function barang()
	{
		$list['pel'] = $this->models->pel();

		$this->load->view('header');
		$this->load->view('barang', $list);
		$this->load->view('footer');
	}

	public function ajaxbarang()
	{
		$list = $this->models->pelanggan();
		$data = array();
		$no = 0;
		foreach ($list as $item) {

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $item->namapelanggan;
			$row[] = $item->alamatpelanggan;
			$row[] = $item->namaarea;
			$row[] = '<button class="btn btn-sm btn-warning detailbarang"  data-kode="' . $item->idpelanggan . '"><i class="nav-icon fas fa-search"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			// <button class="btn btn-sm btn-danger hapusbarangpel"  data-kode="' . $item->idpelanggan . '"><i class="nav-icon fas fa-trash-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			$data[] = $row;
		}

		$output = array(
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function get_editbarang()
	{
		$id = $this->input->post('id');
		$list = $this->models->edit_barang($id);

		echo json_encode($list);
	}

	public function get_barang()
	{
		$id = $this->input->post('id');
		$list = $this->models->vwbarang($id);
		$data = array();
		$no = 0;
		foreach ($list as $item) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $item->namabarang;
			$row[] = $item->serial;
			$row[] = $item->jumlah;
			$row[] = number_format($item->harga, 0, ',', '.');
			$row[] = '<button class="btn btn-sm btn-warning editbarang"  data-kode="' . $item->idbarang . '"><i class="nav-icon fas fa-pencil-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div> 
			<button class="btn btn-sm btn-danger hapusbarang"  data-kode="' . $item->idbarang . '"><i class="nav-icon fas fa-trash-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			$data[] = $row;
		}

		$output = array(
			"data" => $data,
			// "kode" => $kode
		);
		echo json_encode($output);
	}

	public function simpan()
	{
		$master = $this->input->post('master');
		$detail = $this->input->post('detail');
		$idpelanggan = $this->input->post('idpelanggan');
		$jumlah = $this->input->post('jumlah');
		$harga = $this->input->post('harga');
		$serial = $this->input->post('serial');
		$namabarang = $this->input->post('namabarang');
		$total = $this->input->post('total');
		$tgl = date('Y-m-d');

		$arr_new = array(
			'idpelanggan' => $idpelanggan,
			'namabarang' => strtoupper($namabarang),
			'jumlah' => $jumlah,
			'serial' => $serial,
			'hargasatuan' => $harga,
			'harga' => $total,
			'tglpembelian' => $tgl
		);

		$cart = [];

		for ($col = 0; $col < count($arr_new['idpelanggan']); $col++) {

			$data = array(
				'idpelanggan' => $arr_new['idpelanggan'][$col],
				'namabarang' => $arr_new['namabarang'][$col],
				'jumlah' => $arr_new['jumlah'][$col],
				'serial' => $arr_new['serial'][$col],
				'hargasatuan' => $arr_new['hargasatuan'][$col],
				'harga' => $arr_new['harga'][$col],
				'tglpembelian' => $arr_new['tglpembelian'],

			);
			array_push($cart, $data);
		}
		// print_r($cart);

		$list = $this->models->multiSave('barang', $cart);

		if ($list == TRUE) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	// public function insertbarang()
	// {
	// 	$namabarang = $_POST['namabarang'];
	// 	$serial = $_POST['serial'];
	// 	$jumlah = $_POST['jumlah'];
	// 	$harga = $_POST['harga'];
	// 	$data = array(
	// 		'namabarang' => $namabarang,
	// 		'serial' => $serial,
	// 		'jumlah' => $jumlah,
	// 		'harga' => $harga,
	// 		'tglpembelian' => date('Y-m-d')
	// 	);
	// 	$list = $this->models->insert('barang', $data);

	// 	if ($list) {
	// 		echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
	// 	} else {
	// 		echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
	// 	}

	// }

	public function updatebarang()
	{
		$id = $_POST['idbarang'];
		$namabarang = $_POST['namabarang'];
		$serial = $_POST['serial'];
		$jumlah = $_POST['jumlah'];
		$harga = $_POST['harga'];
		$total = $_POST['total'];
		$data = array(
			'namabarang' => strtoupper($namabarang),
			'serial' => $serial,
			'jumlah' => $jumlah,
			'hargasatuan' => $harga,
			'harga' => $total,
			'tglpembelian' => date('Y-m-d')
		);

		$where = array(
			'idbarang' => $id,
		);

		$list = $this->models->update('barang', $data, $where);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function deletebarang()
	{
		$id = $_POST['id'];
		$data = array('idbarang' => $id);
		$list = $this->models->delete('barang', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Dihapus', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function absenkaryawan()
	{
		$data['karyawan'] = $this->models->absenkaryawan();
		$this->load->view('header');
		$this->load->view('absenkaryawan', $data);
		$this->load->view('footer');
	}

	public function ajaxabsenkaryawan()
	{
		$list = $this->models->absen();
		$data = array();
		$no = 0;
		foreach ($list as $item) {

			if ($item->bulan == 1) {
				$bulan = 'Januari';
			} elseif ($item->bulan == 2) {
				$bulan = 'Februari';
			} elseif ($item->bulan == 3) {
				$bulan = 'Maret';
			} elseif ($item->bulan == 4) {
				$bulan = 'April';
			} elseif ($item->bulan == 5) {
				$bulan = 'Mei';
			} elseif ($item->bulan == 6) {
				$bulan = 'Juni';
			} elseif ($item->bulan == 7) {
				$bulan = 'Juli';
			} elseif ($item->bulan == 8) {
				$bulan = 'Agustus';
			} elseif ($item->bulan == 9) {
				$bulan = 'September';
			} elseif ($item->bulan == 10) {
				$bulan = 'Oktober';
			} elseif ($item->bulan == 11) {
				$bulan = 'November';
			} elseif ($item->bulan == 12) {
				$bulan = 'Desember';
			}

			if ($item->telat == 1) {
				$telat = 'Telat';
			} else {
				$telat = '';
			}

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $item->namakaryawan;
			$row[] = $bulan;
			$row[] = $item->tgl;
			$row[] = $item->jammasuk;
			$row[] = $item->jamkeluar;
			$row[] = $telat;
			$row[] = '<button class="btn btn-sm btn-warning editabsenkaryawan"  data-kode="' . $item->id . '"><i class="nav-icon fas fa-pencil-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div> 
			<button class="btn btn-sm btn-danger hapusabsenkaryawan"  data-kode="' . $item->id . '"><i class="nav-icon fas fa-trash-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			$data[] = $row;
		}

		$output = array(
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function get_editabsenkaryawan()
	{
		$id = $this->input->post('id');
		$list = $this->models->absenedit($id);

		echo json_encode($list);
	}

	public function insertabsenkaryawan()
	{
		$idkaryawan = $_POST['karyawan'];
		$tanggal = $_POST['tanggal'];
		$absen = $_POST['absen'];
		$jam = $_POST['jam'];

		$karya = $this->models->editkaryawan($idkaryawan);
	
		if ($jam > '08:10:00') {
			$telat = 1;
		} else {
			$telat = null;
		}

		if ($absen == 'masuk') {
			$data = array(
				'idkaryawan' => $karya[0]->idkartu,
				'bulan'		 =>	date('m'),
				'tgl'		 =>	$tanggal,
				'jammasuk'	 => $jam,
				'telat'		 => $telat
			);
			$list = $this->models->insert('absen', $data);
		} else {
			$data = array(
				'idkaryawan' => $karya[0]->idkartu,
				'bulan'		 =>	date('m'),
				'tgl'		 =>	$tanggal,
				'jamkeluar'	 => $jam,
				'telat'		 =>	$telat
			);

			$where = array(
				'idkaryawan' => $karya[0]->idkartu,
				'tgl'		 => $tanggal
			);

			$list = $this->models->update('absen', $data, $where);
		}

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function updateabsenkaryawan()
	{
		$id = $_POST['idkaryawan'];
		$tanggal = $_POST['tanggal'];
		$jammasuk = $_POST['jammasuk'];
		$jamkeluar = $_POST['jamkeluar'];

		if ($jammasuk > '08:10:00') {
			$telat = 1;
		} else {
			$telat = null;
		}

		$data = array(
			'tgl' => $tanggal,
			'jammasuk' => $jammasuk,
			'jamkeluar' => $jamkeluar,
			'telat'		=> $telat
		);

		$where = array(
			'id' => $id,
		);

		$list = $this->models->update('absen', $data, $where);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function deleteabsenkaryawan()
	{
		$id = $_POST['id'];
		$data = array('id' => $id);
		$list = $this->models->delete('absen', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Dihapus', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}
}
