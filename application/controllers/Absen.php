<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Absen extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('models');
	}

	public function index()
	{

		$this->load->view('absen');
	}

	public function jam()
	{
		$timezone = new DateTimeZone('Asia/Jakarta');
		$date = new DateTime();
		$date->setTimeZone($timezone);
		// date_default_timezone_set('Asia/Jakarta'); // Menyesuaikan waktu dengan tempat kita tinggal
		echo $timestamp = $date->format('H:i:s'); // Menampilkan Jam Sekarang
	}

	public function input()
	{

		$timezone = new DateTimeZone('Asia/Jakarta');
		$date = new DateTime();
		$date->setTimeZone($timezone);

		$id	=	$_POST['id'];
		$tgl = $date->format('Y-m-d');
		$cek = $this->models->cekkaryawan($id);
		$cektgl = [];
		$cekjammasuk = [];
		$cekid = [];
		$cekjamkeluar = [];
		$absen = $this->models->cekabsenkaryawan($id, $tgl);
		foreach ($absen as $cekabsen) {
			$cektgl[] = $cekabsen->tgl;
			$cekjammasuk[] = $cekabsen->jammasuk;
			$cekid[] = $cekabsen->idkaryawan;
			$cekjamkeluar[] = $cekabsen->jamkeluar;
		}
		
		if ($cek) {
			foreach ($cek as $kar) {

				if (isset($kar->idkartu) && isset($cektgl[0]) == null && $cekjammasuk == null) {

					if ($date->format('H:i:s') > '08:10:00') {
						$telat = 1;
					} else {
						$telat = null;
					}
					$data	=	array(
						'idkaryawan' => $kar->idkartu,
						'bulan'		 => $date->format('m'),
						'tgl'		 =>	$date->format('Y-m-d'),
						'jammasuk'	 =>	$date->format('H:i:s'),
						'telat'		 =>	$telat
					);

					$this->models->insert('absen', $data);

					$output = array(
						"status" => TRUE, "alert" => 'success', "data" => $kar->namakaryawan, "title" => 'Success Abasen Masuk'
					);
				} elseif (isset($kar->idkartu) == $cekid[0] && $cektgl[0] == $date->format('Y-m-d') && $date->format('H:i:s') < '12:00:00') {

					$output = array(
						"status" => true, "alert" => 'success', "data" => $kar->namakaryawan, "title" => 'Sudah Abasen Masuk'
					);
				} elseif (isset($kar->idkartu) == $cekid[0] && $cektgl[0] == $date->format('Y-m-d') && $date->format('H:i:s') > '12:00:00' && $cekjamkeluar[0] == null) {
					$data	=	array(
						'idkaryawan' => $kar->idkartu,
						'bulan'		 => $date->format('m'),
						'tgl'		 =>	$date->format('Y-m-d'),
						'jamkeluar'	 =>	$date->format('H:i:s')
					);

					$where = array(
						'idkaryawan' => $cekid[0],
						'tgl'		=> $date->format('Y-m-d')
					);

					$this->models->update('absen', $data, $where);

					$output = array(
						"status" => TRUE, "alert" => 'success', "data" => $kar->namakaryawan, "title" => 'Success Asben Pulang'
					);
				} elseif (isset($kar->idkartu) == $cekid[0] && $cektgl[0] == $date->format('Y-m-d') && $date->format('H:i:s') > '12:00:00' && $cekjamkeluar[0] != null) {
					$output = array(
						"status" => TRUE, "alert" => 'success', "data" => $kar->namakaryawan, "title" => 'Sudah Asben Pulang'
					);
				} else {
					$output = array(
						"status" => false, "alert" => 'warning', "data" => null, "title" => 'Warning!!'
					);
				}
			}
		} else {
			$output = array(
				"status" => false, "alert" => 'error', "data" => 'Karyawan Tidak Terdaftar', "title" => 'Warning!!'
			);
		}
		echo json_encode($output);
	}
}
