<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <button type="button" id="pdfdesa" class="btn btn-primary" data-toggle="modal" data-target="#modal-pelanggan" title="Tambah Data Area" > Tambah
            </button>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table width="100%" id="tbpelanggan" class="table table-striped table-bordered table-sm">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Pelanggan</th>
                    <th>Alamat </th>
                    <th>Area</th>
                    <th>No. Telp</th>
                    <th>Paket</th>
                    <th>Sales</th>
										<th>Status</th>
										<th>Tgl. Berhenti</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<div class="modal fade" id="modal-pelanggan">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Pelanggan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-6">
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-3 col-form-label">Nama Pelanggan</label>
                  <div class="col-sm-9">
                    <input type="text" name="namapelanggan" class="form-control" id="namapelanggan" placeholder="Nama Pelanggan">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputPassword3" class="col-sm-3 col-form-label">Alamat</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="alamatpelanggan" name="alamatpelanggan" placeholder="Alamat">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputPassword3" class="col-sm-3 col-form-label">No. Telp</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="notelp" name="notelp" placeholder="Nomor Telpon">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputPassword3" class="col-sm-3 col-form-label">Area</label>
                  <div class="col-sm-9">
                    <select class="form-control select2" id="areapelanggan" name="areapelanggan">
                      <option value=""> --- Pilih Area --- </option>
                      <?php foreach ($data as $key) : ?>
                        <option value="<?php echo $key->idarea; ?>"><?php echo $key->namaarea; ?></option>
                      <?php endforeach; ?>

                    </select>
                  </div>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group row">
                  <label for="inputPassword3" class="col-sm-3 col-form-label">Paket</label>
                  <div class="col-sm-9">
                    <select class="form-control select2" id="paketpelanggan" name="paketpelanggan">
                      <option value=""> --- Pilih Paket --- </option>
                      <?php foreach ($paket as $key) : ?>
                        <option value="<?php echo $key->idpaket; ?>"><?php echo $key->namapaket; ?></option>
                      <?php endforeach; ?>

                    </select>
                  </div>
                </div>
                <div class="postx">

                </div>
                <div class="form-group row">
                  <label for="inputPassword3" class="col-sm-3 col-form-label">Sales</label>
                  <div class="col-sm-9">
                    <select class="form-control select2" id="sales" name="sales">
                      <option value=""> --- Pilih Sales --- </option>
                      <?php foreach ($sales as $key) : ?>
                        <option value="<?php echo $key->idkaryawan; ?>"><?php echo $key->namakaryawan; ?></option>
                      <?php endforeach; ?>

                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="button" id="simpan_pelanggan" class="btn btn-primary simpan_pelanggan">Simpan</button>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal-edit-pelanggan">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Pelanggan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-6">
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-3 col-form-label">ID</label>
                  <div class="col-sm-9">
                    <input type="text" type="hidden" name="idpelanggan" class="form-control" id="idpelanggan" placeholder="ID" readonly="true">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-3 col-form-label">Nama Pelanggan</label>
                  <div class="col-sm-9">
                    <input type="text" name="editnamapelanggan" class="form-control" id="editnamapelanggan" placeholder="Nama Paket">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputPassword3" class="col-sm-3 col-form-label">Alamat</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="editalamatpelanggan" name="editalamatpelanggan" placeholder="Harga">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputPassword3" class="col-sm-3 col-form-label">No. Telp</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="editnotelp" name="editnotelp" placeholder="Nomor Telpon">
                  </div>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group row">
                  <label for="inputPassword3" class="col-sm-3 col-form-label">Area</label>
                  <div class="col-sm-9">
                    <select class="form-control select2" id="editareapelanggan" name="editareapelanggan">
                      <option value=""> --- Pilih Area --- </option>
                      <?php foreach ($data as $key) : ?>
                        <option value="<?php echo $key->idarea; ?>"><?php echo $key->namaarea; ?></option>
                      <?php endforeach; ?>

                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputPassword3" class="col-sm-3 col-form-label">Paket</label>
                  <div class="col-sm-9">
                    <select class="form-control select2" id="editpaketpelanggan" name="editpaketpelanggan">
                      <option value=""> --- Pilih Paket --- </option>
                      <?php foreach ($paket as $key) : ?>
                        <option value="<?php echo $key->idpaket; ?>"><?php echo $key->namapaket; ?></option>
                      <?php endforeach; ?>

                    </select>
                  </div>
                </div>
                <div class="postxedit">

                </div>
                <div class="form-group row">
                  <label for="inputPassword3" class="col-sm-3 col-form-label">Sales</label>
                  <div class="col-sm-9">
                    <select class="form-control select2" id="editsales" name="editsales">
                      <option value=""> --- Pilih Sales --- </option>
                      <?php foreach ($sales as $key) : ?>
                        <option value="<?php echo $key->idkaryawan; ?>"><?php echo $key->namakaryawan; ?></option>
                      <?php endforeach; ?>

                    </select>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </form>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="button" class="btn btn-primary simpan_pelanggan_edit">Simpan</button>
        </div>
      </div>
    </div>
  </div>
</div>
