<section class="content">
  <div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-lg-3">
        <?php foreach ($pelanggan as $key) : ?>
          <div class="col-lg-12 col-12">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3><?= $key->jum ?></h3>

                <p>Pelanggan</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
            </div>
          </div>
        <?php endforeach; ?>
        <!-- ./col -->
        <?php foreach ($tagihan as $key) : ?>
          <div class="col-lg-12 col-12">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>Rp. <?= number_format($key->sumtagihan,0,',','.') ?></h3>

                <p>Tagihan</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
      <div class="col-lg-3">
        <!-- ./col -->
        <?php foreach ($bayar as $key) : ?>
          <div class="col-lg-12 col-12">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>Rp. <?= number_format($key->sumbayar,0,',','.') ?></h3>

                <p>Pembayaran</p>
              </div>
              <div class="icon">

                <i class="ion ion-bag"></i>
              </div>
            </div>
          </div>
        <?php endforeach; ?>
        <!-- ./col -->
        <?php foreach ($kurang as $key) : ?>
          <div class="col-lg-12 col-12">
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>Rp. <?= number_format($key->sumkurangbayar,0,',','.')?></h3>

                <p>Kurang Bayar</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
      <div class="col-lg-6">
        <div class="col-lg-12 col-12">
          <figure class="highcharts-figure" >
            <div id="container2"></div>
          </figure>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 col-12">
        <div class="grid-margin" >
          <div class="card">
            <div class="card-body">
              <figure class="highcharts-figure">
                <div id="container"></div>
              </figure>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>