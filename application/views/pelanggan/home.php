    <div class="content-wrapper">
      <div class="content-header">
      </div>
      <div class="content">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">

              <div class="card card-primary card-outline">
                <div class="card-header">
                  <h5 class="card-title m-0">Data Tagihan dan Pembayaran</h5>
                </div>
                <div class="card-body">
                 <div class="table-responsive">
                  <table width="100%" id="datapelanggan" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>ID Billing</th>
                        <th>Bulan</th>
                        <th>Tahun</th>
                        <th>Paket</th>
                        <th>Tagihan</th>
                        <th>Tgl. Pembayaran</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
