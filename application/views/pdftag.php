<table border="">
	<tr>
		<td rowspan="" style="text-align: center;" width="200px"><img src="<?php echo base_url() ?>assets/dist/img/amc.png" width="125px" alt="AdminLTE Logo"></td>
		<td>
			<p style="font-size: 18px"><b>ATOOM MEDIA CONNECT</b></p>
			<p style="font-size: 12px">Jalan Yogya-Wonosari Km. 17,5 Patuk Gunungkidul Yogyakarta 55862</p>
			<p style="font-size: 12px">Telp : 0852-2522-5959 (CS) Email : atomedia_mail@yahoo.com</p>
		</td>
	</tr>
</table>
<hr>

<h3 style="text-align: center;">Data Pelanggan Belum Terbuat Tagihan </h3>

<h4>Bulan : <?= $bulan ?></h4>
<table width="100%" cellspacing="0" cellpadding="3" style="font-size: 10pt;" border="1">
	<thead>
		<tr>
			<th>No</th>
			<th>Nama Pelanggan</th>
			<th>Alamat</th>
			<th>Area</th>
			<th>Tgl. daftar</th>
			<th>Tgl. Berhenti</th>
			<th>Jumlah (Rp.)</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
		foreach ($data as $key) : ?>
			?>
			<tr>
				<td style="text-align: center;"><?= $no++ ?></td>
				<td><?= $key->namapelanggan ?></td>
				<td><?= $key->alamatpelanggan ?></td>
				<td><?= $key->namaarea ?></td>
				<td><?= $key->tgldaftar ?></td>
				<td><?= $key->tglberhenti ?></td>
				<td style="text-align: right;"><?= number_format($key->hargapelanggan, 0, ',', '.') ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="6" style="text-align: center;"><b>TOTAL</b></td>
			<td style="text-align: right;"><b><?= number_format($totalharga, 0, ',', '.') ?></b></td>
		</tr>
	</tfoot>
</table>
