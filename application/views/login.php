<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>AMC | Log in</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/x-icon" href="<?= base_url('assets/dist/img/amc.ico') ?>">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/AdminLTE3/plugins/fontawesome-free/css/all.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- icheck bootstrap -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/AdminLTE3/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/AdminLTE3/dist/css/adminlte.min.css">
	<link href="<?php echo base_url('assets/AdminLTE3/plugins/iCheck/square/blue.css') ?>" rel="stylesheet" type="text/css" />
	<!-- Google Font: Source Sans Pro -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/custom.css">
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition login-page">
	<div class="login-box">

		<div class="card">
			<div class="card-body login-card-body">
				<div class="login-logo">
					<!-- <span class="brand-text font-weight-light" style="font-size: 25px; color: blue"><b>ATOOM MEDIA CONNECT</b></span> -->
					<img src="<?php echo base_url() ?>assets/dist/img/amc.png" width="280px" alt="AdminLTE Logo">
				</div>
				<!-- <p class="login-box-msg">Sign in to start your session</p> -->
				<?php if ($this->session->flashdata('error') != null) {  ?>
					<div class="alert alert-warning">
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php }; ?>
				<?php if ($this->session->flashdata('success') != null) {  ?>
					<div class="alert alert-success">
						<?php echo $this->session->flashdata('success') ?>
					</div>
				<?php }; ?>
				<?php if ($this->session->flashdata('info') != null) {  ?>
					<div class="alert alert-info">
						<?php echo $this->session->flashdata('info') ?>

					</div>
				<?php }; ?>
				<form action="<?php echo site_url('pengguna/index') ?>" method="post">
					<div class="input-group mb-3">
						<input type="text" class="form-control" name="username" placeholder="Username" required="">
						<div class="input-group-append">
							<div class="input-group-text">
								<span class="fas fa-user"></span>
							</div>
						</div>
					</div>
					<div class="input-group mb-3">
						<input type="password" class="form-control" name="password" placeholder="Password" required="">
						<div class="input-group-append">
							<div class="input-group-text">
								<span class="fas fa-lock"></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-8">
							<div class="icheck-primary">
								<input type="checkbox" id="remember">
								<label for="remember">
									Remember Me
								</label>
							</div>
						</div>
						<div class="col-4">
							<button type="submit" class="btn btn-primary btn-block">Sign In</button>
						</div>
					</div>
				</form>

			</div>
		</div>
	</div>

	<!-- jQuery -->
	<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- AdminLTE App -->
	<script src="<?php echo base_url() ?>assets/AdminLTE3/dist/js/adminlte.min.js"></script>
	<script src="<?php echo base_url('assets/AdminLTE3/plugins/iCheck/icheck.min.js') ?>" type="text/javascript"></script>

	<script>
		$(function() {
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue',
				increaseArea: '20%' // optional
			});

			window.setTimeout(function() {
				$(".alert").fadeTo(1000, 0).slideUp(1000, function() {
					$(this).remove();
				});
			}, 3000);
		});
	</script>

</body>

</html>
