<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Presensi</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/AdminLTE3/plugins/fontawesome-free/css/all.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Tempusdominus Bbootstrap 4 -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/AdminLTE3/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/AdminLTE3/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	<!-- JQVMap -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/AdminLTE3/plugins/jqvmap/jqvmap.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/AdminLTE3/dist/css/adminlte.min.css">
	<!-- overlayScrollbars -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/AdminLTE3/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/AdminLTE3/plugins/daterangepicker/daterangepicker.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/dist/css/toastr.min.css">
	<!-- Select2 -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/AdminLTE3/plugins/select2/css/select2.min.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/AdminLTE3/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
	<!-- summernote -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/AdminLTE3/plugins/summernote/summernote-bs4.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/AdminLTE3/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/AdminLTE3/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
	<!-- bootstrap datepicker -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/sweetalert/dist/sweetalert.css">

	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition layout-top-nav">
	<div class="wrapper">

		<div class="content-wrapper">
			<div class="content-header">
			</div>
			<div class="content">
				<div class="container">
					<div class="col">
						<div id="timestamp" class="text-center" style="font-size: 300px; color:blue; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;">
						</div>
					</div>
					<div class="text-center">
						<h1>
							Selamat Datang
							<br>
							Jangan Lupa Absen Broo !!
						</h1>
					</div>

					<div class="form-group">
						<input type="number" style="opacity: 0;" name="karyawan" class="form-control" id="karyawan" onchange="absen()" placeholder="Tuliskan " value="" autocomplete="off">
					</div>

				</div>
			</div>
		</div>

		<!-- Main Footer -->
		<footer class="main-footer" style="text-align: center;">
			<!-- Default to the left -->
			<strong>Copyright &copy; 2021 <a href="https://mervia.my.id" target="_blank">MERVIA</a>.</strong> All rights reserved.
		</footer>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery -->
	<script src="<?= base_url() ?>assets/AdminLTE3/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="<?= base_url() ?>assets/AdminLTE3/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- AdminLTE App -->
	<script src="<?= base_url() ?>assets/AdminLTE3/dist/js/adminlte.min.js"></script>
	<script src="<?= base_url('assets/js'); ?>/toastr.min.js"></script>
	<script>
		var site_url = '<?= site_url(); ?>';
	</script>
	<!-- DataTables -->
	<script src="<?= base_url() ?>assets/AdminLTE3/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="<?= base_url() ?>assets/AdminLTE3/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
	<script src="<?= base_url() ?>assets/AdminLTE3/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
	<script src="<?= base_url() ?>assets/AdminLTE3/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="<?= base_url() ?>assets/sweetalert/dist/sweetalert.min.js"></script>

	<style>
		span {
			display: none;
		}
	</style>
	<script>
		$('#karyawan').focus();

		// Function ini dijalankan ketika Halaman ini dibuka pada browser
		$(function() {
			setInterval(timestamp, 1000); //fungsi yang dijalan setiap detik, 1000 = 1 detik
		});

		//Fungi ajax untuk Menampilkan Jam dengan mengakses File ajax_timestamp.php
		function timestamp() {
			$.ajax({
				url: site_url + "absen/jam",
				success: function(data) {
					$('#timestamp').html(data);
				},
			});
		}

		function absen() {
			var id = document.getElementById("karyawan").value;
			if (id != '') {
				$.ajax({
					type: 'POST',
					url: site_url + "absen/input",
					dataType: 'JSON',
					data: {
						id: id
					},
					success: function(data, status) {
						$('#karyawan').val('').focus();

						if (data.status == false) {
							var audio = new Audio(site_url + 'assets/dist/audio/notif.mp3');
						} else if (data.title == 'Sudah Abasen Masuk') {
							var audio = new Audio(site_url + 'assets/dist/audio/sky-blip-2.mp3');
						} else if (data.title == 'Sudah Asben Pulang') {
							var audio = new Audio(site_url + 'assets/dist/audio/sky-blip-2.mp3');
						} else {
							var audio = new Audio(site_url + 'assets/dist/audio/gun.mp3');
						}
						audio.play();
						swal({
							text: data.title,
							title: data.data,
							type: data.alert,
							showCancelButton: false,
							showConfirmButton: false,
							timer: 4000
						}, function() {
							swal.close();

						});
					}
				})
			}
		}
	</script>
</body>

</html>
