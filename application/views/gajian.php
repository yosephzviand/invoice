<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <button type="button" id="pdfdesa" class="btn btn-primary" data-toggle="modal" data-target="#modal-gajian" title="Tambah Data Area" > Tambah
            </button>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table width="100%" id="tbgajian" class="table table-striped table-bordered table-sm">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Karyawan</th>
                    <th>Bulan</th>
                    <th>Tahun</th>
                    <th>Gaji Pokok</th>
                    <th>Tambahan</th>
                    <th>Potongan</th>
                    <th>Take Home Pay</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<div class="modal fade" id="modal-gajian">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Gaji Karyawan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-6">
                <div class="form-group row">
                  <label for="inputPassword3" class="col-sm-3 col-form-label">Jenis</label>
                  <div class="col-sm-9">
                    <select class="form-control select2" name="jnskar" id="jnskar" style="width: 100%;">
                     <option >Pilih Jenis</option>
                     <?php foreach ($jenis as $key) : ?>
                      <option value="<?php echo $key->idjenis; ?>"><?php echo $key->namajenis; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-3 col-form-label">Nama </label>
                <div class="col-sm-9">
                  <select class="form-control select2" name="namakaryawan" id="namakaryawan" style="width: 100%;">
                    <!-- <option value="">Pilih Karyawan</option>
                    <?php foreach ($karyawan as $key) : ?>
                      <option value="<?php echo $key->idkaryawan; ?>"><?php echo $key->namakaryawan; ?></option>
                    <?php endforeach; ?> -->
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-3 col-form-label">Bulan</label>
                <div class="col-sm-9">
                  <select class="form-control select2 bulan" name="bulan" id="bulan" style="width: 100%;">
                    <option value="">Pilih Bulan</option>
                    <option value="01">Januari</option>
                    <option value="02">Februari</option>
                    <option value="03">Maret</option>
                    <option value="04">April</option>
                    <option value="05">Mei</option>
                    <option value="06">Juni</option>
                    <option value="07">Juli</option>
                    <option value="08">Agustus</option>
                    <option value="09">September</option>
                    <option value="10">Oktober</option>
                    <option value="11">November</option>
                    <option value="12">Desember</option>
                  </select>
                </div>
              </div>
              <div class="postxgaji">
                
              </div>
              
            </div>
            <div class="col-lg-6">
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-3 col-form-label">Tambahan</label>
                <div class="col-sm-9">
                  <input type="text" name="tunjangan" class="form-control" id="tunjangan" placeholder="Tunjangan" onkeyup="sumgaji();">
                </div>
              </div>
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-3 col-form-label">Potongan</label>
                <div class="col-sm-9">
                  <input type="text" name="potongan" class="form-control" id="potongan" placeholder="Potongan" onkeyup="sumgaji();">
                </div>
              </div>

              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-3 col-form-label">Gaji Bersih</label>
                <div class="col-sm-9">
                  <input type="text" name="gajibersih" class="form-control" id="gajibersih" placeholder="Gaji Bersih" readonly="" onkeyup="sumgaji()">
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div class="modal-footer justify-content-between">
      <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      <button type="button" class="btn btn-primary simpan_gajian">Simpan</button>
    </div>
  </div>
</div>
</div>


<div class="modal fade" id="modal-edit-gajian">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Gaji Karyawan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-6">
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-3 col-form-label">ID</label>
                  <div class="col-sm-9">
                    <input type="text" name="idgajian" class="form-control" id="idgajian" placeholder="ID" readonly="true">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-3 col-form-label">Nama </label>
                  <div class="col-sm-9">
                   <input type="text" name="editnamakaryawan" class="form-control" id="editnamakaryawan" placeholder="Nama Karyawan" readonly="true">
                 </div>
               </div>
               <div class="form-group row">
                <label for="inputEmail3" class="col-sm-3 col-form-label">Tahun</label>
                <div class="col-sm-9">
                  <input type="text" name="edittahun" class="form-control" id="edittahun" placeholder="Tahun">
                </div>
              </div>
              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-3 col-form-label">Bulan</label>
                <div class="col-sm-9">
                  <select class="form-control select2 bulan" name="editbulan" id="editbulan" style="width: 100%;">
                    <option value="">Pilih Bulan</option>
                    <option value="1">Januari</option>
                    <option value="2">Februari</option>
                    <option value="3">Maret</option>
                    <option value="4">April</option>
                    <option value="5">Mei</option>
                    <option value="6">Juni</option>
                    <option value="7">Juli</option>
                    <option value="8">Agustus</option>
                    <option value="9">September</option>
                    <option value="10">Oktober</option>
                    <option value="11">November</option>
                    <option value="12">Desember</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-3 col-form-label">Gaji Pokok</label>
                <div class="col-sm-9">
                  <input type="text" name="editgajipokok" class="form-control" id="editgajipokok" placeholder="Gaji Pokok" readonly="" onkeyup="sumgajiedit();">
                </div>
              </div>
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-3 col-form-label">Tunjangan</label>
                <div class="col-sm-9">
                  <input type="text" name="edittunjangan" class="form-control" id="edittunjangan" placeholder="Tunjangan" onkeyup="sumgajiedit();">
                </div>
              </div>
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-3 col-form-label">Potongan</label>
                <div class="col-sm-9">
                  <input type="text" name="editpotongan" class="form-control" id="editpotongan" placeholder="Potongan" onkeyup="sumgajiedit();">
                </div>
              </div>

              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-3 col-form-label">Gaji Bersih</label>
                <div class="col-sm-9">
                  <input type="text" name="editgajibersih" class="form-control" id="editgajibersih" placeholder="Gaji Bersih" readonly="" onkeyup="sumgajiedit()">
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div class="modal-footer justify-content-between">
      <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      <button type="button" class="btn btn-primary simpan_gajian_edit">Simpan</button>
    </div>
  </div>
</div>
</div>