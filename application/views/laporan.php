<section class="content">
	<div class="container-fluid">
		<div class="card card-primary card-outline">
			<div class="card-header">
				<h3 class="card-title">
					<i class="fas fa-edit"></i>
					Laporan dan Pembukuan
				</h3>
			</div>
			<div class="card-body">
				<!-- <h4>Left Sided</h4> -->
				<div class="row">
					<div class="col-5 col-sm-3">
						<div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
							<a class="nav-link active" id="vert-tabs-home-tab" data-toggle="pill" href="#vert-tabs-home" role="tab" aria-controls="vert-tabs-home" aria-selected="true">Cetak Tagihan</a>
							<a class="nav-link" id="vert-tabs-profile-tab" data-toggle="pill" href="#vert-tabs-profile" role="tab" aria-controls="vert-tabs-profile" aria-selected="false">Cetak Piutang</a>
							<a class="nav-link" id="vert-tabs-messages-tab" data-toggle="pill" href="#vert-tabs-messages" role="tab" aria-controls="vert-tabs-messages" aria-selected="false">Cetak Pelanggan</a>
							<a class="nav-link" id="vert-tabs-settings-tab" data-toggle="pill" href="#vert-tabs-settings" role="tab" aria-controls="vert-tabs-settings" aria-selected="false">Cetak Gajian</a>
						</div>
					</div>
					<div class="col-7 col-sm-9">
						<div class="tab-content" id="vert-tabs-tabContent">
							<div class="tab-pane text-left fade show active" id="vert-tabs-home" role="tabpanel" aria-labelledby="vert-tabs-home-tab">
								<form name="ipladder" id="ipladder" method="post">
									<div class="form-group row">
										<div class="col-sm-4 col-6">
											<div class="input-group date">
												<input name="datepicker" type="text" class="form-control pull-right" id="datepicker" placeholder="Masukkan Tanggal" required="true" >
											</div>
										</div>
										<label class="col-form-label"> s/d</label>
										<div class="col-sm-4 col-6">
											<div class="input-group date">
												<input name="datepicker1" type="text" class="form-control pull-right" id="datepicker1" placeholder="Masukkan Tanggal" required="true" >
											</div>
										</div>
									</form>
									<!-- <div class="col-sm-4 col-6"> -->
										<button type="button" id="lappdftagihan" class="btn btn-primary" data-card-widget="Filter" data-toggle="tooltip" >
											<i class="fas fa-file-pdf"></i> PDF
										</button>
									<!-- </div> -->
								</div>
							</div>
							<div class="tab-pane text-left fade show active" id="vert-tabs-pembayaran" role="tabpanel" aria-labelledby="vert-tabs-home-tab">
								<form name="ipladder" id="ipladder" method="post">
									<div class="form-group row">
										<div class="col-sm-4 col-6">
											<div class="input-group date">
												<input name="datepicker" type="text" class="form-control pull-right" id="datepicker" placeholder="Masukkan Tanggal" required="true" >
											</div>
										</div>
										<label class="col-form-label"> s/d</label>
										<div class="col-sm-4 col-6">
											<div class="input-group date">
												<input name="datepicker1" type="text" class="form-control pull-right" id="datepicker1" placeholder="Masukkan Tanggal" required="true" >
											</div>
										</div>
									</form>
									<!-- <div class="col-sm-4 col-6"> -->
										<button type="button" id="lappdftagihan" class="btn btn-primary" data-card-widget="Filter" data-toggle="tooltip" >
											<i class="fas fa-file-pdf"></i> PDF
										</button>
									<!-- </div> -->
								</div>
							</div>
							<div class="tab-pane fade" id="vert-tabs-profile" role="tabpanel" aria-labelledby="vert-tabs-profile-tab">
								<form name="ipladder1" id="ipladder1" method="post">
									<div class="form-group row">
										<div class="col-sm-2 col-6">
											<select class="form-control select2" name="laptahun" id="laptahun" >
												<option >Pilih Tahun</option>
												<?php
												for ($i = date('Y'); $i >= 2021; $i--) {
													?>
													<option value="<?php echo $i; ?>">Tahun <?php echo $i; ?></option>
													<?php
												}
												?>
											</select>
										</div>
										<div class="col-sm-4 col-6">
											<select class="form-control select2 " name="lapbulan" id="lapbulan" >
												<option value="">Pilih Semua</option>
												<option value="01">Januari</option>
												<option value="02">Februari</option>
												<option value="03">Maret</option>
												<option value="04">April</option>
												<option value="05">Mei</option>
												<option value="06">Juni</option>
												<option value="07">Juli</option>
												<option value="08">Agustus</option>
												<option value="09">September</option>
												<option value="10">Oktober</option>
												<option value="11">November</option>
												<option value="12">Desember</option>
											</select>
										</div>
										<div class="col-sm-4 col-6">
											<button type="button" id="lappdfbelumbayar" class="btn btn-primary" data-card-widget="Filter" data-toggle="tooltip" >
												<i class="fas fa-file-pdf"></i> PDF
											</button>
										</div>
									</form>
								</div>
							</div>
							<div class="tab-pane fade" id="vert-tabs-messages" role="tabpanel" aria-labelledby="vert-tabs-messages-tab">
								<form name="ipladder2" id="ipladder2" method="post">
									<div class="form-group row">
										<div class="col-sm-4 col-6">
											<select class="form-control select2" name="arealap" id="arealap" >
												<option value="">Pilih Semua</option>
												<?php
												foreach ($data as $key )  {
													?>
													<option value="<?php echo $key->idarea; ?>"><?php echo $key->namaarea; ?></option>
													<?php
												}
												?>
											</select>
										</div>
									</form>
									<div class="col-sm-4 col-6">
										<button type="button" id="cetakpelanggan" class="btn btn-primary" data-card-widget="Filter" data-toggle="tooltip" >
											<i class="fas fa-file-pdf"></i> PDF
										</button>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="vert-tabs-settings" role="tabpanel" aria-labelledby="vert-tabs-settings-tab">
												lll
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
