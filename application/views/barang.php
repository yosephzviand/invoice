<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <button type="button" id="pdfdesa" class="btn btn-primary" data-toggle="modal" data-target="#modal-inp-barang" title="Tambah Data Area" > Tambah
            </button>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table width="100%" id="tbbarang" class="table table-striped table-bordered table-sm">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama </th>
                    <th>Alamat </th>
                    <th>Area</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<div class="modal fade" id="modal-inp-barang">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Barang</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="card-body">
            <div class="row">               
              <div class="col-lg-6 col-12">
                <form  id="pembelian" method="POST">
                  <!-- <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Tanggal Penjualan</label>
                    <div class="col-sm-8">
                      <input type="text" name="tglpenjualan" class="form-control" id="datepicker" placeholder="Tanggal Keluar" >
                    </div>
                  </div> -->

                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 col-form-label">Pelanggan</label>
                    <div class="col-sm-9">
                      <select class="form-control select2" name="barangkeluar" id="barangkeluar" style="width: 100%;">
                        <option value="" >Pilih Pelanggan</option>
                        <?php foreach ($pel as $key) : ?>
                          <option value="<?php echo $key->idpelanggan; ?>"><?php echo $key->namapelanggan; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 col-form-label">Nama Barang</label>
                    <div class="col-sm-9">
                      <input type="text" name="namabarang" class="form-control" id="namabarang" placeholder="Nama Barang">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 col-form-label">Serial</label>
                    <div class="col-sm-9">
                      <input type="text" name="serial" class="form-control" id="serial" placeholder="Serial Number">
                    </div>
                  </div>

                  
                </div>
                <div class="col-lg-6 col-12">

                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 col-form-label">Jumlah</label>
                    <div class="col-sm-9">
                      <input type="text" name="jumlahbarang" class="form-control" id="jumlahbarang" placeholder="Jumlah Barang">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 col-form-label">Harga Beli</label>
                    <div class="col-sm-9">
                      <input type="text" name="hargabeli" class="form-control" id="hargabeli" placeholder="Harga Beli">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 col-form-label"></label>
                    <div class="col-sm-9">
                      <button type="button" class="btn btn-primary simpan_keluar">Tambah Barang</button>
                    </div>
                  </div>

                </div>
              </form>
              <div class="table-responsive">
                <table width="100%"  class="table table-striped table-sm">
                  <thead>
                    <tr>
                      <th>Nama Barang </th>
                      <th>Serial</th>
                      <th>Jumlah</th>
                      <th>Harga</th>
                      <th>Harga Total</th>
                    </tr>
                  </thead>
                  <tbody id="tb_pembelian">

                  </tbody>
                </table>
                <button class="btn btn-success float-right bt_simpan_entry" type="button"><i class="fa fa-save"></i> Simpan</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_form_layanan01" role="dialog">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title">Detail</h3>
      </div>
      <div class="modal-body form">
        <div class="container-fluid">
          <div class="table-responsive">
            <table width="100%" id="detail_rpt01" class="table table-striped table-bordered table-sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nama Barang</th>
                  <th>Serial</th>
                  <th>Jumlah</th>
                  <th>Harga</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal-edit-barang">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Barang</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="card-body">
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-3 col-form-label">ID</label>
              <div class="col-sm-9">
                <input type="text" name="idbarang" class="form-control" id="idbarang" placeholder="ID" readonly="true">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-3 col-form-label">Nama Barang</label>
              <div class="col-sm-9">
                <input type="text" name="editnamabarang" class="form-control" id="editnamabarang" placeholder="Nama Barang">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-3 col-form-label">Serial</label>
              <div class="col-sm-9">
                <input type="text" name="editserial" class="form-control" id="editserial" placeholder="Serial Number">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-3 col-form-label">Jumlah</label>
              <div class="col-sm-9">
                <input type="text" name="editjumlahbarang" class="form-control" id="editjumlahbarang" placeholder="Jumlah Barang" onkeyup="sumbarang()">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-3 col-form-label">Harga Beli</label>
              <div class="col-sm-9">
                <input type="text" name="edithargabeli" class="form-control" id="edithargabeli" placeholder="Harga Beli" onkeyup="sumbarang()">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-3 col-form-label">Total Harga</label>
              <div class="col-sm-9">
                <input type="text" name="total" class="form-control" id="total" placeholder="Total Harga" readonly="" onkeyup="sumbarang()">
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary simpan_barang_edit">Simpan</button>
      </div>
    </div>
  </div>
</div>

