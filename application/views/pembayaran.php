<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <!-- <div class="card-header"> -->
            <!-- <button type="button" id="pdfdesa" class="btn btn-primary" data-toggle="modal" data-target="#modal-tagihan" title="Tambah Data Area" > Tambah -->
              <!-- </button> -->
              <!-- </div> -->
              <div class="card-body">
                <div class="table-responsive">
                  <table width="100%" id="tbpembayaran" class="table table-striped table-bordered table-sm">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>ID Billing</th>
                        <th>Nama Pelanggan</th>
                        <th>Alamat</th>
                        <th>Bulan</th>
                        <th>Tahun</th>
                        <th>Paket</th>
                        <th>Tagihan</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <div class="modal fade" id="modal-pembayaran">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Pembayaran Tagihan</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="form-horizontal">
              <div class="card-body">
                <div class="form-group row">

                  <label for="inputEmail3" class="col-sm-3 col-form-label">ID Billing</label>
                  <div class="col-sm-9">
                    <input type="hidden" name="idtransaksi" class="form-control" id="idtransaksi" placeholder="ID" readonly="true">
                    <input type="text" name="idbilling" class="form-control" id="idbilling" placeholder="ID" readonly="true">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-3 col-form-label">Nama Pelanggan</label>
                  <div class="col-sm-9">
                    <input type="text" name="namapelanggan" class="form-control" id="namapelanggan" placeholder="Tagihan" readonly="true">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputPassword3" class="col-sm-3 col-form-label">Bulan</label>
                  <div class="col-sm-9">
                    <input type="text" name="bulan" class="form-control" id="bulan" placeholder="Bulan" readonly="true">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputPassword3" class="col-sm-3 col-form-label">Paket</label>
                  <div class="col-sm-9">
                   <input type="text" name="paket" class="form-control" id="paket" placeholder="Paket" readonly="true">
                 </div>
               </div>
               <div class="form-group row">
                <label for="inputPassword3" class="col-sm-3 col-form-label">Jumlah Tagihan</label>
                <div class="col-sm-9">
                  <input type="text" name="tagihan" class="form-control" id="tagihan" placeholder="Tagihan" readonly="true">
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="button" class="btn btn-primary simpan_pembayaran">Bayar</button>
        </div>
      </div>
    </div>
  </div>


  <div class="modal fade" id="modal-hapus-pembayaran">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Konfirmasi</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form class="form-horizontal">
            <div class="card-body">
              <div class="form-group row">
                <!-- <label for="inputEmail3" class="col-sm-2 col-form-label">ID</label> -->
                <div class="col-sm-10">
                 <input type="hidden" name="idtransaksi" class="form-control" id="idtransaksi" placeholder="ID" readonly="true">
                 <p>Apakah Yakin Akan Menghapus Pembayaran?</p>
               </div>
             </div>
           </div>
         </form>
       </div>
       <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary simpan_hapus_pembayaran">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-cetak-pembayaran">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Konfirmasi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="card-body">
           <form name="ipladder" id="ipladder" method="post">
            <div class="form-group row">
              <!-- <label for="inputEmail3" class="col-sm-2 col-form-label">ID</label> -->
              <div class="col-sm-10">
               <input type="hidden" name="idtransaksi" class="form-control" id="idtransaksi" placeholder="ID" readonly="true">
               <p>Apakah Akan Mencetak Pembayaran?</p>
             </div>
             
           </div>
         </form>
       </div>
     </form>
   </div>
   <div class="modal-footer justify-content-between">
    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
    <button type="button" class="btn btn-primary simpan_cetak_pembayaran" id="simpan_cetak_pembayaran">Ya</button>
  </div>
</div>
</div>
</div>