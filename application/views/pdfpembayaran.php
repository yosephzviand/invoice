<?php $no = 1;
foreach ($data as $key) : ?>
	<table border="" style="font-family: helvetica">
		<tr>
			<td rowspan="" style="text-align: center;" width="200px"><img src="<?php echo base_url() ?>assets/dist/img/amc.png" width="125px" alt="AdminLTE Logo"></td>
			<td>
				<p style="font-size: 18px"><b>ATOOM MEDIA CONNECT</b></p>
				<p style="font-size: 12px">Jalan Yogya-Wonosari Km. 17,5 Patuk Gunungkidul Yogyakarta 55862</p>
				<p style="font-size: 12px">Telp : 0852-2522-5959 (CS) Email : atomedia_mail@yahoo.com</p>
			</td>
		</tr>
	</table>

	<table width="100%">
		<tr>
			<td style="text-align: center; font-size: 15px; border-top: inset;border-bottom: inset;"><b>BUKTI PEMBAYARAN</b></td>
		</tr>
	</table>

	<table width="100%" border="" style="font-family: helvetica;font-size: 9pt;">
		<tr>
			<td width="100px">ID Billing</td>
			<td width="20px">: </td>
			<td> <?= $key->idbilling ?></td>
			<td width="150px"></td>
			<td width="50px">Nama</td>
			<td width="20px">: </td>
			<td><?= $key->namapelanggan ?></td>
		</tr>
		<tr>
			<td>Tgl. Bayar</td>
			<td>: </td>
			<td> <?= $key->tglpembayaran ?></td>
			<td></td>
			<td>Alamat</td>
			<td>: </td>
			<td><?= $key->alamatpelanggan ?></td>
		</tr>
	</table>

	<table width="100%" cellspacing="" cellpadding="3" border="1" style="font-family: helvetica; font-size: 9pt;">
		<thead>
			<tr>
				<th style="background-color: #6666ff; color: white">No</th>
				<th colspan="2" style="background-color: #6666ff; color: white">Keterangan Pembayaran</th>
				<th style="background-color: #6666ff; color: white">Jumlah (Rp. )</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td width="20px" style="text-align: center;"><?= $no++ ?> </td>
				<td colspan="2">Pembayaran Internet Bulan <?= $key->nmbulan . ' ' . $key->tahun ?></td>
				<td style="text-align: right;"><?= number_format($key->harga, 0, ',', '.') ?></td>
			</tr>
			<tr>
				<td colspan="3" style="text-align: center;"><b>TOTAL</b></td>
				<td style="text-align: right;"><b>Rp. <?= number_format($key->harga, 0, ',', '.') ?></b></td>
			</tr>
		</tbody>
	</table>
	<table width="100%" cellspacing="" cellpadding="3" border="" style="font-family: helvetica;font-size: 9pt;">
		<tr>
			<td colspan="2" width="300px"><b>Terbilang :</b></td>
			<!-- <td colspan="" style="text-align: ;"><b>Grand Total :  </b></td> -->
		</tr>
		<tr>
			<td colspan="2"> <?= $key->terbilang ?> Rupiah.</td>
			<!-- <td style="text-align: right;"><?= number_format($key->harga, 0, ',', '.') ?></td> -->
		</tr>
	</table>

	<!-- <br> -->
	<table width="100%" border="" style="font-family: helvetica; font-size: 9pt;">
		<tr>
			<td width="500px"></td>
			<td style="text-align: center;">Wonosari, <?= date('d-m-Y') ?></td>
		</tr>
		<tr>
			<td></td>
			<td style="text-align: center;">Mengetahui,</td>
		</tr>
		<tr>
			<td colspan="2" height="0px"></td>
		</tr>
		<tr>
			<td rowspan="2">
				<p>Catatan : </p>
				<p> - Disimpan sebagai bukti pembayaran yang SAH.</p>
				<p> - Uang yang sudah dibayarkan tidak dapat diminta kembali.</p>
			</td>
			<td style="text-align: center;">Adhi Wibowo</td>
		</tr>
	</table>
	<br>
	-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	<br>
	<!-- <br> -->
<?php endforeach; ?>
