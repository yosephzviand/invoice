<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <button type="button" id="pdfdesa" class="btn btn-primary" data-toggle="modal" data-target="#modal-tagihan" title="Tambah Data Area" > Tambah
            </button>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table width="100%" id="tbtagihan" class="table table-striped table-bordered table-sm">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>ID Billing</th>
                    <th>Nama Pelanggan</th>
                    <th>Alamat</th>
                    <th>Bulan</th>
                    <th>Tahun</th>
                    <th>Paket</th>
                    <th>Tagihan</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<div class="modal fade" id="modal-tagihan">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Tagihan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="card-body">
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-3 col-form-label">Nama Pelanggan</label>
              <div class="col-sm-9">
                <select class="form-control select2 pelanggan" name="pelanggan" id="pelanggan" style="width: 100%;">
                  <option >Pilih Pelanggan</option>
                  <?php foreach ($pelanggan as $key) : ?>
                    <option value="<?php echo $key->idpelanggan; ?>"><?php echo $key->namapelanggan; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword3" class="col-sm-3 col-form-label">Bulan</label>
              <div class="col-sm-9">
                <select class="form-control select2 bulan" name="bulan" id="bulan" style="width: 100%;">
                  <option >Pilih Bulan</option>
                  <option value="01">Januari</option>
                  <option value="02">Februari</option>
                  <option value="03">Maret</option>
                  <option value="04">April</option>
                  <option value="05">Mei</option>
                  <option value="06">Juni</option>
                  <option value="07">Juli</option>
                  <option value="08">Agustus</option>
                  <option value="09">September</option>
                  <option value="10">Oktober</option>
                  <option value="11">November</option>
                  <option value="12">Desember</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword3" class="col-sm-3 col-form-label">Tambahan</label>
              <div class="col-sm-9">
                <input type="text" name="tambahan" class="form-control" id="tambahan" placeholder="Jika Tidak Ada Tambahan Masukkan Nilai 0 (nol)" onkeyup="sum();">
              </div>
            </div>
            <div class="form-group row">
              <!-- <label for="inputEmail3" class="col-sm-2 col-form-label">Paket</label> -->
              <div class="col-sm-9">
                <input type="hidden" name="idpakettagihan" class="form-control" id="idpakettagihan" placeholder="Paket Pelanggan" readonly="true">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-3 col-form-label">Paket</label>
              <div class="col-sm-9">
                <input type="text" name="pakettagihan" class="form-control" id="pakettagihan" placeholder="Paket Pelanggan" readonly="true">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-3 col-form-label">Tagihan</label>
              <div class="col-sm-9">
                <input type="text" name="tagihan" class="form-control" id="tagihan" placeholder="Tagihan Pelanggan" onkeyup="sum();" readonly="true">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-3 col-form-label">Total Tagihan</label>
              <div class="col-sm-9">
                <input type="text" name="totaltagihan" class="form-control" id="totaltagihan" placeholder="Tagihan Pelanggan" onkeyup="sum();" readonly="true">
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary simpan_tagihan" onsubmit="return validatetagihan()">Simpan</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal-edit-tagihan">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Tagihan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="card-body">
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-3 col-form-label">ID</label>
              <div class="col-sm-9">
                <input type="text" type="hidden" name="idtagihan" class="form-control" id="idtagihan" placeholder="ID" readonly="true">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-3 col-form-label">Nama Pelanggan</label>
              <div class="col-sm-9">
                <input type="text" name="editnamapelanggan" class="form-control" id="editnamapelanggan" placeholder="Nama Pelanggan" readonly="true">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword3" class="col-sm-3 col-form-label">Bulan</label>
              <div class="col-sm-9">
                <select class="form-control select2 bulan" name="editbulan" id="editbulan" style="width: 100%;">
                  <option >Pilih Bulan</option>
                  <option value="1">Januari</option>
                  <option value="2">Februari</option>
                  <option value="3">Maret</option>
                  <option value="4">April</option>
                  <option value="5">Mei</option>
                  <option value="6">Juni</option>
                  <option value="7">Juli</option>
                  <option value="8">Agustus</option>
                  <option value="9">September</option>
                  <option value="10">Oktober</option>
                  <option value="11">November</option>
                  <option value="12">Desember</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword3" class="col-sm-3 col-form-label">Tambahan</label>
              <div class="col-sm-9">
                <input type="text" name="edittambahan" class="form-control" id="edittambahan" placeholder="Jika Tidak Ada Tambahan Masukkan Nilai 0 (nol)" onkeyup="sumedit();">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-3 col-form-label">Paket</label>
              <div class="col-sm-9">
                <input type="text" name="editpakettagihan" class="form-control" id="editpakettagihan" placeholder="Paket Pelanggan" readonly="true">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-3 col-form-label">Tagihan</label>
              <div class="col-sm-9">
                <input type="text" name="edittagihan" class="form-control" id="edittagihan" placeholder="Tagihan Pelanggan" onkeyup="sumedit();" readonly="true">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-3 col-form-label">Total Tagihan</label>
              <div class="col-sm-9">
                <input type="text" name="edittotaltagihan" class="form-control" id="edittotaltagihan" placeholder="Tagihan Pelanggan" onkeyup="sumedit();" readonly="true">
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary simpan_tagihan_edit">Simpan</button>
      </div>
    </div>
  </div>
</div>