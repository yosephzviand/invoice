<?php foreach ($data as $key => $value) :
	if ($value->bulan == 1) {
		$bulan = 'Januari';
	} elseif ($value->bulan == 2) {
		$bulan = 'Februari';
	} elseif ($value->bulan == 3) {
		$bulan = 'Maret';
	} elseif ($value->bulan == 4) {
		$bulan = 'April';
	} elseif ($value->bulan == 5) {
		$bulan = 'Mei';
	} elseif ($value->bulan == 6) {
		$bulan = 'Juni';
	} elseif ($value->bulan == 7) {
		$bulan = 'Juli';
	} elseif ($value->bulan == 8) {
		$bulan = 'Agustus';
	} elseif ($value->bulan == 9) {
		$bulan = 'September';
	} elseif ($value->bulan == 10) {
		$bulan = 'Oktober';
	} elseif ($value->bulan == 11) {
		$bulan = 'November';
	} else {
		$bulan = 'Desember';
	}

	$totala = $value->gajipokok + $value->tunjangan;
	$thp = $totala - $value->potongan;
?>
	<table border="">
		<tr>
			<td rowspan="" style="text-align: center;" width="200px"><img src="<?php echo base_url() ?>assets/dist/img/amc.png" width="125px" alt="AdminLTE Logo"></td>
			<td>
				<p style="font-size: 16px"><b>ATOOM MEDIA CONNECT</b></p>
				<p style="font-size: 12px">Jalan Yogya-Wonosari Km. 17,5 Patuk Gunungkidul Yogyakarta 55862</p>
				<p style="font-size: 12px">Telp : 0852-2522-5959 (CS) Email : atomedia_mail@yahoo.com</p>
			</td>
		</tr>
	</table>
	<hr>

	<table width="100%" cellspacing="0" cellpadding="3" border="" style="font-family: Courier New; font-size: 10pt">
		<tr>
			<td colspan="6" style="text-align: center;">
				<p style="font-size: 12pt"><b><u>SLIP GAJI KARYAWAN</u></b></p>
			</td>
		</tr>
		<tr>
			<td height="20px"></td>
		</tr>
		<tr>
			<td width="100px">Nama</td>
			<td width="30px">:</td>
			<td> <?= $value->namakaryawan ?> </td>
		</tr>
		<tr>
			<td>Jabatan</td>
			<td>:</td>
			<td> <?= $value->namajabatan ?> </td>
		</tr>
		<tr>
			<td>Bulan</td>
			<td>:</td>
			<td> <?= $bulan ?></td>
		</tr>
	</table>
	<table width="100%" cellspacing="0" style="font-family: Courier New; font-size: 10pt" border="">

		<tr>
			<td height="30px" colspan="6"></td>
		</tr>
		<tr>
			<td colspan="3" style="text-align: center;"><b>Penghasilan</b></td>
			<td></td>
			<td colspan="3" style="text-align: center;"><b>Potongan</b></td>
		</tr>
		<tr>
			<td width="150px">Gaji Pokok</td>
			<td width="30px"> = </td>
			<td style="text-align: right;"> <?= number_format($value->gajipokok, 0, ',', '.') ?> </td>
			<td width="50px"></td>
			<td width="150px">Potongan</td>
			<td width="30px">=</td>
			<td style="text-align: right;"><?= number_format($value->potongan, 0, ',', '.') ?></td>
		</tr>
		<tr>
			<td>Tambahan</td>
			<td>=</td>
			<td style="text-align: right;"><?= number_format($value->tunjangan, 0, ',', '.') ?></td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: center;"><b>TOTAL (A)</b></td>
			<td style="border-top: solid; text-align: right;"><?= number_format($totala, 0, ',', '.') ?></td>
			<td></td>
			<td colspan="2" style="text-align: center;"><b>TOTAL (B)</b></td>
			<td style="border-top: solid; text-align: right;"><?= number_format($value->potongan, 0, ',', '.') ?></td>
		</tr>
		<tr>
			<td height="30px"></td>
		</tr>
		<tr>
			<td colspan="7" style="border-top: solid;text-align: center; background-color: #b3b3b3 "><b>TAKE HOME PAY (A - B) = Rp. <?= number_format($thp, 0, ',', '.') ?></b></td>

		</tr>
		<tr>
			<td colspan="7" style="border-top: solid;text-align: center; border-bottom: solid; background-color: #b3b3b3">Terbilang : <?= $value->terbilang ?> Rupiah.</td>
		</tr>
		<tr>
			<td height="30px"></td>
		</tr>
		<tr>
			<td colspan="3"></td>
			<td></td>
			<td colspan="3" style="text-align: center;"> Wonosari, <?= date('d-m-Y') ?></td>
		</tr>
		<tr>
			<td colspan="3" style="text-align: center;">Diterima</td>
			<td></td>
			<td colspan="3" style="text-align: center;">Mengetahui</td>
		</tr>
		<tr>
			<td height="40px"></td>
		</tr>
		<tr>
			<td colspan="3" style="text-align: center;">...................</td>
			<td></td>
			<td colspan="3" style="text-align: center;">Adhi Wibowo</td>
		</tr>

	</table>
	<br>
	-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	<br>
	<br>
<?php endforeach; ?>
