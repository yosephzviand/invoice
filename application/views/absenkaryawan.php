<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<button type="button" id="pdfdesa" class="btn btn-primary" data-toggle="modal" data-target="#modal-absenkaryawan" title="Tambah Data ABsen"> Tambah
						</button>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table width="100%" id="tbabsen" class="table table-striped table-bordered table-sm">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama</th>
										<th>Bulan</th>
										<th>Tanggal</th>
										<th>Jam Masuk</th>
										<th>Jam Pulang</th>
										<th>Keterangan</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="modal-absenkaryawan">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Tambah Absen</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form class="form-horizontal">
					<div class="card-body">
						<div class="form-group row">
							<label for="inputEmail3" class="col-3 col-form-label">Nama Karyawan</label>
							<div class="col-9">
								<select class="form-control select2" name="namakaryawan" id="idkaryawan" style="width: 100%;">
									<option value="">Pilih Karyawan</option>
									<?php foreach ($karyawan as $key) : ?>
										<option value="<?php echo $key->idkaryawan; ?>"><?php echo $key->namakaryawan; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="inputEmail3" class="col-3 col-form-label">Tanggal</label>
							<div class="col-9">
								<input type="input" class="form-control" value="<?= date('Y-m-d') ?>" name="tanggal" id="datepicker3">
							</div>
						</div>
						<div class="form-group row">
							<label for="inputEmail3" class="col-3 col-form-label">Absen</label>
							<div class="col-9">
								<select name="absen" id="absen" class="form-control">
									<option value="masuk">Masuk</option>
									<option value="pulang">Pulang</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="inputEmail3" class="col-3 col-form-label">Jam</label>
							<div class="col-9">
								<div class="input-group" id="timepicker" data-target-input="nearest">
									<input type="text" class="form-control datetimepicker-input" id="jam" data-target="#timepicker" />
									<div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
										<div class="input-group-text"><i class="far fa-clock"></i></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer justify-content-between">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary simpan_absenkaryawan">Simpan</button>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="modal-edit-absenkaryawan">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Absen</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form class="form-horizontal">
					<div class="card-body">
						<div class="form-group row">
							<label for="inputEmail3" class="col-3 col-form-label">ID</label>
							<div class="col-9">
								<input type="text" name="ideditkaryawan" class="form-control" id="ideditkaryawan" placeholder="ID" readonly="true">
							</div>
						</div>
						<div class="form-group row">
							<label for="inputEmail3" class="col-3 col-form-label">Nama Karyawan</label>
							<div class="col-9">
								<input type="text" name="editnama" class="form-control" id="editnama" placeholder="Nama Karyawan" readonly>
							</div>
						</div>
						<div class="form-group row">
							<label for="inputEmail3" class="col-3 col-form-label">Tanggal</label>
							<div class="col-9">
								<input type="text" name="edittanggal" class="form-control" id="datepicker" placeholder="Tanggal">
							</div>
						</div>
						<div class="form-group row">
							<label for="inputEmail3" class="col-3 col-form-label">Jam Masuk</label>
							<div class="col-9">
								<div class="input-group" id="timepicker1" data-target-input="nearest">
									<input type="text" class="form-control datetimepicker-input" name="timepicker1" id="jammasuk" data-target="#timepicker1" />
									<div class="input-group-append" data-target="#timepicker1" data-toggle="datetimepicker">
										<div class="input-group-text"><i class="far fa-clock"></i></div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label for="inputEmail3" class="col-3 col-form-label">Jam Pulang</label>
							<div class="col-9">
								<div class="input-group" id="timepicker2" data-target-input="nearest">
									<input type="text" class="form-control datetimepicker2-input" name="timepicker2" id="jamkeluar" data-target="#timepicker2" />
									<div class="input-group-append" data-target="#timepicker2" data-toggle="datetimepicker">
										<div class="input-group-text"><i class="far fa-clock"></i></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer justify-content-between">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary simpan_absenkaryawan_edit">Simpan</button>
			</div>
		</div>
	</div>
</div>
