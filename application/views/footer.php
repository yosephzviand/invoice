</div>

<footer class="main-footer">
	<strong>Copyright &copy; 2021 <a href="http://mervia.my.id" target="_blank">MERVIA</a>.</strong>
	All rights reserved.
</footer>

</div>

<!-- jQuery -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script src="<?php echo base_url('assets/js'); ?>/toastr.min.js"></script>
<script>
	$.widget.bridge('uibutton', $.ui.button)
	var site_url = '<?php echo site_url(); ?>';
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/chart.js/Chart.min.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/moment/moment.min.js"></script>
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="<?php echo base_url() ?>assets/AdminLTE3/dist/js/pages/dashboard.js"></script> -->
<!-- DataTables -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/dist/js/demo.js"></script>
<script src="<?php echo base_url() ?>assets/AdminLTE3/dist/js/masadi.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/moment/moment.min.js"></script>
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<!-- Custom js for this page-->
<!-- <script src="<?php echo base_url() ?>assets/assets/js/shared/chart.js"></script> -->
<!-- <script src="<?php echo base_url() ?>assets/assets/vendors/js/vendor.bundle.base.js"></script> -->
<!-- <script src="<?php echo base_url() ?>assets/assets/vendors/js/vendor.bundle.addons.js"></script> -->
<!-- endinject -->

<script src="<?php echo base_url() ?>assets/assets/js/shared/misc.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/select2/js/select2.full.min.js"></script>
<!-- Highchart -->
<script src="<?= base_url() ?>assets/Highcharts/code/highcharts.js"></script>
<script src="<?= base_url() ?>assets/Highcharts/code/modules/variable-pie.js"></script>
<script src="<?= base_url() ?>assets/Highcharts/code/modules/accessibility.js"></script>
<!-- bootstrap datepicker -->
<script src="<?= base_url()?>assets/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url()?>assets/sweetalert/dist/sweetalert.min.js"></script>
<script>
	$(function () {
    //Initialize Select2 Elements
    $('.select2').select2({
    	theme: 'bootstrap4'
    })

    $('[data-mask]').inputmask()

    $('#paketpelanggan').change(function () {
    	var id = $(this).val();
    	console.log(id);
    	$.ajax({
    		url: site_url + "amc/vwtagihan",
    		method: "POST",
    		data: { id: id },
    		async: true,
    		dataType: 'json',
    		success: function (data) {
    			console.log(data);
          var konten = $('.postx');
          konten.empty();

          for (i in data) {
            if (data[i].hargapaket == 0) {
              konten.append('<div class="form-group row">' +
                '<label for="inputPassword3" class="col-sm-3 col-form-label">Harga Paket</label>'+
                '<div class="col-sm-9">'+
                '<input type="text" name="hargapaket" class="form-control" id="hargapaket" placeholder="Tagihan">'+
                '</div>'+
                '</div>');
            } else {
              konten.append('<div class="form-group row">' +
                '<label for="inputPassword3" class="col-sm-3 col-form-label">Harga Paket</label>'+
                '<div class="col-sm-9">'+
                '<input type="text" name="hargapaket" class="form-control" id="hargapaket" value="'+data[i].hargapaket+'" placeholder="Tagihan" readonly="">'+
                '</div>'+
                '</div>');
            }
          }
        }
      });
    	return false;
    });

    $('#editpaketpelanggan').change(function () {
      var id = $(this).val();
      console.log(id);
      $.ajax({
        url: site_url + "amc/vwtagihan",
        method: "POST",
        data: { id: id },
        async: true,
        dataType: 'json',
        success: function (data) {
          console.log(data);
          var konten = $('.postxedit');
          konten.empty();

          for (i in data) {
            if (data[i].hargapaket == 0) {
              konten.append('<div class="form-group row">' +
                '<label for="inputPassword3" class="col-sm-3 col-form-label">Jumlah Tagihan</label>'+
                '<div class="col-sm-9">'+
                '<input type="text" name="edithargapaket" class="form-control" id="edithargapaket" placeholder="Tagihan">'+
                '</div>'+
                '</div>');
            } else {
              konten.append('<div class="form-group row">' +
                '<label for="inputPassword3" class="col-sm-3 col-form-label">Jumlah Tagihan</label>'+
                '<div class="col-sm-9">'+
                '<input type="text" name="edithargapaket" class="form-control" id="edithargapaket" value="'+data[i].hargapaket+'" placeholder="Tagihan" readonly="">'+
                '</div>'+
                '</div>');
            }
          }
        }
      });
      return false;
    });

    $('.jenis').change(function () {
      // alert();
      var id = $(this).val();
      console.log(id);
      var konten = $('.postgaji');
      konten.empty();
      // $.ajax({
        if (id == 1) {
          konten.append('<div class="form-group row">'+
            '<label for="inputPassword3" class="col-sm-3 col-form-label">Gaji Pokok</label>'+
            '<div class="col-sm-9">'+
            '<input type="text" class="form-control" id="gajipokok" name="gajipokok"  data-mask>'+
            '</div>'+
            '</div>');
        } else {
          konten.append('<div class="form-group row">'+
          // '<label for="inputPassword3" class="col-sm-3 col-form-label">Gaji Pokok</label>'+
          '<div class="col-sm-9">'+
          '<input type="hidden" class="form-control" id="gajipokok" name="gajipokok"  data-mask>'+
          '</div>'+
          '</div>');
        }
      // })
    });

    $('.editjenis').change(function () {
      // alert();
      var id = $(this).val();
      console.log(id);
      var konten = $('.postgajiedit');
      konten.empty();

      if (id == 1) {
        konten.append('<div class="form-group row">'+
          '<label for="inputPassword3" class="col-sm-3 col-form-label">Gaji Pokok</label>'+
          '<div class="col-sm-9">'+
          '<input type="text" class="form-control" id="editgajipokok" name="editgajipokok"  data-mask>'+
          '</div>'+
          '</div>');
      } else {
        konten.append('<div class="form-group row">'+
          // '<label for="inputPassword3" class="col-sm-3 col-form-label">Gaji Pokok</label>'+
          '<div class="col-sm-9">'+
          '<input type="hidden" class="form-control" id="editgajipokok" name="editgajipokok"  data-mask>'+
          '</div>'+
          '</div>');
      }
    });


    $('#pelanggan').change(function () {
      var idtagihan = $(this).val();
      // console.log(id);
      $.ajax({
        url: site_url + "amc/vwpelanggan",
        method: "POST",
        data: { idtagihan: idtagihan },
        async: true,
        dataType: 'json',
        success: function (data) {
          console.log(data);
          for (i in data) {
            $('[name="idpakettagihan"]').val(data[i].idpaketharga);
            $('[name="pakettagihan"]').val(data[i].namapaket);
            $('[name="tagihan"]').val(data[i].hargapaket);
            $('#modal-tagihan').modal('show');
          }
        }
      });
      return false;
    });

    $('#jnskar').change(function () {
      var id = $(this).val();
      // console.log(id);
      $.ajax({
        url: site_url + "amc/getkaryawan",
        method: "POST",
        data: { id: id },
        async: true,
        dataType: 'json',
        success: function (data) {
          console.log(data);
          var html = '';
          var i;
          html = '<option value="">Pilih Karyawan</option>';
          for (i = 0; i < data.length; i++) {
            html += '<option value=' + data[i].idkaryawan + '>' + data[i].namakaryawan + '</option>';
          }
          $('#namakaryawan').html(html);
        }
      });
      return false;
    });

    $('#namakaryawan').change(function () {
      var idkaryawan = $(this).val();
      // console.log(id);
      $.ajax({
        url: site_url + "amc/vwkaryawan",
        method: "POST",
        data: { idkaryawan: idkaryawan },
        async: true,
        dataType: 'json',
        success: function (data) {
          console.log(data);
          // for (i in data) {
          //   $('[name="gajipokok"]').val(data[i].gajipokok);
          //   $('#modal-gajian').modal('show');
          // }
          var konten = $('.postxgaji');
          konten.empty();
          for (i in data) {
            if (data[i].jeniskaryawan == 1) {
              konten.append('<div class="form-group row">' +
                '<label for="inputPassword3" class="col-sm-3 col-form-label">Gaji Pokok</label>'+
                '<div class="col-sm-9">'+
                '<input type="text" name="gajipokok" class="form-control" id="gajipokok" value="'+data[i].gajipokok+'" placeholder="Tagihan" readonly="" onkeyup="sumgaji();">'+
                '</div>'+
                '</div>');
            } else {
              konten.append('<div class="form-group row">' +
                '<label for="inputPassword3" class="col-sm-3 col-form-label">Fee</label>'+
                '<div class="col-sm-9">'+
                '<input type="text" name="gajipokok" class="form-control" id="gajipokok" value="'+data[i].gajipokok+'" placeholder="Fee"  onkeyup="sumgaji();">'+
                '</div>'+
                '</div>');
            }
          }
        }
      });
      return false;
    });


  });


function rubah(angka) {
  var reverse = angka.toString().split('').reverse().join(''),
  ribuan = reverse.match(/\d{1,3}/g);
  ribuan = ribuan.join('.').split('').reverse().join('');
  return ribuan;
}

function sum() {
  var harga = document.getElementById('tagihan').value;
  var tambahan = document.getElementById('tambahan').value;
  var result =  parseInt(harga) + parseInt(tambahan);
  if (!isNaN(result)) {
   document.getElementById('totaltagihan').value = result;
 } 
}

function sumedit() {
  var harga = document.getElementById('edittagihan').value;
  var tambahan = document.getElementById('edittambahan').value;
  var result =  parseInt(harga) + parseInt(tambahan);
  if (!isNaN(result)) {
   document.getElementById('edittotaltagihan').value = result;
 }
}

function sumgaji() {
  var gajipokok = document.getElementById('gajipokok').value;
  var tunjangan = document.getElementById('tunjangan').value;
  var potongan = document.getElementById('potongan').value;
  var result =  parseInt(gajipokok) + parseInt(tunjangan) - parseInt(potongan);
  if (!isNaN(result)) {
   document.getElementById('gajibersih').value = result;
 } 
}

function sumgajiedit() {
  var gajipokok = document.getElementById('editgajipokok').value;
  var tunjangan = document.getElementById('edittunjangan').value;
  var potongan = document.getElementById('editpotongan').value;
  var result =  parseInt(gajipokok) + parseInt(tunjangan) - parseInt(potongan);
  if (!isNaN(result)) {
   document.getElementById('editgajibersih').value = result;
 } 
}

function sumbarang() {
  var jumlah = document.getElementById('editjumlahbarang').value;
  var harga = document.getElementById('edithargabeli').value;
  var result =  parseInt(jumlah) * parseInt(harga);
  if (!isNaN(result)) {
   document.getElementById('total').value = result;
 } 
}

</script>
</body>
</html>
