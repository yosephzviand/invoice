<?php $no = 1;
foreach ($data as $key) : ?>
	<table border="" width="100%" cellspacing="0" cellpadding="3" style="font-family: helvetica; ">
		<tr>
			<td rowspan="5" style="text-align: center;" width="200px"><img src="<?php echo base_url() ?>assets/dist/img/amc.png" width="180px" alt="AdminLTE Logo"></td>
			<td></td>
			<td colspan="3" style="text-align: center;"><b>ID Billing : <?= $key->idbilling ?></b></td>

		</tr>
		<tr>
			<td></td>
			<td colspan="3" style="font-size: 9pt;">Kepada Yth.</td>
		</tr>
		<tr>
			<td></td>
			<td width="10px" style="font-size: 9pt;">Nama</td>
			<td width="10px" style="font-size: 9pt;">:</td>
			<td style="font-size: 9pt;"><?= $key->namapelanggan ?></td>
		</tr>
		<tr>
			<td rowspan="2" style="text-align: center;" width="250px">
				<p style="text-align: center; font-size: 20px; text-align: center;"><b>NOTA TAGIHAN</b></p>
				<p><u>Nomor : <?= $key->nosurat ?></u></p>
			</td>
			<td style="font-size: 9pt;">Alamat</td>
			<td style="font-size: 9pt;">:</td>
			<td style="font-size: 9pt;"><?= $key->alamatpelanggan ?></td>
		</tr>
		<tr>
			<td style="font-size: 9pt;">Telp</td>
			<td style="font-size: 9pt;">:</td>
			<td style="font-size: 9pt;"><?= $key->notelp ?></td>
		</tr>
	</table>
	<br>
	<table width="100%" cellspacing="0" cellpadding="3" style="font-size: 10pt;font-family: helvetica; font-size: 9pt;" border="1">
		<thead>
			<tr>
				<th style="background-color: #6666ff; color: white">No</th>
				<th style="background-color: #6666ff; color: white">Keterangan</th>
				<th style="background-color: #6666ff; color: white">Harga</th>
				<th style="background-color: #6666ff; color: white">Diskon</th>
				<th style="background-color: #6666ff; color: white">Sub Total</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style="text-align: center;"><?= $no++ ?></td>
				<td>Pembayaran Internet Bulan <?= $key->nmbulan . ' ' . $key->tahun ?></td>
				<td style="text-align: right;">Rp. <?= number_format($key->harga, 0, ',', '.') ?></td>
				<td style="text-align: center;">-</td>
				<td style="text-align: right;">Rp. <?= number_format($key->subharga, 0, ',', '.') ?></td>
			</tr>
			<tr>
				<td colspan="4" style="text-align: center;"><b>TOTAL</b></td>
				<td style="text-align: right;"><b>Rp. <?= number_format($key->subharga, 0, ',', '.') ?></b></td>
			</tr>
		</tbody>
	</table>
	<br>
	<?php
	if ($key->bulan == 1) {
		$bulan = 'Januari';
	} elseif ($key->bulan == 2) {
		$bulan = 'Februari';
	} elseif ($key->bulan == 3) {
		$bulan = 'Maret';
	} elseif ($key->bulan == 4) {
		$bulan = 'April';
	} elseif ($key->bulan == 5) {
		$bulan = 'Mei';
	} elseif ($key->bulan == 6) {
		$bulan = 'Juni';
	} elseif ($key->bulan == 7) {
		$bulan = 'Juli';
	} elseif ($key->bulan == 8) {
		$bulan = 'Agustus';
	} elseif ($key->bulan == 9) {
		$bulan = 'September';
	} elseif ($key->bulan == 10) {
		$bulan = 'Oktober';
	} elseif ($key->bulan == 11) {
		$bulan = 'November';
	} else {
		$bulan = 'Desember';
	}
	?>
	<table width="100%" cellspacing="0" cellpadding="3" style="font-family: helvetica; font-size: 9pt;" border="1">
		<tr>
			<td width="755">No Rekening : <b>BRI</b> 6983.01.000413.50.0 a.n Adhi Wibowo <b>( Tanggal Jatuh Tempo terakhir 20-25 <?= $bulan ?> <?php echo date('Y') ?> )</b></td>
		</tr>
	</table>

	<table width="100%" cellspacing="" cellpadding="" border="">
		<tr>
			<td height="10px"></td>
			<td></td>
		</tr>
		<tr>
			<td rowspan="4" style="font-size: 9pt; border: dotted;" width="450px">
				<p> ATOOM MEDIA CONNECT</p>
				<p> Jalan Yogya-Wonosari Km. 17,5 Patuk Gunungkidul Yogyakarta 55862</p>
				<p> Telp : 0852-2522-5959 (CS)</p>
				<p> Email : atomedia_mail@yahoo.com</p>
			</td>
			<td></td>
			<td>Hormat Kami</td>
		</tr>
		<tr>
			<td></td>
			<td colspan=""></td>
		</tr>
		<tr>
			<!-- <td style="font-size: 9pt">Email : atomedia_mail@yahoo.com</td> -->
			<td></td>
			<td height="60px" rowspan="2">_______________</td>
		</tr>
	</table>
	<br>
	<!-- <br> -->
	-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	<br>
	<br>
<?php endforeach; ?>
