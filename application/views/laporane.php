<section class="content">
	<div class="container-fluid">

		<div class="card">
			<div class="card-header">
				<h3 class="card-title">Pembukuan dan Pelaporan</h3>
			</div>
			<!-- /.card-header -->
			<div class="card-body">
				<div id="accordion">
					<!-- we are adding the .class so bootstrap.js collapse plugin detects it -->
					<div class="card card-primary">
						<div class="card-header">
							<h4 class="card-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
									Cetak Pelanggan
								</a>
							</h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse in">
							<div class="card-body">
								<form name="ipladder2" id="ipladder2" method="post">
									<div class="form-group row">
										<div class="col-sm-4 col-6">
											<select class="form-control select2" name="arealap" id="arealap">
												<option value="">Pilih Semua</option>
												<?php
												foreach ($data as $key) {
												?>
													<option value="<?php echo $key->idarea; ?>"><?php echo $key->namaarea; ?></option>
												<?php
												}
												?>
											</select>
										</div>

										<div class="col-sm-4 col-6">
											<button type="button" id="cetakpelanggan" class="btn btn-primary" data-card-widget="Filter" data-toggle="tooltip">
												<i class="fas fa-file-pdf"></i> PDF
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="card card-danger">
						<div class="card-header">
							<h4 class="card-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
									Cetak Tagihan
								</a>
							</h4>
						</div>
						<div id="collapseTwo" class="panel-collapse collapse">
							<div class="card-body">
								<form name="ipladder" id="ipladder" method="post">
									<div class="form-group row">
										<div class="col-sm-4 col-6">
											<div class="input-group date">
												<input name="datepicker" type="text" class="form-control pull-right" id="datepicker" placeholder="Masukkan Tanggal" required="true">
											</div>
										</div>
										<label class="col-form-label"> s/d</label>
										<div class="col-sm-4 col-6">
											<div class="input-group date">
												<input name="datepicker1" type="text" class="form-control pull-right" id="datepicker1" placeholder="Masukkan Tanggal" required="true">
											</div>
										</div>

										<button type="button" id="lappdftagihan" class="btn btn-primary" data-card-widget="Filter" data-toggle="tooltip">
											<i class="fas fa-file-pdf"></i> PDF
										</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="card card-info">
						<div class="card-header">
							<h4 class="card-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapsePembayaran">
									Cetak Pembayaran
								</a>
							</h4>
						</div>
						<div id="collapsePembayaran" class="panel-collapse collapse">
							<div class="card-body">
								<form name="bayar" id="bayar" method="post">
									<div class="form-group row">
										<div class="col-sm-4 col-6">
											<div class="input-group date">
												<input name="datepicker2" type="text" class="form-control pull-right" id="datepicker2" placeholder="Masukkan Tanggal" required="true">
											</div>
										</div>
										<label class="col-form-label"> s/d</label>
										<div class="col-sm-4 col-6">
											<div class="input-group date">
												<input name="datepicker3" type="text" class="form-control pull-right" id="datepicker3" placeholder="Masukkan Tanggal" required="true">
											</div>
										</div>

										<button type="button" id="lappdfpembayaran" class="btn btn-primary" data-card-widget="Filter" data-toggle="tooltip">
											<i class="fas fa-file-pdf"></i> PDF
										</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="card card-success">
						<div class="card-header">
							<h4 class="card-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
									Cetak Piutang
								</a>
							</h4>
						</div>
						<div id="collapseThree" class="panel-collapse collapse">
							<div class="card-body">
								<form name="ipladder1" id="ipladder1" method="post">
									<div class="form-group row">
										<div class="col-sm-2 col-6">
											<select class="form-control select2" name="laptahun" id="laptahun">
												<option>Pilih Tahun</option>
												<?php
												for ($i = date('Y'); $i >= 2021; $i--) {
												?>
													<option value="<?php echo $i; ?>">Tahun <?php echo $i; ?></option>
												<?php
												}
												?>
											</select>
										</div>
										<div class="col-sm-4 col-6">
											<select class="form-control select2 " name="lapbulan" id="lapbulan">
												<option value="">Pilih Semua</option>
												<option value="01">Januari</option>
												<option value="02">Februari</option>
												<option value="03">Maret</option>
												<option value="04">April</option>
												<option value="05">Mei</option>
												<option value="06">Juni</option>
												<option value="07">Juli</option>
												<option value="08">Agustus</option>
												<option value="09">September</option>
												<option value="10">Oktober</option>
												<option value="11">November</option>
												<option value="12">Desember</option>
											</select>
										</div>
										<div class="col-sm-4 col-6">
											<button type="button" id="lappdfbelumbayar" class="btn btn-primary" data-card-widget="Filter" data-toggle="tooltip">
												<i class="fas fa-file-pdf"></i> PDF
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>

					<div class="card card-warning">
						<div class="card-header">
							<h4 class="card-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
									Cetak Gajian
								</a>
							</h4>
						</div>
						<div id="collapseFour" class="panel-collapse collapse">
							<div class="card-body">
								<form name="formgajian" id="formgajian" method="post">
									<div class="form-group row">
										<div class="col-sm-2 col-6">
											<select class="form-control select2" name="tahungajian" id="tahungajian">
												<option value="">Pilih Tahun</option>
												<?php
												for ($i = date('Y'); $i >= 2021; $i--) {
												?>
													<option value="<?php echo $i; ?>">Tahun <?php echo $i; ?></option>
												<?php
												}
												?>
											</select>
										</div>
										<div class="col-sm-4 col-6">
											<select class="form-control select2 " name="bulangajian" id="bulangajian">
												<option value="">Pilih Semua</option>
												<option value="01">Januari</option>
												<option value="02">Februari</option>
												<option value="03">Maret</option>
												<option value="04">April</option>
												<option value="05">Mei</option>
												<option value="06">Juni</option>
												<option value="07">Juli</option>
												<option value="08">Agustus</option>
												<option value="09">September</option>
												<option value="10">Oktober</option>
												<option value="11">November</option>
												<option value="12">Desember</option>
											</select>
										</div>

										<div class="col-sm-4 col-6">
											<button type="button" id="lapgajian" class="btn btn-primary" data-card-widget="Filter" data-toggle="tooltip">
												<i class="fas fa-file-pdf"></i> PDF
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>

					<div class="card card-primary">
						<div class="card-header">
							<h4 class="card-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
									Cetak Tidak Terbuat Tagihan Pelanggan
								</a>
							</h4>
						</div>
						<div id="collapseFive" class="panel-collapse collapse">
							<div class="card-body">
								<form name="formtag" id="formtag" method="post">
									<div class="form-group row">
										<!-- <div class="col-sm-2 col-6">
											<select class="form-control select2" name="tahungajian" id="tahungajian">
												<option value="">Pilih Tahun</option>
												<?php
												for ($i = date('Y'); $i >= 2021; $i--) {
												?>
													<option value="<?php echo $i; ?>">Tahun <?php echo $i; ?></option>
												<?php
												}
												?>
											</select>
										</div> -->
										<div class="col-sm-4 col-6">
											<select class="form-control select2 " name="bulantag" id="bulantag">
												<option value="">Pilih Bulan</option>
												<option value="01">Januari</option>
												<option value="02">Februari</option>
												<option value="03">Maret</option>
												<option value="04">April</option>
												<option value="05">Mei</option>
												<option value="06">Juni</option>
												<option value="07">Juli</option>
												<option value="08">Agustus</option>
												<option value="09">September</option>
												<option value="10">Oktober</option>
												<option value="11">November</option>
												<option value="12">Desember</option>
											</select>
										</div>

										<div class="col-sm-4 col-6">
											<button type="button" id="laptag" class="btn btn-primary" data-card-widget="Filter" data-toggle="tooltip">
												<i class="fas fa-file-pdf"></i> PDF
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>

					<div class="card card-danger">
						<div class="card-header">
							<h4 class="card-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapsePelanggan">
									Cetak Pembayaran By Pelanggan
								</a>
							</h4>
						</div>
						<div id="collapsePelanggan" class="panel-collapse collapse">
							<div class="card-body">
								<form name="formpempel" id="formpempel" method="post">
									<div class="form-group row">
										<div class="col-3">
											<select name="pelanggan" class="form-control select2" id="pelanggan">
												<option value="" disabled selected hidden>Pilih Pelanggan</option>
												<?php foreach ($pelanggan as $key => $pel) : ?>
													<option value="<?= $pel->idpelanggan ?>"> <?= $pel->namapelanggan?></option>
												<?php endforeach; ?>
											</select>
										</div>
										<!-- <div class="col-2">
											<select name="status" class="form-control select2" id="status">
												<option value="" disabled selected hidden>Pilih Status Bayar</option>
												<option value=""> Belum Bayar </option>
												<option value="1"> Bayar </option>
											</select>
										</div> -->

										<div class="col-2">
											<select class="form-control select2" name="tahunpel" id="tahunpel">
												<option value="">Pilih Tahun</option>
												<?php
												for ($i = date('Y'); $i >= 2021; $i--) {
												?>
													<option value="<?php echo $i; ?>">Tahun <?php echo $i; ?></option>
												<?php
												}
												?>
											</select>
										</div>
										<div class="col-2">
											<select class="form-control select2 " name="bulanpelstart" id="bulanpelstart">
												<option value="">Pilih Bulan</option>
												<option value="01">Januari</option>
												<option value="02">Februari</option>
												<option value="03">Maret</option>
												<option value="04">April</option>
												<option value="05">Mei</option>
												<option value="06">Juni</option>
												<option value="07">Juli</option>
												<option value="08">Agustus</option>
												<option value="09">September</option>
												<option value="10">Oktober</option>
												<option value="11">November</option>
												<option value="12">Desember</option>
											</select>
										</div>
										<label class="col-form-label"> s/d</label>
										<div class="col-2 ">
											<select class="form-control select2 " name="bulanpelend" id="bulanpelend">
												<option value="">Pilih Bulan</option>
												<option value="01">Januari</option>
												<option value="02">Februari</option>
												<option value="03">Maret</option>
												<option value="04">April</option>
												<option value="05">Mei</option>
												<option value="06">Juni</option>
												<option value="07">Juli</option>
												<option value="08">Agustus</option>
												<option value="09">September</option>
												<option value="10">Oktober</option>
												<option value="11">November</option>
												<option value="12">Desember</option>
											</select>
										</div>
									</div>
									<div class="row">
										<div class="col-4">
											<button type="button" id="lappempel" class="btn btn-primary" data-card-widget="Filter" data-toggle="tooltip">
												<i class="fas fa-file-pdf"></i> PDF
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>

					<div class="card card-info">
						<div class="card-header">
							<h4 class="card-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
									Cetak Rekap Absensi
								</a>
							</h4>
						</div>
						<div id="collapseSix" class="panel-collapse collapse">
							<div class="card-body">
								<form name="formabsen" id="formabsen" method="post">

									<div class="form-group row">
										<div class="col-sm-2 col-6">
											<select class="form-control select2" name="tahunabsen" id="tahunabsen">
												<option value="">Pilih Tahun</option>
												<?php
												for ($i = date('Y'); $i >= 2021; $i--) {
												?>
													<option value="<?php echo $i; ?>">Tahun <?php echo $i; ?></option>
												<?php
												}
												?>
											</select>
										</div>
										<div class="col-sm-4 col-6">
											<select class="form-control select2 " name="bulanabsen" id="bulanabsen">
												<option value="">Pilih Bulan</option>
												<option value="01">Januari</option>
												<option value="02">Februari</option>
												<option value="03">Maret</option>
												<option value="04">April</option>
												<option value="05">Mei</option>
												<option value="06">Juni</option>
												<option value="07">Juli</option>
												<option value="08">Agustus</option>
												<option value="09">September</option>
												<option value="10">Oktober</option>
												<option value="11">November</option>
												<option value="12">Desember</option>
											</select>
										</div>
										<div class="col-sm-4 col-6">
											<button type="button" id="lapabsen" class="btn btn-primary" data-card-widget="Filter" data-toggle="tooltip">
												<i class="fas fa-file-pdf"></i> PDF
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
		<!-- /.card-body -->
	</div>
</section>
