<table border="">
	<tr>
		<td rowspan="" style="text-align: center;" width="200px"><img src="<?php echo base_url() ?>assets/dist/img/amc.png" width="125px" alt="AdminLTE Logo"></td>
		<td>
			<p style="font-size: 18px"><b>ATOOM MEDIA CONNECT</b></p>
			<p style="font-size: 12px">Jalan Yogya-Wonosari Km. 17,5 Patuk Gunungkidul Yogyakarta 55862</p>
			<p style="font-size: 12px">Telp : 0852-2522-5959 (CS) Email : atomedia_mail@yahoo.com</p>
		</td>
	</tr>
</table>
<hr>

<h3 style="text-align: center;">Data Rekap Absen Karyawan</h3>

<h4>Bulan : <?= $bulan ?></h4>
<table width="100%" cellspacing="0" cellpadding="3" style="font-size: 10pt;" border="1">
	<thead>
		<tr>
			<th>No</th>
			<th>Nama Pelanggan</th>
			<th>Jumlah Absen</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
		foreach ($data as $key) : ?>
			<tr>
				<td style="text-align: center;"><?= $no++ ?></td>
				<td><?= $key->namakaryawan ?></td>
				<td style="text-align: right;"><?= $key->jumlah ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
