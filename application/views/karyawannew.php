<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <button type="button" id="pdfdesa" class="btn btn-primary" data-toggle="modal" data-target="#modal-karyawan" title="Tambah Data Area" > Tambah
            </button>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table width="100%" id="tbkaryawan" class="table table-striped table-bordered table-sm">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Karyawan</th>
                    <th>Alamat</th>
                    <th>No. Telp</th>
                    <th>Jabatan</th>
                    <th>Pendidikan</th>
                    <th>Masa Kerja</th>
                    <th>Gaji Pokok</th>
                    <th>Jenis Karyawan</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<div class="modal fade" id="modal-karyawan">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Karyawan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-6">
                <div class="form-group row">
                  <label for="inputPassword3" class="col-sm-3 col-form-label">Jenis</label>
                  <div class="col-sm-9">
                    <select class="form-control select2 jenis" name="jnskaryawan" id="jnskaryawan" style="width: 100%;">
                     <option value="">Pilih Jenis</option>
                     <?php foreach ($jenis as $key) : ?>
                      <option value="<?php echo $key->idjenis; ?>"><?php echo $key->namajenis; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-3 col-form-label">Nama </label>
                <div class="col-sm-9">
                  <input type="text" name="nmkaryawan" class="form-control" id="nmkaryawan" placeholder="Nama Karyawan">
                </div>
              </div>
              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-3 col-form-label">Alamat</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="alamatkaryawan" name="alamatkaryawan" placeholder="Alamat">
                </div>
              </div>
              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-3 col-form-label">Jabatan</label>
                <div class="col-sm-9">
                  <select class="form-control select2" name="jabatan" id="jabatan" style="width: 100%;">
                    <option >Pilih Jabatan</option>
                    <?php foreach ($jabatan as $key) : ?>
                      <option value="<?php echo $key->idjabatan; ?>"><?php echo $key->namajabatan; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>

            </div>
            <div class="col-lg-6">
              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-3 col-form-label">Pendidikan</label>
                <div class="col-sm-9">
                  <select class="form-control select2" name="pendidikan" id="pendidikan" style="width: 100%;">
                    <option >Pilih Pendidikan</option>
                    <?php foreach ($pendidikan as $key) : ?>
                      <option value="<?php echo $key->idpendidikan; ?>"><?php echo $key->namapendidikan; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-3 col-form-label">No. Telp</label>
                <div class="col-sm-9">
                  <input type="text" name="notelpkaryawan" class="form-control" id="notelpkaryawan" placeholder="No Telp">
                </div>
              </div>
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-3 col-form-label">Diterima</label>
                <div class="col-sm-9">
                  <input type="text" name="tglterima" class="form-control" id="datepicker" placeholder="Tanggal Diterima">
                </div>
              </div>
              <div class="postgaji">

              </div>

                <!-- <div class="form-group row">
                  <label for="inputPassword3" class="col-sm-3 col-form-label">Gaji Pokok</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="gajipokok" name="gajipokok" data-inputmask='"mask": "9999999"' data-mask>
                  </div>
                </div> -->
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary simpan_karyawan">Simpan</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal-edit-karyawan">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Karyawan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-6">
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-3 col-form-label">ID</label>
                  <div class="col-sm-9">
                    <input type="text" name="idkaryawan" class="form-control" id="idkaryawan" placeholder="ID" readonly="true">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputPassword3" class="col-sm-3 col-form-label">Jenis Karyawan</label>
                  <div class="col-sm-9">
                    <select class="form-control select2 editjenis" name="editjeniskaryawan" id="editjeniskaryawan" style="width: 100%;">
                     <option value="">Pilih Jenis</option>
                     <?php foreach ($jenis as $key) : ?>
                      <option value="<?php echo $key->idjenis; ?>"><?php echo $key->namajenis; ?></option>
                    <?php endforeach; ?>
                  </select>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-3 col-form-label">Nama </label>
              <div class="col-sm-9">
                <input type="text" name="editnamakaryawan" class="form-control" id="editnamakaryawan" placeholder="Nama Karyawan">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword3" class="col-sm-3 col-form-label">Alamat</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="editalamatkaryawan" name="editalamatkaryawan" placeholder="Alamat">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword3" class="col-sm-3 col-form-label">Jabatan</label>
              <div class="col-sm-9">
                <select class="form-control select2" name="editjabatan" id="editjabatan" style="width: 100%;">
                  <option value="">Pilih Jabatan</option>
                  <?php foreach ($jabatan as $key) : ?>
                    <option value="<?php echo $key->idjabatan; ?>"><?php echo $key->namajabatan; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="form-group row">
              <label for="inputPassword3" class="col-sm-3 col-form-label">Pendidikan</label>
              <div class="col-sm-9">
                <select class="form-control select2 " name="editpendidikan" id="editpendidikan" style="width: 100%;">
                  <option >Pilih Pendidikan</option>
                  <?php foreach ($pendidikan as $key) : ?>
                    <option value="<?php echo $key->idpendidikan; ?>"><?php echo $key->namapendidikan; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-3 col-form-label">No. Telp</label>
              <div class="col-sm-9">
                <input type="text" name="editnotelpkaryawan" class="form-control" id="editnotelpkaryawan" placeholder="No Telp">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-3 col-form-label">Diterima</label>
              <div class="col-sm-9">
                <input type="text" name="edittglterima" class="form-control" id="datepicker1" placeholder="Tanggal Diterima">
              </div>
            </div>
            <div class="postgajiedit">

            </div>

                <!-- <div class="form-group row">
                  <label for="inputPassword3" class="col-sm-3 col-form-label">Gaji Pokok</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="editgajipokok" name="editgajipokok" data-inputmask='"mask": "9999999"' data-mask>
                  </div>
                </div> -->
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary simpan_karyawan_edit">Simpan</button>
      </div>
    </div>
  </div>
</div>