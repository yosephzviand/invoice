<table border="">
	<tr>
		<td rowspan="" style="text-align: center;" width="200px"><img src="<?php echo base_url() ?>assets/dist/img/amc.png" width="125px" alt="AdminLTE Logo"></td>
		<td>
			<p style="font-size: 18px"><b>ATOOM MEDIA CONNECT</b></p>
			<p style="font-size: 12px">Jalan Yogya-Wonosari Km. 17,5 Patuk Gunungkidul Yogyakarta 55862</p>
			<p style="font-size: 12px">Telp : 0852-2522-5959 (CS) Email : atomedia_mail@yahoo.com</p>
		</td>
	</tr>
</table>
<hr>

<h3 style="text-align: center;">Data Pembayaran <?= $nama->namapelanggan ?></h3>

<h4>Tahun : <?= $tahun ?></h4>
<table width="100%" cellspacing="0" cellpadding="3" style="font-size: 10pt;" border="1">
	<thead>
		<tr>
			<th>No</th>
			<th>Nama Pelanggan</th>
			<th>Alamat</th>
			<th>Bulan</th>
			<th>Tgl. Pembayaran</th>
			<th>Tagihan</th>
			<th>Pembayaran</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
		$tagihan = 0;
		$bayar = 0;
		foreach ($data as $key) : ?>

			<?php
			$tagihan += $key->harga;
			if ($key->bulan == 1) {
				$bulan = 'Januari';
			} elseif ($key->bulan == 2) {
				$bulan = 'Februari';
			} elseif ($key->bulan == 3) {
				$bulan = 'Maret';
			} elseif ($key->bulan == 4) {
				$bulan = 'April';
			} elseif ($key->bulan == 5) {
				$bulan = 'Mei';
			} elseif ($key->bulan == 6) {
				$bulan = 'Juni';
			} elseif ($key->bulan == 7) {
				$bulan = 'Juli';
			} elseif ($key->bulan == 8) {
				$bulan = 'Agustus';
			} elseif ($key->bulan == 9) {
				$bulan = 'September';
			} elseif ($key->bulan == 10) {
				$bulan = 'Oktober';
			} elseif ($key->bulan == 11) {
				$bulan = 'November';
			} else {
				$bulan = 'Desember';
			}
			?>
			<tr>
				<td style="text-align: center;"><?= $no++ ?></td>
				<td><?= $key->namapelanggan ?></td>
				<td><?= $key->alamatpelanggan ?></td>
				<td><?= $bulan ?></td>
				<td><?= $key->tglpembayaran ?></td>
				<td style="text-align: right;"><?= number_format($key->harga, 0, ',', '.') ?></td>
				<?php if ($key->statusbayar == 1) {
					$bayar += $key->harga;
				?>
					<td style="text-align: right;"><?= number_format($key->harga, 0, ',', '.') ?></td>
				<?php } else { ?>
					<td style="text-align: right;"><?= 0 ?></td>
				<?php } ?>
			</tr>
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="5" style="text-align: center;"><b>TOTAL</b></td>
			<td style="text-align: right;"><b><?= number_format($tagihan, 0, ',', '.') ?></b></td>
			<td style="text-align: right;"><b><?= number_format($bayar, 0, ',', '.') ?></b></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align: center;"><b> KURANG BAYAR</b></td>
			<td colspan="2" style="text-align: center;"><b><?= number_format($tagihan - $bayar, 0, ',', '.') ?></b></td>
		</tr>
	</tfoot>
</table>
