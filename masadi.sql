-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 23, 2021 at 09:44 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `masadi`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `Billing` () RETURNS INT(11) BEGIN
	declare billing char (9);
	DECLARE Y CHAR(2);
	DECLARE M CHAR(2);
	DECLARE URUT INT;
	
	SET M = (SELECT DATE_FORMAT(CURDATE(),'%m'));
	SET Y = (SELECT DATE_FORMAT(CURDATE(),'%yy'));
	SET URUT = (SELECT count(idbilling) from transaksi WHERE MONTH(tglpenetapan)=M);
	
	set billing = CONCAT(Y,M,LPAD(urut+1,4,0));


	RETURN billing;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `NOMERSURAT` () RETURNS CHAR(16) CHARSET utf8mb4 BEGIN
	
	DECLARE NS CHAR(16);
	DECLARE M CHAR(2);
	DECLARE Y CHAR(4);
	DECLARE ROMAWI CHAR(2);
	DECLARE URUT INT;
	
	
	SET M = (SELECT DATE_FORMAT(CURDATE(),'%m'));
	SET Y = (SELECT DATE_FORMAT(CURDATE(),'%Y'));
	SET URUT = (SELECT count(nosurat) from transaksi WHERE MONTH(tglpenetapan)=M);
	SET ROMAWI = CASE 
	WHEN M = 01 THEN
		'I'
	ELSE
		'II'
END;


	set NS = CONCAT(LPAD(urut+1,4,0),'/AMC/',ROMAWI,'/',Y);

	RETURN NS;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `terbilang` (`v_angka` BIGINT) RETURNS VARCHAR(5000) CHARSET utf8mb4 Begin
    Declare sString varchar(30);
    Declare Bil1 varchar(255);
    Declare Bil2 varchar(255);
    Declare STot varchar(255);
    Declare X int;
    Declare Y int;
    Declare Z int;
    Declare Urai varchar(5000);
    SET sString = CAST(v_angka as char);
    SET Urai = '';
    SET X = 0;
    SET Y = 0;
 
    WHILE X <> LENGTH(v_angka) Do
 SET X = X + 1;
 SET sTot = MID(sString, X, 1);
     SET Y = Y + CAST(sTot as unsigned);
     SET Z = LENGTH(sString) - X + 1;
 CASE CAST(sTot as unsigned)
            WHEN 1 THEN
              BEGIN
                 IF (Z = 1 OR Z = 7 OR Z = 10 OR Z = 13) THEN
                  SET Bil1 = 'Satu ';
                 ELSEIF (z = 4) THEN
                  IF (x = 1) THEN
                   SET Bil1 = 'SE';
                  ELSE
                   SET Bil1 = 'Satu';
                  END IF;
                 ELSEIF (Z = 2 OR Z = 5 OR Z = 8 OR Z = 11 OR Z = 14) THEN
                  SET X = X + 1;
                  SET sTot = MID(sString, X, 1);
                  SET Z = LENGTH(sString) - X + 1;
                  SET Bil2 = '';
                    CASE CAST(sTot AS unsigned)
                       WHEN 0 THEN SET Bil1 = 'Sepuluh ';
                       WHEN 1 THEN SET Bil1 = 'Sebelas ';
                       WHEN 2 THEN SET Bil1 = 'Dua Belas ';
                       WHEN 3 THEN SET Bil1 = 'Tiga Belas ';
                       WHEN 4 THEN SET Bil1 = 'Empat Belas ';
                       WHEN 5 THEN SET Bil1 = 'Lima Belas ';
                       WHEN 6 THEN SET Bil1 = 'Enam Belas ';
                       WHEN 7 THEN SET Bil1 = 'Tujuh Belas ';
                       WHEN 8 THEN SET Bil1 = 'Delapan Belas ';
                       WHEN 9 THEN SET Bil1 = 'Sembilan Belas ';
                       ELSE BEGIN END;
                    END CASE;
                ELSE
                    SET Bil1 = 'SE';
                END IF;
            END;
        WHEN 2 THEN SET Bil1 = 'Dua ';
        WHEN 3 THEN SET Bil1 = 'Tiga ';
        WHEN 4 THEN SET Bil1 = 'Empat ';
        WHEN 5 THEN SET Bil1 = 'Lima ';
        WHEN 6 THEN SET Bil1 = 'Enam ';
        WHEN 7 THEN SET Bil1 = 'Tujuh ';
        WHEN 8 THEN SET Bil1 = 'Delapan ';
        WHEN 9 THEN SET Bil1 = 'Sembilan ';
        ELSE SET Bil1 = '';
    END CASE;
 
     IF CAST(sTot as unsigned) > 0 THEN
        IF (Z = 2 OR Z = 5 OR Z = 8 OR Z = 11 OR Z = 14) THEN
            SET Bil2 = 'Puluh ';
            ELSEIF (Z = 3 OR Z = 6 OR Z = 9 OR Z = 12 OR Z = 15) THEN
            SET Bil2 = 'Ratus ';
        ELSE
            SET Bil2 = '';
        END IF;
    ELSE
        SET Bil2 = '';
    END IF;
    IF Y > 0 THEN
        CASE Z
            WHEN 4 THEN BEGIN SET Bil2 = CONCAT(Bil2, 'Ribu '); SET Y = 0; END;
            WHEN 7 THEN BEGIN SET Bil2 = CONCAT(Bil2, 'Juta '); SET Y = 0; END;
            WHEN 10 THEN BEGIN SET Bil2 = CONCAT(Bil2, 'Milyar '); SET Y = 0; END;
            WHEN 13 THEN BEGIN SET Bil2 = CONCAT(Bil2, 'Trilyun '); SET Y = 0; END;
            ELSE BEGIN END;
        END CASE;
    END IF;
    SET Urai = CONCAT(Urai, Bil1, Bil2);
    END WHILE;
   RETURN Urai;
End$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `idarea` int(11) NOT NULL,
  `namaarea` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`idarea`, `namaarea`) VALUES
(1, 'PATUK'),
(2, 'DLINGO'),
(3, 'KARANGMOJO'),
(4, 'PONJONG'),
(5, 'PLAYEN'),
(6, 'BANTUL'),
(7, 'KALASAN'),
(10, 'WONOSARI');

-- --------------------------------------------------------

--
-- Stand-in structure for view `billing`
-- (See below for the actual view)
--
CREATE TABLE `billing` (
`billing` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `idlevel` int(255) NOT NULL,
  `namalevel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`idlevel`, `namalevel`) VALUES
(1, 'Admin'),
(2, 'Karyawan'),
(3, 'Pelanggan');

-- --------------------------------------------------------

--
-- Stand-in structure for view `nomer`
-- (See below for the actual view)
--
CREATE TABLE `nomer` (
`nomer` char(16)
);

-- --------------------------------------------------------

--
-- Table structure for table `paketharga`
--

CREATE TABLE `paketharga` (
  `idpaket` int(255) NOT NULL,
  `namapaket` varchar(255) DEFAULT NULL,
  `hargapaket` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `paketharga`
--

INSERT INTO `paketharga` (`idpaket`, `namapaket`, `hargapaket`) VALUES
(1, 'UP TO 3 Mbps', 250000),
(2, 'UP TO 5 Mbps', 350000),
(3, 'UP TO 7 Mbps', 500000),
(4, 'UP TO 10 Mbps', 750000),
(5, 'UP TO 15 Mbps', 1000000),
(6, 'PEMASANGAN BARU', 0),
(9, 'COSTUM', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `idpelanggan` int(11) NOT NULL,
  `namapelanggan` varchar(255) DEFAULT NULL,
  `alamatpelanggan` varchar(255) DEFAULT NULL,
  `idarea` int(11) DEFAULT NULL,
  `notelp` char(255) DEFAULT NULL,
  `tgldaftar` date DEFAULT NULL,
  `idpaketharga` int(11) DEFAULT NULL,
  `hargapelanggan` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`idpelanggan`, `namapelanggan`, `alamatpelanggan`, `idarea`, `notelp`, `tgldaftar`, `idpaketharga`, `hargapelanggan`) VALUES
(1, 'YOSEPHUS', 'KARANGMOJO', 3, '082320003832', '2021-01-13', 1, 250000),
(2, 'NOVIANTO', 'SUDIMORO, KELOR', 3, '082320003832', '2021-01-13', 1, 250000),
(3, 'BAHRUDIN', 'DLINGO', 2, '082320003832', '2021-01-13', 3, 500000),
(4, 'MERVIA', 'MINOMARTANI', 7, '082320003832', '2021-01-14', 9, 1200000),
(10, 'BRIANY', 'TAWARSARI', 10, '09768789', '2021-01-14', 9, 20000),
(13, 'BURHAN', 'PROLIMAN', 4, '123456', '2021-01-15', 4, 750000),
(14, 'ADI', 'PATUK', 1, '12345', '2021-01-18', 2, 350000),
(15, 'BMT ILMI', 'playen', 5, '000000000', '2021-01-18', 3, 500000),
(16, 'VENDI', 'PALIYAN', 5, '0987', '2021-01-18', 6, 10000),
(17, 'KOI', 'PATUNG', 10, '09887', '2021-01-18', 9, 200000),
(24, 'PAINO', 'BENDUNGAN', 3, '', '2021-01-19', 1, 250000),
(26, 'BONICA', 'SERUT', 1, '', '2021-01-19', 2, 350000);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `idtransaksi` int(100) NOT NULL,
  `idpelanggan` int(100) DEFAULT NULL,
  `tglpenetapan` date DEFAULT NULL,
  `nosurat` varchar(255) NOT NULL,
  `bulan` int(2) DEFAULT NULL,
  `tahun` int(4) DEFAULT NULL,
  `idbilling` int(255) DEFAULT NULL,
  `idpaketharga` int(255) DEFAULT NULL,
  `harga` int(100) DEFAULT NULL,
  `diskon` int(100) DEFAULT NULL,
  `subharga` int(100) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `tglpembayaran` date DEFAULT NULL,
  `statusbayar` int(1) DEFAULT NULL,
  `usertagihan` int(255) DEFAULT NULL,
  `useredittagihan` int(255) DEFAULT NULL,
  `tgledittagihan` date DEFAULT NULL,
  `tgldelbayar` date DEFAULT NULL,
  `userbayar` int(255) DEFAULT NULL,
  `userdelbayar` int(255) DEFAULT NULL,
  `penambahan` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`idtransaksi`, `idpelanggan`, `tglpenetapan`, `nosurat`, `bulan`, `tahun`, `idbilling`, `idpaketharga`, `harga`, `diskon`, `subharga`, `keterangan`, `tglpembayaran`, `statusbayar`, `usertagihan`, `useredittagihan`, `tgledittagihan`, `tgldelbayar`, `userbayar`, `userdelbayar`, `penambahan`) VALUES
(38, 1, '2021-01-19', '0001/AMC/I/2021', 1, 2021, 210140001, 4, 750000, NULL, 850000, NULL, NULL, NULL, 3, 3, '2021-01-19', '2021-01-19', 3, 3, 100000),
(40, 2, '2021-01-19', '0003/AMC/I/2021', 1, 2021, 210110003, 1, 250000, NULL, 350000, NULL, NULL, NULL, 3, 3, '2021-01-19', NULL, NULL, NULL, 100000),
(41, 2, '2021-01-19', '0003/AMC/I/2021', 2, 2021, 210110004, 1, 250000, NULL, 250000, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 0),
(42, 1, '2021-01-19', '0004/AMC/I/2021', 2, 2021, 210140005, 4, 750000, NULL, 750000, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 0),
(43, 3, '2021-01-19', '0005/AMC/I/2021', 1, 2021, 210130006, 3, 500000, NULL, 500000, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 0),
(44, 3, '2021-01-19', '0006/AMC/I/2021', 2, 2021, 210130007, 3, 500000, NULL, 500000, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 0),
(45, 3, '2021-01-19', '0007/AMC/I/2021', 3, 2021, 210130008, 3, 500000, NULL, 500000, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 0),
(46, 1, '2021-01-19', '0008/AMC/I/2021', 3, 2021, 210140009, 4, 750000, NULL, 750000, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 0),
(47, 2, '2021-01-19', '0009/AMC/I/2021', 3, 2021, 210110010, 1, 250000, NULL, 250000, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 0),
(48, 4, '2021-01-19', '0010/AMC/I/2021', 1, 2021, 210190011, 9, 1200000, NULL, 1200000, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 0);

--
-- Triggers `transaksi`
--
DELIMITER $$
CREATE TRIGGER `Billing` BEFORE INSERT ON `transaksi` FOR EACH ROW BEGIN
	declare billing char (9);
	DECLARE Y CHAR(2);
	DECLARE M CHAR(2);
	DECLARE Z CHAR(2);
	DECLARE URUT INT;
	DECLARE A INT;
	
	SET M = (SELECT DATE_FORMAT(CURDATE(),'%m'));
	SET Y = (SELECT DATE_FORMAT(CURDATE(),'%yy'));
	SET Z = (new.idpaketharga);
	
	SET A = (SELECT MAX(idbilling) FROM transaksi);
	
	if A is null then
		SET URUT = (SELECT count(idbilling) from transaksi WHERE MONTH(tglpenetapan)=M);
	else
		SET URUT = (SELECT max(SUBSTR(idbilling,6,4)) from transaksi WHERE MONTH(tglpenetapan)=M);
	end if ;
	
	set billing = CONCAT(Y,M,Z,LPAD(URUT+1,4,0));
set new.idbilling=billing;
end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `NOMORSURAT` BEFORE INSERT ON `transaksi` FOR EACH ROW BEGIN
	
	DECLARE NS CHAR(16);
	DECLARE M CHAR(2);
	DECLARE Y CHAR(4);
	DECLARE ROMAWI CHAR(2);
	DECLARE URUT INT;
	
	
	SET M = (SELECT DATE_FORMAT(CURDATE(),'%m'));
	SET Y = (SELECT DATE_FORMAT(CURDATE(),'%Y'));
	SET URUT = (SELECT count(nosurat) from transaksi WHERE MONTH(tglpenetapan)=M);
	SET ROMAWI = CASE 
	WHEN M = 01 THEN
		'I'
		WHEN M = 02 THEN
		'II'
		WHEN M = 03 THEN
		'III'
		WHEN M = 04 THEN
		'IV'
		WHEN M = 05 THEN
		'V'
		WHEN M = 06 THEN
		'VI'
		WHEN M = 07 THEN
		'VII'
		WHEN M = 08 THEN
		'VIII'
		WHEN M = 09 THEN
		'IX'
		WHEN M = 10 THEN
		'X'
		WHEN M = 11 THEN
		'XI'
	ELSE
		'XII'
END;
	set NS = CONCAT(LPAD(urut+1,4,0),'/AMC/',ROMAWI,'/',Y);
	
	SET NEW.nosurat=NS;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `idusers` int(100) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `idpelanggan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`idusers`, `username`, `password`, `level`, `nama`, `idpelanggan`) VALUES
(2, 'kadis', '827ccb0eea8a706c4c34a16891f84e7b', 2, 'Yosephus', NULL),
(3, 'admin', '827ccb0eea8a706c4c34a16891f84e7b', 1, 'Ngadimin', NULL),
(4, 'bendahara', '827ccb0eea8a706c4c34a16891f84e7b', 2, 'Bendahara', NULL),
(5, 'yosephus', '827ccb0eea8a706c4c34a16891f84e7b', 3, NULL, 1),
(7, 'bahrudin', '827ccb0eea8a706c4c34a16891f84e7b', 3, NULL, 3),
(8, 'Ietha', 'e807f1fcf82d132f9bb018ca6738a19f', 1, 'NOFRITA', NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_grafik`
-- (See below for the actual view)
--
CREATE TABLE `vw_grafik` (
`bulan` int(2)
,`nmbulan` varchar(9)
,`tagihan` decimal(65,0)
,`pembayaran` decimal(65,0)
,`kurangbayar` decimal(65,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_pelanggan`
-- (See below for the actual view)
--
CREATE TABLE `vw_pelanggan` (
`idpelanggan` int(11)
,`namapelanggan` varchar(255)
,`alamatpelanggan` varchar(255)
,`idarea` int(11)
,`namaarea` varchar(255)
,`notelp` char(255)
,`tgldaftar` date
,`namapaket` varchar(255)
,`hargapaket` int(255)
,`idpaketharga` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_tagihan`
-- (See below for the actual view)
--
CREATE TABLE `vw_tagihan` (
`idtransaksi` int(100)
,`idpelanggan` int(11)
,`namapelanggan` varchar(255)
,`alamatpelanggan` varchar(255)
,`namaarea` varchar(255)
,`tglpenetapan` date
,`nosurat` varchar(255)
,`bulan` int(2)
,`nmbulan` varchar(9)
,`tahun` int(4)
,`idbilling` int(255)
,`harga` int(100)
,`penambahan` int(255)
,`subharga` int(100)
,`terbilang` varchar(5000)
,`diskon` int(100)
,`namapaket` varchar(255)
,`tglpembayaran` date
,`statusbayar` int(1)
,`notelp` char(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_users`
-- (See below for the actual view)
--
CREATE TABLE `vw_users` (
`idusers` int(100)
,`username` varchar(255)
,`password` varchar(255)
,`level` int(11)
,`nama` varchar(255)
,`namalevel` varchar(255)
,`namapelanggan` varchar(255)
,`idpelanggan` int(11)
);

-- --------------------------------------------------------

--
-- Structure for view `billing`
--
DROP TABLE IF EXISTS `billing`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `billing`  AS SELECT `Billing`() AS `billing` ;

-- --------------------------------------------------------

--
-- Structure for view `nomer`
--
DROP TABLE IF EXISTS `nomer`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `nomer`  AS SELECT `NOMERSURAT`() AS `nomer` ;

-- --------------------------------------------------------

--
-- Structure for view `vw_grafik`
--
DROP TABLE IF EXISTS `vw_grafik`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_grafik`  AS SELECT `a`.`bulan` AS `bulan`, `a`.`nmbulan` AS `nmbulan`, `a`.`tagihan` AS `tagihan`, `b`.`pembayaran` AS `pembayaran`, `c`.`kurangbayar` AS `kurangbayar` FROM (((select `transaksi`.`bulan` AS `bulan`,case when `transaksi`.`bulan` = 1 then 'Januari' when `transaksi`.`bulan` = 2 then 'Februari' when `transaksi`.`bulan` = 3 then 'Maret' when `transaksi`.`bulan` = 4 then 'April' when `transaksi`.`bulan` = 5 then 'Mei' when `transaksi`.`bulan` = 6 then 'Juni' when `transaksi`.`bulan` = 7 then 'Juli' when `transaksi`.`bulan` = 8 then 'Agustus' when `transaksi`.`bulan` = 9 then 'September' when `transaksi`.`bulan` = 10 then 'Oktober' when `transaksi`.`bulan` = 11 then 'November' else 'Desember' end AS `nmbulan`,sum(`transaksi`.`subharga`) AS `tagihan` from `transaksi` where `transaksi`.`tahun` = extract(year from current_timestamp()) group by `transaksi`.`bulan`) `a` left join (select `transaksi`.`bulan` AS `bulan`,case when `transaksi`.`bulan` = 1 then 'Januari' when `transaksi`.`bulan` = 2 then 'Februari' when `transaksi`.`bulan` = 3 then 'Maret' when `transaksi`.`bulan` = 4 then 'April' when `transaksi`.`bulan` = 5 then 'Mei' when `transaksi`.`bulan` = 6 then 'Juni' when `transaksi`.`bulan` = 7 then 'Juli' when `transaksi`.`bulan` = 8 then 'Agustus' when `transaksi`.`bulan` = 9 then 'September' when `transaksi`.`bulan` = 10 then 'Oktober' when `transaksi`.`bulan` = 11 then 'November' else 'Desember' end AS `nmbulan`,sum(`transaksi`.`subharga`) AS `pembayaran` from `transaksi` where `transaksi`.`tahun` = extract(year from current_timestamp()) and `transaksi`.`tglpembayaran` is not null group by `transaksi`.`bulan`) `b` on(`a`.`bulan` = `b`.`bulan`)) left join (select `transaksi`.`bulan` AS `bulan`,case when `transaksi`.`bulan` = 1 then 'Januari' when `transaksi`.`bulan` = 2 then 'Februari' when `transaksi`.`bulan` = 3 then 'Maret' when `transaksi`.`bulan` = 4 then 'April' when `transaksi`.`bulan` = 5 then 'Mei' when `transaksi`.`bulan` = 6 then 'Juni' when `transaksi`.`bulan` = 7 then 'Juli' when `transaksi`.`bulan` = 8 then 'Agustus' when `transaksi`.`bulan` = 9 then 'September' when `transaksi`.`bulan` = 10 then 'Oktober' when `transaksi`.`bulan` = 11 then 'November' else 'Desember' end AS `nmbulan`,sum(`transaksi`.`subharga`) AS `kurangbayar` from `transaksi` where `transaksi`.`tahun` = extract(year from current_timestamp()) and `transaksi`.`tglpembayaran` is null group by `transaksi`.`bulan`) `c` on(`a`.`bulan` = `c`.`bulan`)) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_pelanggan`
--
DROP TABLE IF EXISTS `vw_pelanggan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_pelanggan`  AS SELECT `pelanggan`.`idpelanggan` AS `idpelanggan`, `pelanggan`.`namapelanggan` AS `namapelanggan`, `pelanggan`.`alamatpelanggan` AS `alamatpelanggan`, `pelanggan`.`idarea` AS `idarea`, `area`.`namaarea` AS `namaarea`, `pelanggan`.`notelp` AS `notelp`, `pelanggan`.`tgldaftar` AS `tgldaftar`, `paketharga`.`namapaket` AS `namapaket`, `pelanggan`.`hargapelanggan` AS `hargapaket`, `pelanggan`.`idpaketharga` AS `idpaketharga` FROM ((`area` join `pelanggan` on(`pelanggan`.`idarea` = `area`.`idarea`)) left join `paketharga` on(`pelanggan`.`idpaketharga` = `paketharga`.`idpaket`)) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_tagihan`
--
DROP TABLE IF EXISTS `vw_tagihan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_tagihan`  AS SELECT `transaksi`.`idtransaksi` AS `idtransaksi`, `vw_pelanggan`.`idpelanggan` AS `idpelanggan`, `vw_pelanggan`.`namapelanggan` AS `namapelanggan`, `vw_pelanggan`.`alamatpelanggan` AS `alamatpelanggan`, `vw_pelanggan`.`namaarea` AS `namaarea`, `transaksi`.`tglpenetapan` AS `tglpenetapan`, `transaksi`.`nosurat` AS `nosurat`, `transaksi`.`bulan` AS `bulan`, CASE WHEN `transaksi`.`bulan` = 1 THEN 'Januari' WHEN `transaksi`.`bulan` = 2 THEN 'Februari' WHEN `transaksi`.`bulan` = 3 THEN 'Maret' WHEN `transaksi`.`bulan` = 4 THEN 'April' WHEN `transaksi`.`bulan` = 5 THEN 'Mei' WHEN `transaksi`.`bulan` = 6 THEN 'Juni' WHEN `transaksi`.`bulan` = 7 THEN 'Juli' WHEN `transaksi`.`bulan` = 8 THEN 'Agustus' WHEN `transaksi`.`bulan` = 9 THEN 'September' WHEN `transaksi`.`bulan` = 10 THEN 'Oktober' WHEN `transaksi`.`bulan` = 11 THEN 'November' ELSE 'Desember' END AS `nmbulan`, `transaksi`.`tahun` AS `tahun`, `transaksi`.`idbilling` AS `idbilling`, `transaksi`.`harga` AS `harga`, `transaksi`.`penambahan` AS `penambahan`, `transaksi`.`subharga` AS `subharga`, `terbilang`(`transaksi`.`subharga`) AS `terbilang`, `transaksi`.`diskon` AS `diskon`, `paketharga`.`namapaket` AS `namapaket`, `transaksi`.`tglpembayaran` AS `tglpembayaran`, `transaksi`.`statusbayar` AS `statusbayar`, `vw_pelanggan`.`notelp` AS `notelp` FROM ((`transaksi` join `vw_pelanggan` on(`transaksi`.`idpelanggan` = `vw_pelanggan`.`idpelanggan`)) join `paketharga` on(`paketharga`.`idpaket` = `transaksi`.`idpaketharga`)) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_users`
--
DROP TABLE IF EXISTS `vw_users`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_users`  AS SELECT `users`.`idusers` AS `idusers`, `users`.`username` AS `username`, `users`.`password` AS `password`, `users`.`level` AS `level`, `users`.`nama` AS `nama`, `level`.`namalevel` AS `namalevel`, `pelanggan`.`namapelanggan` AS `namapelanggan`, `users`.`idpelanggan` AS `idpelanggan` FROM ((`level` join `users` on(`level`.`idlevel` = `users`.`level`)) left join `pelanggan` on(`users`.`idpelanggan` = `pelanggan`.`idpelanggan`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`idarea`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`idlevel`);

--
-- Indexes for table `paketharga`
--
ALTER TABLE `paketharga`
  ADD PRIMARY KEY (`idpaket`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`idpelanggan`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`idtransaksi`,`nosurat`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idusers`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `idarea` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `idlevel` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `paketharga`
--
ALTER TABLE `paketharga`
  MODIFY `idpaket` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `idpelanggan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `idtransaksi` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `idusers` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
