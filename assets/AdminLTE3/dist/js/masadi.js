jQuery(document).ready(function($) {
    // $('.select2').select2();

    $(".bt_simpan_entry").attr('disabled', 'disabled');

    $('#datepicker').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
    });

    $('#timepicker').datetimepicker({
        // autoclose: true,
        format: 'HH:mm:ss'
    })

    $('#timepicker1').datetimepicker({
        autoclose: true,
        format: 'HH:mm:ss'
    })

    $('#timepicker2').datetimepicker({
        autoclose: true,
        format: 'HH:mm:ss'
    })

    $('#datepicker1').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
    });

    $('#datepicker2').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
    });

    $('#datepicker3').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
    });

    area = $('#tbarea').DataTable({

        "ajax": {
            "url": site_url + "amc/ajaxarea",
            "type": "POST"
        },

    });

    paket = $('#tbpaketharga').DataTable({

        "ajax": {
            "url": site_url + "amc/ajaxharga",
            "type": "POST"
        },

    });

    pelanggan = $('#tbpelanggan').DataTable({

        "ajax": {
            "url": site_url + "amc/ajaxpelanggan",
            "type": "POST"
        },

    });

    pengguna = $('#tbpengguna').DataTable({

        "ajax": {
            "url": site_url + "amc/ajaxpengguna",
            "type": "POST"
        },

    });

    tbtagihan = $('#tbtagihan').DataTable({

        "ajax": {
            "url": site_url + "amc/ajaxtagihan",
            "type": "POST"
        },

    });

    tbpembayaran = $('#tbpembayaran').DataTable({

        "ajax": {
            "url": site_url + "amc/ajaxpembayaran",
            "type": "POST"
        },

    });

    tbuserpelanggan = $('#tbuserpelanggan').DataTable({

        "ajax": {
            "url": site_url + "amc/ajaxuserpelanggan",
            "type": "POST"
        },

    });

    tbkaryawan = $('#tbkaryawan').DataTable({

        "ajax": {
            "url": site_url + "amc/ajaxkaryawan",
            "type": "POST"
        },

    });

    tbgajian = $('#tbgajian').DataTable({

        "ajax": {
            "url": site_url + "amc/ajaxgajian",
            "type": "POST"
        },

    });

    tbbarang = $('#tbbarang').DataTable({

        "ajax": {
            "url": site_url + "amc/ajaxbarang",
            "type": "POST"
        },

    });

    tbabsen = $('#tbabsen').DataTable({

        "ajax": {
            "url": site_url + "amc/ajaxabsenkaryawan",
            "type": "POST"
        },

    });


    $('.content-wrapper').on('click', '.editarea', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/get_editarea",
            data: { id: id },
            dataType: "JSON",
            success: function(data) {
                for (i in data) {
                    $('[name="idarea"]').val(data[i].idarea);
                    $('[name="editnamaarea"]').val(data[i].namaarea);
                    $('#modal-edit-area').modal('show');
                }
            }
        })
    });

    $('.content-wrapper').on('click', '.simpan_area', function(e) {
        e.preventDefault();
        var namaarea = $('#namaarea').val();
        if (namaarea == '') {
            swal({
                title: "Error!",
                text: "Nama Area Harus Disi !!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/insertarea",
                data: { namaarea: namaarea },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-area').modal('hide');
                    area.ajax.reload(null, false);
                }
            })
        }

    });

    $('.content-wrapper').on('click', '.simpan_area_edit', function(e) {
        e.preventDefault();
        var idarea = $('#idarea').val();
        var namaarea = $('#editnamaarea').val();
        if (namaarea == '') {
            swal({
                title: "Error!",
                text: "Nama Area Harus Disi !!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/updatearea",
                data: { idarea: idarea, namaarea: namaarea },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-edit-area').modal('hide');
                    area.ajax.reload(null, false);
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.hapusarea', function(e) {
        e.preventDefault();
        var idarea = $(this).data('kode');
        swal({
                title: 'Delete Data',
                text: 'Yakin Ingin Menghapus Data ?',
                html: true,
                confirmButtonColor: '#d9534f',
                showCancelButton: true,
            },
            function() {
                $.ajax({
                    type: 'POST',
                    url: site_url + "amc/deletearea",
                    data: { idarea: idarea },
                    dataType: "JSON",
                    success: function(data) {
                        console.log(data);
                        toastr[data.alert](data.data, data.title);
                        area.ajax.reload(null, false);
                    }
                });
            });
    });

    $('.content-wrapper').on('click', '.editpaket', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/get_editpaket",
            data: { id: id },
            dataType: "JSON",
            success: function(data) {
                for (i in data) {
                    $('[name="idpaket"]').val(data[i].idpaket);
                    $('[name="editnamapaket"]').val(data[i].namapaket);
                    $('[name="edithargapaket"]').val(data[i].hargapaket);
                    $('#modal-edit-paket').modal('show');
                }
            }
        })
    });

    $('.content-wrapper').on('click', '.simpan_paket', function(e) {
        e.preventDefault();
        var namapaket = $('#namapaket').val();
        var hargapaket = $('#hargapaket').val();
        if (namapaket == '' || hargapaket == '') {
            swal({
                title: "Error!",
                text: "Harus Disi Semua!!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/insertpaket",
                data: { namapaket: namapaket, hargapaket: hargapaket },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-paket').modal('hide');
                    paket.ajax.reload(null, false);
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.simpan_paket_edit', function(e) {
        e.preventDefault();
        var idpaket = $('#idpaket').val();
        var namapaket = $('#editnamapaket').val();
        var hargapaket = $('#edithargapaket').val();
        if (namapaket == '' || hargapaket == '') {
            swal({
                title: "Error!",
                text: "Harus Disi Semua!!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/updatepaket",
                data: { idpaket: idpaket, namapaket: namapaket, hargapaket: hargapaket },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-edit-paket').modal('hide');
                    paket.ajax.reload(null, false);
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.hapuspaket', function(e) {
        e.preventDefault();
        var idpaket = $(this).data('kode');
        swal({
                title: 'Delete Data',
                text: 'Yakin Ingin Menghapus Data ?',
                html: true,
                confirmButtonColor: '#d9534f',
                showCancelButton: true,
            },
            function() {
                $.ajax({
                    type: 'POST',
                    url: site_url + "amc/deletepaket",
                    data: { idpaket: idpaket },
                    dataType: "JSON",
                    success: function(data) {
                        console.log(data);
                        toastr[data.alert](data.data, data.title);
                        paket.ajax.reload(null, false);
                    }
                })
            })
    });

    $('.content-wrapper').on('click', '.editpelanggan', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/get_editpelanggan",
            data: { id: id },
            dataType: "JSON",
            success: function(data) {
                for (i in data) {
                    $('[name="idpelanggan"]').val(data[i].idpelanggan);
                    $('[name="editnamapelanggan"]').val(data[i].namapelanggan);
                    $('[name="editalamatpelanggan"]').val(data[i].alamatpelanggan);
                    $('[name="editnotelp"]').val(data[i].notelp);
                    $('[name="editareapelanggan"]').val(data[i].idarea);
                    $('#modal-edit-pelanggan').modal('show');
                }
            }
        })
    });

    $('.content-wrapper').on('click', '.simpan_pelanggan', function(e) {
        e.preventDefault();
        var namapelanggan = $('#namapelanggan').val();
        var alamatpelanggan = $('#alamatpelanggan').val();
        var areapelanggan = $('#areapelanggan').val();
        var notelp = $('#notelp').val();
        var paket = $('#paketpelanggan').val();
        var harga = $('#hargapaket').val();
        var sales = $('#sales').val();
        if (namapelanggan == '' || alamatpelanggan == '' || areapelanggan == '' || notelp == '' || paket == '' || harga == '') {
            swal({
                title: "Error!",
                text: "Harus Disi Semua!!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/insertpelanggan",
                data: { namapelanggan: namapelanggan, alamatpelanggan: alamatpelanggan, areapelanggan: areapelanggan, notelp: notelp, paket: paket, harga: harga, sales: sales },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-pelanggan').modal('hide');
                    pelanggan.ajax.reload(null, false);
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.simpan_pelanggan_edit', function(e) {
        e.preventDefault();
        var idpelanggan = $('#idpelanggan').val();
        var namapelanggan = $('#editnamapelanggan').val();
        var alamatpelanggan = $('#editalamatpelanggan').val();
        var areapelanggan = $('#editareapelanggan').val();
        var notelp = $('#editnotelp').val();
        var paket = $('#editpaketpelanggan').val();
        var harga = $('#edithargapaket').val();
        var sales = $('#editsales').val();
        if (namapelanggan == '' || alamatpelanggan == '' || areapelanggan == '' || notelp == '' || paket == '' || harga == '') {
            swal({
                title: "Error!",
                text: "Harus Disi Semua!!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/updatepelanggan",
                data: { idpelanggan: idpelanggan, namapelanggan: namapelanggan, alamatpelanggan: alamatpelanggan, areapelanggan: areapelanggan, notelp: notelp, paket: paket, harga: harga, sales: sales },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-edit-pelanggan').modal('hide');
                    pelanggan.ajax.reload(null, false);
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.hapuspelanggan', function(e) {
        e.preventDefault();
        var idpelanggan = $(this).data('kode');
        swal({
                title: 'Delete Data',
                text: 'Yakin Ingin Menghapus Data ?',
                html: true,
                confirmButtonColor: '#d9534f',
                showCancelButton: true,
            },
            function() {
                $.ajax({
                    type: 'POST',
                    url: site_url + "amc/deletepelanggan",
                    data: { idpelanggan: idpelanggan },
                    dataType: "JSON",
                    success: function(data) {
                        console.log(data);
                        toastr[data.alert](data.data, data.title);
                        pelanggan.ajax.reload(null, false);
                    }
                })
            })
    });


    $('.content-wrapper').on('click', '.berhentipelanggan', function(e) {
        e.preventDefault();
        var idpelanggan = $(this).data('kode');
        swal({
                title: 'Pelanggan Berhenti ??',
                // text: 'Yakin Ingin Menghapus Data ?',
                html: true,
                confirmButtonColor: '#d9534f',
                showCancelButton: true,
            },
            function() {
                $.ajax({
                    type: 'POST',
                    url: site_url + "amc/berhenti",
                    data: { idpelanggan: idpelanggan },
                    dataType: "JSON",
                    success: function(data) {
                        console.log(data);
                        toastr[data.alert](data.data, data.title);
                        pelanggan.ajax.reload(null, false);
                    }
                })
            })
    });

    $('.content-wrapper').on('click', '.bukapelanggan', function(e) {
        e.preventDefault();
        var idpelanggan = $(this).data('kode');
        swal({
                title: 'Berlangganan Kembali',
                // text: 'Yakin Ingin Menghapus Data ?',
                html: true,
                confirmButtonColor: '#d9534f',
                showCancelButton: true,
            },
            function() {
                $.ajax({
                    type: 'POST',
                    url: site_url + "amc/buka",
                    data: { idpelanggan: idpelanggan },
                    dataType: "JSON",
                    success: function(data) {
                        console.log(data);
                        toastr[data.alert](data.data, data.title);
                        pelanggan.ajax.reload(null, false);
                    }
                })
            })
    });

    $('.content-wrapper').on('click', '.editpengguna', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/get_editpengguna",
            data: { id: id },
            dataType: "JSON",
            success: function(data) {
                for (i in data) {
                    $('[name="idpengguna"]').val(data[i].idusers);
                    $('[name="editnamapengguna"]').val(data[i].nama);
                    $('[name="editusername"]').val(data[i].username);
                    $('#modal-edit-pengguna').modal('show');
                }
            }
        })
    });

    $('.content-wrapper').on('click', '.simpan_pengguna', function(e) {
        e.preventDefault();
        var namapengguna = $('#namapengguna').val();
        var username = $('#username').val();
        var password = $('#password').val();
        var level = $('#level').val();
        if (namapengguna == '' || username == '' || password == '' || level == '') {
            swal({
                title: "Error!",
                text: "Harus Disi Semua!!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/insertpengguna",
                data: { namapengguna: namapengguna, username: username, password: password, level: level },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-pengguna').modal('hide');
                    pengguna.ajax.reload(null, false);
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.simpan_pengguna_edit', function(e) {
        e.preventDefault();
        var idpengguna = $('#idpengguna').val();
        var editnamapengguna = $('#editnamapengguna').val();
        var editusername = $('#editusername').val();
        var editpassword = $('#editpassword').val();
        var editlevel = $('#editlevel').val();
        if (editnamapengguna == '' || editusername == '' || editpassword == '' || editlevel == '') {
            swal({
                title: "Error!",
                text: "Harus Disi Semua!!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/updatepengguna",
                data: { idpengguna: idpengguna, editnamapengguna: editnamapengguna, editusername: editusername, editpassword: editpassword, editlevel: editlevel },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-edit-pengguna').modal('hide');
                    pengguna.ajax.reload(null, false);
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.hapuspengguna', function(e) {
        e.preventDefault();
        var idpengguna = $(this).data('kode');
        swal({
                title: 'Delete Data',
                text: 'Yakin Ingin Menghapus Data ?',
                html: true,
                confirmButtonColor: '#d9534f',
                showCancelButton: true,
            },
            function() {
                $.ajax({
                    type: 'POST',
                    url: site_url + "amc/deletepengguna",
                    data: { idpengguna: idpengguna },
                    dataType: "JSON",
                    success: function(data) {
                        console.log(data);
                        toastr[data.alert](data.data, data.title);
                        pengguna.ajax.reload(null, false);
                    }
                })
            })
    });

    $('.content-wrapper').on('click', '.edittagihan', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/get_edittagihan",
            data: { id: id },
            dataType: "JSON",
            success: function(data) {
                for (i in data) {
                    $('[name="idtagihan"]').val(data[i].idtransaksi);
                    $('[name="editbulan"]').val(data[i].bulan);
                    $('[name="edittambahan"]').val(data[i].penambahan);
                    $('[name="editnamapelanggan"]').val(data[i].namapelanggan);
                    $('[name="editpakettagihan"]').val(data[i].namapaket);
                    $('[name="edittagihan"]').val(data[i].harga);
                    $('#modal-edit-tagihan').modal('show');
                }
            }
        })
    });

    $('.content-wrapper').on('click', '.simpan_tagihan', function(e) {
        e.preventDefault();
        var pelanggan = $('#pelanggan').val();
        var bulan = $('#bulan').val();
        var paket = $('#idpakettagihan').val();
        var harga = $('#tagihan').val();
        var tagihan = $('#totaltagihan').val();
        var tambahan = $('#tambahan').val();
        if (pelanggan == '' || bulan == '' || paket == '' || harga == '' || tagihan == '' || tambahan == '') {
            swal({
                title: "Error!",
                text: "Harus Disi Semua!!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/inserttagihan",
                data: { pelanggan: pelanggan, bulan: bulan, paket: paket, tagihan: tagihan, harga: harga, tambahan: tambahan },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-tagihan').modal('hide');
                    tbtagihan.ajax.reload(null, false);
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.simpan_tagihan_edit', function(e) {
        e.preventDefault();
        var idtagihan = $('#idtagihan').val();
        var bulan = $('#editbulan').val();
        var tagihan = $('#edittotaltagihan').val();
        var tambahan = $('#edittambahan').val();
        if (bulan == '' || tagihan == '' || tambahan == '') {
            swal({
                title: "Error!",
                text: "Harus Disi Semua!!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/updatetagihan",
                data: { idtagihan: idtagihan, bulan: bulan, tagihan: tagihan, tambahan: tambahan },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-edit-tagihan').modal('hide');
                    tbtagihan.ajax.reload(null, false);
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.hapustagihan', function(e) {
        e.preventDefault();
        var idtransaksi = $(this).data('kode');
        swal({
                title: 'Delete Data',
                text: 'Yakin Ingin Menghapus Data ?',
                html: true,
                confirmButtonColor: '#d9534f',
                showCancelButton: true,
            },
            function() {
                $.ajax({
                    type: 'POST',
                    url: site_url + "amc/deletetagihan",
                    data: { idtransaksi: idtransaksi },
                    dataType: "JSON",
                    success: function(data) {
                        console.log(data);
                        toastr[data.alert](data.data, data.title);
                        tbtagihan.ajax.reload(null, false);
                    }
                })
            })
    });

    $('.content-wrapper').on('click', '.pdftagihan', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        window.open(site_url + "amc/pdf/" + id, '_blank');
    });


    $('.content-wrapper').on('click', '.bayar', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/get_editpembayaran",
            data: { id: id },
            dataType: "JSON",
            success: function(data) {
                for (i in data) {
                    $('[name="idtransaksi"]').val(data[i].idtransaksi);
                    $('[name="idbilling"]').val(data[i].idbilling);
                    $('[name="namapelanggan"]').val(data[i].namapelanggan);
                    $('[name="bulan"]').val(data[i].nmbulan);
                    $('[name="paket"]').val(data[i].namapaket);
                    $('[name="tagihan"]').val(rubah(data[i].subharga));
                    $('#modal-pembayaran').modal('show');
                }
            }
        })
    });

    $('.content-wrapper').on('click', '.simpan_pembayaran', function(e) {
        e.preventDefault();
        var id = $('#idtransaksi').val();
        console.log(id);
        $.ajax({
            type: 'POST',
            url: site_url + "amc/insertpembayaran",
            data: { id: id },
            dataType: "JSON",
            success: function(data) {
                console.log(data);
                toastr[data.alert](data.data, data.title);
                $('#modal-pembayaran').modal('hide');
                tbpembayaran.ajax.reload(null, false);
            }
        })
    });

    $('.content-wrapper').on('click', '.hapuspembayaran', function(e) {
        e.preventDefault();
        // var id = $('#idtransaksi').val();
        var id = $(this).data('kode');
        swal({
                title: 'Delete Data',
                text: 'Yakin Ingin Menghapus Data ?',
                html: true,
                confirmButtonColor: '#d9534f',
                showCancelButton: true,
            },
            function() {
                $.ajax({
                    type: 'POST',
                    url: site_url + "amc/updatehapuspembayaran",
                    data: { id: id },
                    dataType: "JSON",
                    success: function(data) {
                        console.log(data);
                        toastr[data.alert](data.data, data.title);
                        $('#modal-hapus-pembayaran').modal('hide');
                        tbpembayaran.ajax.reload(null, false);
                    }
                })
            })
    });

    $('.content-wrapper').on('click', '.pdfpembayaran', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        window.open(site_url + "amc/pdfpembayaran/" + id, '_blank');
    });


    $('.content-wrapper').on('click', '.edituserpelanggan', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/get_editpengguna",
            data: { id: id },
            dataType: "JSON",
            success: function(data) {
                for (i in data) {
                    $('[name="idpenggunauser"]').val(data[i].idusers);
                    $('[name="editnamapenggunauser"]').val(data[i].namapelanggan);
                    $('[name="editusernameuser"]').val(data[i].username);
                    $('#modal-edit-penggunauser').modal('show');
                }
            }
        })
    });

    $('.content-wrapper').on('click', '.simpan_user_pelanggan', function(e) {
        e.preventDefault();
        var pelangganuser = $('#pelangganuser').val();
        var usernameuser = $('#usernameuser').val();
        var passworduser = $('#passworduser').val();
        var leveluser = $('#leveluser').val();
        if (pelangganuser == '' || usernameuser == '' || passworduser == '' || leveluser == '') {
            swal({
                title: "Error!",
                text: "Harus Disi Semua!!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/insertuserpelanggan",
                data: { pelangganuser: pelangganuser, usernameuser: usernameuser, passworduser: passworduser, leveluser: leveluser },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-userpelanggan').modal('hide');
                    tbuserpelanggan.ajax.reload(null, false);
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.simpan_penggunauser_edit', function(e) {
        e.preventDefault();
        var idpenggunauser = $('#idpenggunauser').val();
        var editusernameuser = $('#editusernameuser').val();
        var editpassworduser = $('#editpassworduser').val();
        if (editusernameuser == '' || editpassworduser == '') {
            swal({
                title: "Error!",
                text: "Harus Disi Semua!!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/updateuserpelanggan",
                data: { idpenggunauser: idpenggunauser, editusernameuser: editusernameuser, editpassworduser: editpassworduser },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-edit-penggunauser').modal('hide');
                    tbuserpelanggan.ajax.reload(null, false);
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.hapususerpelanggan', function(e) {
        e.preventDefault();
        var idpenggunauser = $(this).data('kode');
        swal({
                title: 'Delete Data',
                text: 'Yakin Ingin Menghapus Data ?',
                html: true,
                confirmButtonColor: '#d9534f',
                showCancelButton: true,
            },
            function() {
                $.ajax({
                    type: 'POST',
                    url: site_url + "amc/deleteuserpelanggan",
                    data: { idpenggunauser: idpenggunauser },
                    dataType: "JSON",
                    success: function(data) {
                        console.log(data);
                        toastr[data.alert](data.data, data.title);
                        tbuserpelanggan.ajax.reload(null, false);
                    }
                })
            })
    });

    //Karyawan

    $('.content-wrapper').on('click', '.editkaryawan', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/get_editkaryawan",
            data: { id: id },
            dataType: "JSON",
            success: function(data) {
                for (i in data) {
                    $('[name="idkaryawan"]').val(data[i].idkaryawan);
                    $('[name="editnamakaryawan"]').val(data[i].namakaryawan);
                    $('[name="editalamatkaryawan"]').val(data[i].alamatkaryawan);
                    $('[name="editgajipokok"]').val(data[i].gajipokok);
                    $('[name="editnotelpkaryawan"]').val(data[i].notelp);
                    $('[name="edittglterima"]').val(data[i].masakerja);
                    $('[name="editpendidikan"]').val(data[i].pendidikan);
                    $('[name="editjabatan"]').val(data[i].jabatan);
                    $('[name="editidkartu"]').val(data[i].idkartu);
                    $('#modal-edit-karyawan').modal('show');
                }
            }
        })
    });


    $('.content-wrapper').on('click', '.simpan_karyawan', function(e) {
        e.preventDefault();
        var nmkaryawan = $('#nmkaryawan').val();
        var alamatkaryawan = $('#alamatkaryawan').val();
        var jabatan = $('#jabatan').val();
        var pendidikan = $('#pendidikan').val();
        var tglterima = $('#datepicker').val();
        var gajipokok = $('#gajipokok').val();
        var notelp = $('#notelpkaryawan').val();
        var jnskaryawan = $('#jnskaryawan').val();
        var idkartu = $('#idkartu').val();

        if (jnskaryawan == '' || nmkaryawan == '' || alamatkaryawan == '' || jabatan == '' || pendidikan == '' || tglterima == '' || notelp == '') {
            swal({
                title: "Error!",
                text: "Harus Disi Semua!!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/insertkaryawan",
                data: {
                    jnskaryawan: jnskaryawan,
                    nmkaryawan: nmkaryawan,
                    alamatkaryawan: alamatkaryawan,
                    jabatan: jabatan,
                    pendidikan: pendidikan,
                    tglterima: tglterima,
                    gajipokok,
                    gajipokok,
                    notelp: notelp,
                    idkartu: idkartu,
                },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-karyawan').modal('hide');
                    tbkaryawan.ajax.reload(null, false);
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.simpan_karyawan_edit', function(e) {
        e.preventDefault();
        var idkaryawan = $('#idkaryawan').val();
        var namakaryawan = $('#editnamakaryawan').val();
        var alamatkaryawan = $('#editalamatkaryawan').val();
        var jabatan = $('#editjabatan').val();
        var pendidikan = $('#editpendidikan').val();
        var tglterima = $('#datepicker1').val();
        var gajipokok = $('#editgajipokok').val();
        var notelp = $('#editnotelpkaryawan').val();
        var jeniskaryawan = $('#editjeniskaryawan').val();
        var idkartu = $('#editidkartu').val();

        if (jeniskaryawan == '' || namakaryawan == '' || alamatkaryawan == '' || jabatan == '' || pendidikan == '' || tglterima == '' || notelp == '') {
            swal({
                title: "Error!",
                text: "Harus Disi Semua!!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/updatekaryawan",
                data: {
                    idkaryawan: idkaryawan,
                    jeniskaryawan: jeniskaryawan,
                    namakaryawan: namakaryawan,
                    alamatkaryawan: alamatkaryawan,
                    jabatan: jabatan,
                    pendidikan: pendidikan,
                    tglterima: tglterima,
                    gajipokok,
                    gajipokok,
                    notelp: notelp,
                    idkartu: idkartu

                },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-edit-karyawan').modal('hide');
                    tbkaryawan.ajax.reload(null, false);
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.hapuskaryawan', function(e) {
        e.preventDefault();
        var idkaryawan = $(this).data('kode');
        swal({
                title: 'Delete Data',
                text: 'Yakin Ingin Menghapus Data ?',
                html: true,
                confirmButtonColor: '#d9534f',
                showCancelButton: true,
            },
            function() {
                $.ajax({
                    type: 'POST',
                    url: site_url + "amc/deletekaryawan",
                    data: { idkaryawan: idkaryawan },
                    dataType: "JSON",
                    success: function(data) {
                        console.log(data);
                        toastr[data.alert](data.data, data.title);
                        tbkaryawan.ajax.reload(null, false);
                    }
                });
            });
    });

    // Gaji Karyawan

    $('.content-wrapper').on('click', '.editgajian', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/get_editgajian",
            data: { id: id },
            dataType: "JSON",
            success: function(data) {
                for (i in data) {
                    $('[name="idgajian"]').val(data[i].idgaji);
                    $('[name="editnamakaryawan"]').val(data[i].namakaryawan);
                    $('[name="edittahun"]').val(data[i].tahun);
                    $('[name="editbulan"]').val(data[i].bulan);
                    $('[name="editgajipokok"]').val(data[i].gajipokok);
                    $('[name="edittunjangan"]').val(data[i].tunjangan);
                    $('[name="editpotongan"]').val(data[i].potongan);
                    $('#modal-edit-gajian').modal('show');
                }
            }
        })
    });

    $('.content-wrapper').on('click', '.simpan_gajian', function(e) {
        e.preventDefault();
        var namakaryawan = $('#namakaryawan').val();
        // var tahun =  $('#tahun').val();
        var bulan = $('#bulan').val();
        var gajipokok = $('#gajipokok').val();
        var tunjangan = $('#tunjangan').val();
        var potongan = $('#potongan').val();
        var gajibersih = $('#gajibersih').val();
        if (namakaryawan == '' || gajibersih == '' || gajipokok == '' || bulan == '' || tunjangan == '' || potongan == '') {
            swal({
                title: "Error!",
                text: "Harus Disi Semua!!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/insertgajian",
                data: { namakaryawan: namakaryawan, bulan: bulan, gajipokok: gajipokok, tunjangan: tunjangan, potongan, potongan, gajibersih: gajibersih },
                dataType: "JSON",
                success: function(data, status) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-gajian').modal('hide');
                    tbgajian.ajax.reload(null, false);

                    if (status == 'success') {
                        $('#namakaryawan').val(null).trigger('change');
                        $('#bulan').val(null).trigger('change');
                        $('#gajipokok').val('');
                        $('#tunjangan').val('');
                        $('#potongan').val('');
                        $('#gajibersih').val('');

                    }
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.simpan_gajian_edit', function(e) {
        e.preventDefault();
        var idgajian = $('#idgajian').val();
        // var tahun =  $('#edittahun').val();
        var bulan = $('#editbulan').val();
        var gajipokok = $('#editgajipokok').val();
        var tunjangan = $('#edittunjangan').val();
        var potongan = $('#editpotongan').val();
        var gajibersih = $('#editgajibersih').val();
        if (gajibersih == '' || gajipokok == '' || bulan == '' || tunjangan == '' || potongan == '') {
            swal({
                title: "Error!",
                text: "Harus Disi Semua!!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/updategajian",
                data: { idgajian: idgajian, bulan: bulan, gajipokok: gajipokok, tunjangan: tunjangan, potongan, potongan, gajibersih: gajibersih },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-edit-gajian').modal('hide');
                    tbgajian.ajax.reload(null, false);
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.hapusgajian', function(e) {
        e.preventDefault();
        var idgaji = $(this).data('kode');
        swal({
                title: 'Delete Data',
                text: 'Yakin Ingin Menghapus Data ?',
                html: true,
                confirmButtonColor: '#d9534f',
                showCancelButton: true,
            },
            function() {
                $.ajax({
                    type: 'POST',
                    url: site_url + "amc/deletegajian",
                    data: { idgaji: idgaji },
                    dataType: "JSON",
                    success: function(data) {
                        console.log(data);
                        toastr[data.alert](data.data, data.title);
                        tbgajian.ajax.reload(null, false);
                    }
                });
            });
    });

    $('.content-wrapper').on('click', '.pdfgajian', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        window.open(site_url + "amc/pdfgajian/" + id, '_blank');
    });

    $('.content-wrapper').on('click', '.editbarang', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/get_editbarang",
            data: { id: id },
            dataType: "JSON",
            success: function(data, status) {
                for (i in data) {
                    $('#modal_form_layanan01').modal('hide');
                    $('[name="idbarang"]').val(data[i].idbarang);
                    $('[name="editnamabarang"]').val(data[i].namabarang);
                    $('[name="editserial"]').val(data[i].serial);
                    $('[name="editjumlahbarang"]').val(data[i].jumlah);
                    $('[name="edithargabeli"]').val(data[i].hargasatuan);
                    $('[name="total"]').val(data[i].harga);
                    $('#modal-edit-barang').modal('show');

                }
            }
        })
    });

    $('.content-wrapper').on('click', '.simpan_barang_edit', function(e) {
        e.preventDefault();
        var idbarang = $('#idbarang').val();
        var namabarang = $('#editnamabarang').val();
        var serial = $('#editserial').val();
        var jumlah = $('#editjumlahbarang').val();
        var harga = $('#edithargabeli').val();
        var total = $('#total').val();
        if (namabarang == '' || total == '' || jumlah == '' || harga == '') {
            swal({
                title: "Error!",
                text: "Harus Disi Semua!!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/updatebarang",
                data: { idbarang: idbarang, namabarang: namabarang, serial: serial, jumlah: jumlah, harga: harga, total, total },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-edit-barang').modal('hide');
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.hapusbarang', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        swal({
                title: 'Delete Data',
                text: 'Yakin Ingin Menghapus Data ?',
                html: true,
                confirmButtonColor: '#d9534f',
                showCancelButton: true,
            },
            function() {
                $.ajax({
                    type: 'POST',
                    url: site_url + "amc/deletebarang",
                    data: { id: id },
                    dataType: "JSON",
                    success: function(data) {
                        console.log(data);
                        toastr[data.alert](data.data, data.title);
                        $('#modal_form_layanan01').modal('hide');
                    }
                });
            });
    });

    var detailObj = {};

    var arrDataPel = {};
    var arrDataSer = {};
    var arrDatajum = {};
    var arrDataharga = {};
    var arrDataNam = {};
    var arrDataTot = {};

    $('body').on('click', '.simpan_keluar', function(e) {
        var id = $('#barangkeluar').val();
        var jumlah = $('#jumlahbarang').val();
        var namabarang = $('#namabarang').val();
        var harga = $('#hargabeli').val();
        var serial = $('#serial').val();

        var total = '';
        if (id == '' || jumlah == '' || namabarang == '' || harga == '') {
            swal({
                title: "Error!",
                text: "Harus di isi !!",
                type: "warning",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });

            return false;
        } else {
            var sumtotal = 0;
            var sumjumlah = 0;
            var sumharga = 0;
            $.ajax({
                type: 'POST',
                url: site_url + "amc/get_editpelanggan",
                data: { id: id },
                dataType: "JSON",
                success: function(data, status) {

                    for (i in data) {
                        detail_idx = Object.keys(detailObj).length;
                        detailObj[detail_idx] = (data);
                        total = jumlah * harga;

                        $('#tb_pembelian').append('<tr data-idx="' + detail_idx + '">' +
                            '<td class="nama">' + namabarang + '</td> <td class="serial">' + serial + '</td>' +
                            '<td class"jumlah">' + jumlah + '</td> <td class="hargajual">' + harga + '</td>  <td>' + total + '</td>' +
                            // '<td> <button class="btn btn-sm btn-danger del_pem"><i class="fa fa-trash-alt"></i></button></td>'+
                            '</tr>');
                        $(".bt_simpan_entry").removeAttr('disabled');

                        arrDataPel[detail_idx] = (data[i].idpelanggan);
                        arrDataSer[detail_idx] = (serial);
                        arrDatajum[detail_idx] = (jumlah);
                        arrDataharga[detail_idx] = (harga);
                        arrDataNam[detail_idx] = (namabarang);
                        arrDataTot[detail_idx] = (total);
                    }
                    // $.each(arrDatatot,function(){sumtotal+=parseFloat(this) || 0;});
                    // $('[name="total"]').val(sumtotal);


                    console.log(status);

                    if (status == 'success') {
                        // $('#barangkeluar').val(null).trigger('change');
                        $('#jumlahbarang').val('');
                        $('#hargabeli').val('');
                        $('#serial').val('');
                        $('#namabarang').val('');

                    }


                }
            })
        }

    });

    $('body').on('click', '.bt_simpan_entry', function(e) {

        dt_pend = $('form#pembelian').serializeArray();
        total = $('#total').val();
        bayar = $('#bayar').val();
        sisa = $('#sisa').val();
        if (bayar == '' || sisa == '') {
            swal({
                title: "Error!",
                text: "Harus di isi !!",
                type: "warning",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });

            return false;
        } else {
            $.ajax({
                url: site_url + "amc/simpan",
                type: 'POST',
                dataType: 'json',
                data: {
                    master: dt_pend,
                    detail: detailObj,
                    idpelanggan: arrDataPel,
                    serial: arrDataSer,
                    jumlah: arrDatajum,
                    harga: arrDataharga,
                    namabarang: arrDataNam,
                    total: arrDataTot
                },
                success: function(data, status) {
                    // toastr[data.alert](data.data, data.title);

                    if (status == 'success') {
                        swal({
                            title: "Success!",
                            text: "Berhasil Di Simpan.",
                            type: "success",
                            showCancelButton: false,
                            showConfirmButton: false,
                            timer: 3000
                        }, function() {
                            swal.close();
                            location.reload();
                        });


                    }

                }
            })
        }

    });

    $('body').on('click', '.detailbarang', function(e) {
        e.preventDefault();

        var id = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/get_barang",
            data: { id: id },
            dataType: "JSON",
            success: function(data) {

                if ($.fn.DataTable.isDataTable('#detail_rpt01')) {
                    $('#detail_rpt01').DataTable().destroy();
                }
                $('#detail_rpt01').DataTable(data);
                $('#modal_form_layanan01').modal('show');

            }
        });
    });

    // Absen

    $('.content-wrapper').on('click', '.simpan_absenkaryawan', function(e) {
        e.preventDefault();
        var karyawan = $('#idkaryawan').val();
        var tanggal = $('#datepicker3').val();
        var absen = $('#absen').val();
        var jam = $('#jam').val();
        if (karyawan == '' || tanggal == '' || absen == '' || jam == '') {
            swal({
                title: "Error!",
                text: "Harus Disi Semua!!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/insertabsenkaryawan",
                data: { karyawan: karyawan, tanggal: tanggal, absen: absen, jam: jam },
                dataType: "JSON",
                success: function(data) {
                    // console.log(data);
                    toastr[data.alert](data.data, data.title);
                    tbabsen.ajax.reload(null, false);
                    $('#modal-absenkaryawan').modal('hide');
                    $('#idkaryawan').val(null).trigger('change');
                    $('#datepicker3').val('');
                    $('#absen').val(null).trigger('change');
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.editabsenkaryawan', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/get_editabsenkaryawan",
            data: { id: id },
            dataType: "JSON",
            success: function(data) {
                for (i in data) {
                    $('[name="ideditkaryawan"]').val(data[i].id);
                    $('[name="editnama"]').val(data[i].namakaryawan);
                    $('[name="edittanggal"]').val(data[i].tgl);
                    $('[name="timepicker1"]').val(data[i].jammasuk);
                    $('[name="timepicker2"]').val(data[i].jamkeluar);
                    $('#modal-edit-absenkaryawan').modal('show');
                }
            }
        })
    });

    $('.content-wrapper').on('click', '.simpan_absenkaryawan_edit', function(e) {
        e.preventDefault();
        var idkaryawan = $('#ideditkaryawan').val();
        var tanggal = $('#datepicker').val();
        var jammasuk = $('#jammasuk').val();
        var jamkeluar = $('#jamkeluar').val();

        $.ajax({
            type: 'POST',
            url: site_url + "amc/updateabsenkaryawan",
            data: { idkaryawan: idkaryawan, tanggal: tanggal, jammasuk: jammasuk, jamkeluar: jamkeluar },
            dataType: "JSON",
            success: function(data) {
                tbabsen.ajax.reload(null, false);
                toastr[data.alert](data.data, data.title);
                $('#modal-edit-absenkaryawan').modal('hide');
            }
        })
    });

    $('.content-wrapper').on('click', '.hapusabsenkaryawan', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        swal({
                title: 'Delete Data',
                text: 'Yakin Ingin Menghapus Data ?',
                html: true,
                confirmButtonColor: '#d9534f',
                showCancelButton: true,
            },
            function() {
                $.ajax({
                    type: 'POST',
                    url: site_url + "amc/deleteabsenkaryawan",
                    data: { id: id },
                    dataType: "JSON",
                    success: function(data) {
                        console.log(data);
                        tbabsen.ajax.reload(null, false);
                        toastr[data.alert](data.data, data.title);
                    }
                });
            });
    });


    // Chart

    $.ajax({
        url: site_url + "amc/chart",
        method: "POST",
        dataType: "JSON",
        success: function(data) {
            var label = [];
            var tagihan = [];
            var pembayaran = [];
            var kurangbayar = [];
            var d = new Date();
            var n = d.getFullYear();
            for (var i in data) {
                label.push(data[i].nmbulan);
                tagihan.push(Number(data[i].tagihan));
                pembayaran.push(Number(data[i].pembayaran));
                kurangbayar.push(Number(data[i].kurangbayar));
            }

            Highcharts.chart('container', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Data Tagihan, Pembayaran dan Kurang Bayar Tahun ' + n
                },
                credits: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                xAxis: {
                    categories: label,
                    crosshair: true
                },
                yAxis: {
                    min: 0
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>Rp. {point.y} </b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Tagihan',
                    data: tagihan

                }, {
                    name: 'Pembayaran',
                    data: pembayaran

                }, {
                    name: 'Kurang Bayar',
                    data: kurangbayar

                }]
            });
        }
    });

    $.ajax({
        url: site_url + "amc/chartbarang",
        method: "POST",
        dataType: "JSON",
        success: function(data) {
            var label = [];
            var pembelian = [];
            var invoice = [];
            var d = new Date();
            var n = d.getFullYear();
            for (var i in data) {
                pembelian.push(Number(data[i].sumhargabarang));
                invoice.push(Number(data[i].sumpendapatan));
            }

            Highcharts.chart('container2', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: 0,
                    plotShadow: false,
                    height: 250,
                },
                title: {
                    text: 'Barang & Invoice',
                    align: 'center',
                    verticalAlign: 'middle',
                    y: 60
                },
                credits: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b> Rp. {point.y} </b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                // accessibility: {
                // 	point: {
                // 		valueSuffix: '%'
                // 	}
                // },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: true,
                            distance: -25,
                            style: {
                                fontWeight: 'bold',
                                color: 'black'
                            }
                        },
                        startAngle: -100,
                        endAngle: 100,
                        size: '100%'
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Jumlah',
                    innerSize: '50%',
                    data: [
                        ['Barang', pembelian[0]],
                        ['Invoice', invoice[0]],
                    ]
                }]
            });
        }
    });


});

$('#lappdftagihan').click(function() {
    $('#ipladder').attr('target', '_blank');
    $('#ipladder').attr('action', site_url + "amc/pdftagihanlaporan");
    $('#ipladder').submit();
});

$('#lappdfbelumbayar').click(function() {
    $('#ipladder1').attr('target', '_blank');
    $('#ipladder1').attr('action', site_url + "amc/pdfblmbayar");
    $('#ipladder1').submit();
});

$('#lappdfpembayaran').click(function() {
    $('#bayar').attr('target', '_blank');
    $('#bayar').attr('action', site_url + "amc/pdfbayarlaporan");
    $('#bayar').submit();
});

$('#cetakpelanggan').click(function() {
    $('#ipladder2').attr('target', '_blank');
    $('#ipladder2').attr('action', site_url + "amc/pdfpelanggan");
    $('#ipladder2').submit();
});

$('#lapgajian').click(function() {
    $('#formgajian').attr('target', '_blank');
    $('#formgajian').attr('action', site_url + "amc/pdfgajianglobal");
    $('#formgajian').submit();
});
$('#laptag').click(function() {
    $('#formtag').attr('target', '_blank');
    $('#formtag').attr('action', site_url + "amc/pdftag");
    $('#formtag').submit();
});
$('#lapabsen').click(function() {
    $('#formabsen').attr('target', '_blank');
    $('#formabsen').attr('action', site_url + "amc/pdfabsen");
    $('#formabsen').submit();
});
$('#lappempel').click(function() {
    $('#formpempel').attr('target', '_blank');
    $('#formpempel').attr('action', site_url + "amc/pdfpempel");
    $('#formpempel').submit();
});
