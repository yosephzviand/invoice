jQuery(document).ready(function ($) {
    var site_url = $('body').data('site_url');
    console.log(site_url);
    $("#simpan_pnt").attr('disabled', 'disabled');
    $("#simpan_pnt").removeClass('btn-success');
    //$("#simpan_pnt").addClass('btn-outline-success')
    $("#nopel").focus();

    $('#datepicker').datepicker({
        autoclose: true,
        format: "dd-mm-yyyy",
    })

    //var nopel='';
    //nopel= 
    $('#getPelayanan').on('click', function (e) {
        e.preventDefault();
        //var id = $(this).find(':selected')[0].id;
        var id = $("#nopel").val();
        //alert(id);
        $.ajax({
            type: 'POST',
            url: site_url + "penetapan/get_pelayanan/",
            data: {
                'nopen': id
            },
            dataType: "JSON",
            success: function (data) {
                // the next thing you want to do
                console.log(data);
                var konten = $('.postx');
                konten.empty();
                data = JSON.parse(JSON.stringify(data).replace(/\:null/gi, "\:\"\""));

                // for (var i = 0; i < data.length && sk.length; i++) {
                for (i in data.data) {
                    konten.append('<div class="form-group">' +
                        '<div class="row">' +
                        '<div class="col-8"><label>' + data.data[i].NM_JENIS_PELAYANAN + ' a.n ' + data.data[i].NAMA_TERMOHON + '</label></div>' +
                        '<div class="col-2">' + data.sk[i].tombol + '</div>' +
                        '<div class="col-2">' +
                        '<input type="checkbox" class="float-right status dt_status" name="status" ' +
                        'data-nopen="' + data.data[i].NOPEN_DETAIL +
                        '" data-toggle="toggle" data-on="Terima" data-off="Tolak" data-onstyle="success" data-offstyle="danger" data-size="small" data-width="110">' +
                        '</div></div><div class="row"><div class="input-group"><div class="input-group-prepend">' +
                        '<span class="input-group-text"><i class="fas fa-comment"></i></span></div>' +
                        '<input class="form-control ket_detail" name="ket_detail"  placeholder="Catatan" type="text" value="' + data.data[i].KET_DETAIL + '">' +
                        '</div></div></div>');
                    $('.hidden').hide();
                    if (parseInt(data.data[i].STATUS_PERMOHONAN) == -3) {
                        $('#' + data.data[i].NOPEN_DETAIL).bootstrapToggle('off');
                    }
                    else if (parseInt(data.data[i].STATUS_PERMOHONAN) == 3) {
                        $('#' + data.data[i].NOPEN_DETAIL).bootstrapToggle('on');
                    }
                    else {
                        $('#' + data.data[i].NOPEN_DETAIL).bootstrapToggle('off');

                    }
                }
                //manually trigger a change event for the contry so that the change handler will get triggered
                // console.log( konten );
                $(".dt_status").bootstrapToggle();
                $("#simpan_pnt").removeAttr('disabled');
                $("#simpan_pnt").removeClass('btn-outline-success');
                $("#simpan_pnt").addClass('btn-success')
            }


        });

    });

    /* validation*/
    $(".nik").on('input', function () {
        var value = $(this).val().replace(/[^0-9]*/g, '');
        $(this).val(value)

    });


    $(".numerical").on('input', function () {
        var value = $(this).val().replace(/[^0-9]*/g, '');
        value = value.replace(/\.{2,}/g, '.');
        value = value.replace(/\.,/g, ',');
        value = value.replace(/\,\./g, ',');
        value = value.replace(/\,{2,}/g, ',');
        value = value.replace(/\.[0-9]+\./g, '.');
        $(this).val(value)

    });

    $(".no_telp").on('input', function () {
        var value = $(this).val().replace(/[^0-9.,+,-]*/g, '');
        $(this).val(value)

    });

    $(".alow_text").on('input', function () {
        var value = $(this).val().replace(/[^0-9.,-,a-z,A-Z,']*/g, '');
        $(this).val(value)

    });


    /* simpan Hasil Verifikasi Pendaftaran*/

    $('body').on('click', '#simpan_pnt', function (event) {
        //cetak_url =  $(this).data('cetak');
        proses_url = $(this).data('verifikasi');
        // catatan = $('#catatan').val();
        no_pend = $('#nopel').val();
        //var no_hp = $('.no_hp').val();


        var arrData = [];
        // loop over each table row (tr)
        $("form .postx").each(function () {
            console.log($(this).find('.status').data('nopen'));
            var obj = {};
            obj.no_pend_detail = $(this).find('.status').data('nopen');
            // obj.no_urut = $(this).find('.no_urut').val();
            if ($(this).find('.status:checked').val() == 'on') {
                obj.status_permohonan = '3';
            }
            else {
                obj.status_permohonan = '-3';
            }
            // obj.status_permohonan = $(this).find('.status:checked').val();
            obj.ket_detail = $(this).find('.ket_detail').val();
            //obj.no_hp = $(this).find('.no_hp').val();
            //obj.nama        = nama;

            arrData.push(obj);
        });

        console.log(arrData);
        //console.log(no_pend);
        //console.log(catatan);
        $.ajax({
            url: proses_url,
            type: 'POST',
            dataType: 'json',
            data: {
                // master: catatan,
                detail: arrData,
                no_pend: no_pend

            },
        })
            .done(function (data) {

                //console.log(data.alert);
                toastr[data.alert](data.data, data.title);
                $("#simpan_pnt").attr('disabled', 'disabled');
                $('.postx').empty();
                $("#simpan_pnt").removeClass('btn-success');
                $("#simpan_pnt").addClass('btn-outline-success')
                $("#nopel").val('');
                $("#nopel").focus();
                pnt_layanan.ajax.reload(null, false);
            })
            .fail(function () {
                console.log("error");
            })
            .always(function () {
                console.log("complete");
            });


        // window.open(cetak_url+'/'+no_pend, '_blank');
        //window.location.href = site_url + '/pendataan/index';
        event.preventDefault();

    });

    pnt_layanan = $('#tb_pnt_pelayanan_new').DataTable({

        // "scrollX": true,
        // "processing": true, 
        // "serverSide": true, 
        // "order": [],
        "searching": false,
        "ordering": false,
        "info": false,
        "lengthChange": true,
        "pageLength": 10,
        // Load data for the table's content from an Ajax source
        "ajax": {
            //"url": "<?php echo site_url('tahun_ajaran/ajax_list')?>",
            "url": site_url + "penetapan/ajax_list_pnt_new",
            "type": "POST"
        },

        //Set column definition initialisation properties.

    });




    $('#tb_pnt_pelayanan_new_length').css({ opacity: 0 });

    $('body').on('click', '.cetakpdf', function (e) {
        date = new Date();
        thn = date.getFullYear();
        var kode = $(this).data('kode');
        var nopendetail = $(this).data('nopen-detail');
        $.ajax({
            url: site_url + 'penetapan/cekpath/',
            type: 'POST',
            dataType: 'json',
            data: {
                kode: kode,
                nopendetail: nopendetail
            },
            success: function (data) {
                if (data == 'Data Tidak Ada') {
                    toastr['warning'](data, 'Peringatan');
                } else {
                    window.open(data, '_blank');
                }
            }
        })
    });

    $('#datepicker').change(function () {
        var id = $('#datepicker').val();
        $('#process').css('display', 'block');
        $.ajax({
            url: site_url + "penetapan/get_rpt_penetapan/",
            method: "POST",
            data: {
                'tanggal': id
            },
            async: true,
            dataType: 'json',
            success: function (data) {
                $('#process').css('display', 'none');
                if ($.fn.DataTable.isDataTable('#rpt_penetapan')) {
                    $('#rpt_penetapan').DataTable().destroy();
                }

                $('#rpt_penetapan').DataTable(data);
            }
        });
        return false;
    });

    $('.content-wrapper').on('click', '.detail', function (e) {
        e.preventDefault();
        // var tgl = $(this).data('tgl');
        var tgl = $('#datepicker').val();
        var kode = $(this).data('kode');
        $('#process').css('display', 'block');
        $.ajax({
          type: 'POST',
          url: site_url + "penetapan/get_detail_rpt_penetapan",
          data: { tgl: tgl, kode: kode },
          dataType: "JSON",
          success: function (data) {
            // console.log(data.kode);
            switch (data.kode) {
              case '01':
                $('#process').css('display', 'none');
                if ($.fn.DataTable.isDataTable('#detail_rpt01')) {
                  $('#detail_rpt01').DataTable().destroy();
                }
                $('#detail_rpt01').DataTable(data);
                $('#modal_form_layanan01').modal('show');
                break;
              case '04':
                $('#process').css('display', 'none');
                if ($.fn.DataTable.isDataTable('#detail_rpt04')) {
                  $('#detail_rpt04').DataTable().destroy();
                }
                $('#detail_rpt04').DataTable(data);
                $('#modal_form_layanan04').modal('show');
                break;
              case '05':
                $('#process').css('display', 'none');
                if ($.fn.DataTable.isDataTable('#detail_rpt05')) {
                  $('#detail_rpt05').DataTable().destroy();
                }
                $('#detail_rpt05').DataTable(data);
                $('#modal_form_layanan05').modal('show');
                break;
              case '08':
                $('#process').css('display', 'none');
                if ($.fn.DataTable.isDataTable('#detail_rpt08')) {
                  $('#detail_rpt08').DataTable().destroy();
                }
                $('#detail_rpt08').DataTable(data);
                $('#modal_form_layanan08').modal('show');
                break;
              case '10':
                $('#process').css('display', 'none');
                if ($.fn.DataTable.isDataTable('#detail_rpt10')) {
                  $('#detail_rpt10').DataTable().destroy();
                }
                $('#detail_rpt10').DataTable(data);
                $('#modal_form_layanan10').modal('show');
                break;
              default:
                $('#process').css('display', 'none');
                if ($.fn.DataTable.isDataTable('#detail_rpt02')) {
                  $('#detail_rpt02').DataTable().destroy();
                }
                $('#detail_rpt02').DataTable(data);
                $('#modal_form_layanan02').modal('show');
                break;
            }
          }
        });
      });

    // $('#pdflaporan').click(function () {
    //     $('#ipladder').attr('target', '_blank');
    //     $('#ipladder').attr('action', site_url + 'penetapan/laporanpdf');
    //     $('#ipladder').submit();
    // });

})

Number.prototype.pad = function (size) {
    var s = String(this);
    while (s.length < (size || 2)) { s = "0" + s; }
    return s;
}

