function initialise() {
  //Datemask dd/mm/yyyy
  $('.content-wrapper >.datemask').inputmask('dd/mm/yyyy', {
    'placeholder': 'dd/mm/yyyy'
  });

  $('.monthmask').inputmask('mm/yyyy');

  /* $('.thousand').maskNumber({
     integer: true,
   });
 */



};


jQuery(document).ready(function ($) {
  initialise();
  var site_url = $('body').data('site_url');
  console.log(site_url);
  $("#simpan_pdt").attr('disabled', 'disabled');
  $("#simpan_pdt").removeClass('btn-success');
  //$("#simpan_pdt").addClass('btn-outline-success')
  $("#nopel").focus();

  $('#datepicker').datepicker({
    autoclose: true,
    format: "dd-mm-yyyy",
  })


  //var nopel='';
  //nopel= 
  $('#getPelayanan').on('click', function (e) {
    e.preventDefault();
    //var id = $(this).find(':selected')[0].id;
    var id = $("#nopel").val();
    //alert(id);
    $.ajax({
      type: 'POST',
      url: site_url + "pendataan/get_pelayanan/",
      data: {
        'nopen': id
      },
      dataType: "JSON",
      success: function (data) {
        // the next thing you want to do
        console.log(data);
        var konten = $('.postx');
        konten.empty();

        data = JSON.parse(JSON.stringify(data).replace(/\:null/gi, "\:\"\""));
        for (var i = 0; i < data.length; i++) {

          var status;

          konten.append(
            '<div class="post"><div class="form-group">' +
            '<div class="row">' +
            '<div class="col-8"><label><span>' + data[i].NM_JENIS_PELAYANAN + '</span>' +
            ' a.n ' + data[i].NAMA_TERMOHON + '</label></div>' +
            '<div class="col-2"><a href="#" class="btn btn-sm btn-info float-right bt_detail_permohonan" data-toggle="modal" data-nopen-detail="' + data[i].NOPEN_DETAIL + '" data-kode="' + data[i].KODE + '" data-target="#modal-pendataan"><i class="fa fa-search"></i> Detail</a></div><div class="col-2">' +
            '<input id ="' + data[i].NOPEN_DETAIL + '" type="checkbox" class="float-right status dt_status" name="status" ' +
            'data-nopen="' + data[i].NOPEN_DETAIL +
            '" data-toggle="toggle" data-on="Terima" data-off="Tolak" data-onstyle="success" data-offstyle="danger" data-size="small" data-width="110">' +
            '</div></div><div class="row"><div class="input-group"><div class="input-group-prepend">' +
            '<span class="input-group-text"><i class="fas fa-comment"></i></span></div>' +
            '<input class="form-control ket_detail" name="ket_detail"  placeholder="Catatan" type="text" value="' + data[i].KET_DETAIL + '">' +
            '<input class="form-control kode" name="kode"  placeholder="" type="hidden" value="' + data[i].KODE + '">' +
            '</div></div></div></div>');
          if (parseInt(data[i].STATUS_PERMOHONAN) == -2) {
            $('#' + data[i].NOPEN_DETAIL).bootstrapToggle('off');
          }
          else if (parseInt(data[i].STATUS_PERMOHONAN) == 2) {
            $('#' + data[i].NOPEN_DETAIL).bootstrapToggle('on');
          }
          else {
            $('#' + data[i].NOPEN_DETAIL).bootstrapToggle('off');

          }
        }

        //manually trigger a change event for the contry so that the change handler will get triggered
        // console.log( konten );
        $(".dt_status").bootstrapToggle();
        $("#simpan_pdt").removeAttr('disabled');
        $("#simpan_pdt").removeClass('btn-outline-success');
        $("#simpan_pdt").addClass('btn-success')
      }
    });
  });

  $('body').on('click', '.bt_detail_permohonan', function (e) {
    //console.log('');

    // var id = $(this).find(':selected')[0].id;
    var kode = $(this).data('kode');
    var nopendetail = $(this).data('nopen-detail');
    var jns_layanan = $(this).closest('.row').find('label>span').text();

    //alert(id);
    $.ajax({
      type: 'POST',
      url: site_url + "pendataan/get_permohonan" + kode + "/",
      data: {
        'id': nopendetail
      },
      //  dataType: "JSON",

      success: function (html) {
        $(".modal-append").html("");
        $(".modal-append").append(html);
        $(".modal-subtitle").html(jns_layanan);
        $(".modal-append input").prop('readonly', true);
        $("#acc_pct_pengurangan").removeAttr('readonly');
        $("#no_lhp").removeAttr('readonly');
        $("#tgl_lhp").removeAttr('readonly');
        $('.datemask').inputmask('dd/mm/yyyy', {
          'placeholder': 'dd/mm/yyyy'
        });
        $("#tabs_bng_ke").tab();

      }

    });

    e.preventDefault();
  });

  $('body').on('click', '#terima_vrf', function (e) {
    /*20201025-026*/
    var id_detail = $('#nopen_detail').val();
    var kode = $('#kodepel').val();
    var pct = $('#acc_pct_pengurangan').val();
    var arrKet = $('#modal-pendataan').find('.persentase').serializeArray();
    console.log(arrKet);
    switch (kode) {
      case '10':
        $.ajax({
          type: 'POST',
          url: site_url + "pendataan/update_permohonan/",
          data: {
            'id': id_detail,
            'kode': kode,
            'no_lhp': $('#no_lhp').val(),
            'tgl_lhp': $('#tgl_lhp').val(),
            'detail': arrKet
          },
          dataType: "JSON",

          success: function (html) {
            console.log('update sukses');

          }

        });
        break;
      case '08':
        $.ajax({
          type: 'POST',
          url: site_url + "pendataan/update_permohonan/",
          data: {
            'id': id_detail,
            'acc': pct,
            'kode': kode,
            'no_lhp': $('#no_lhp').val(),
            'tgl_lhp': $('#tgl_lhp').val()
          },
          //  dataType: "JSON",

          success: function (html) {
            console.log('update sukses');

          }

        });
        break;
      case '04':
        $.ajax({
          type: 'POST',
          url: site_url + "pendataan/update_permohonan/",
          data: {
            'id': id_detail,
            'kode': kode,
            'no_lhp': $('#no_lhp').val(),
            'tgl_lhp': $('#tgl_lhp').val()
          },
          //  dataType: "JSON",

          success: function (html) {
            console.log('update sukses');

          }

        });
        break;
      case '15':
        $.ajax({
          type: 'POST',
          url: site_url + "pendataan/update_permohonan/",
          data: {
            'id': id_detail,
            'kode': kode,
            'no_lhp': $('#no_lhp').val(),
            'tgl_lhp': $('#tgl_lhp').val()
          },
          //  dataType: "JSON",

          success: function (html) {
            console.log('update sukses');

          }

        });


        break;
      default:
      // code to be executed if n is different from case 1 and 2
    }

    console.log(id_detail);
    $('#' + id_detail).bootstrapToggle('on');
    $(".modal-append").html("");
    $('.modal').modal('hide');






    // e.preventDefault();
  });

  $('body').on('click', '#tolak_vrf', function (e) {
    /*20201025-026*/
    var kode = $('#kodepel').val();
    var id_detail = $('#nopen_detail').val();
    switch (kode) {
      case '10':
        $.ajax({
          type: 'POST',
          url: site_url + "pendataan/update_permohonan/",
          data: {
            'id': id_detail,
            'kode': kode,
            'no_lhp': $('#no_lhp').val(),
            'tgl_lhp': $('#tgl_lhp').val()
          },
          //  dataType: "JSON",

          success: function (html) {
            console.log('update sukses');

          }

        });
        break;
      case '08':
        $.ajax({
          type: 'POST',
          url: site_url + "pendataan/update_permohonan/",
          data: {
            'id': id_detail,
            'kode': kode,
            'no_lhp': $('#no_lhp').val(),
            'tgl_lhp': $('#tgl_lhp').val()
          },
          //  dataType: "JSON",

          success: function (html) {
            console.log('update sukses');

          }

        });
        break;
      case '04':
        $.ajax({
          type: 'POST',
          url: site_url + "pendataan/update_permohonan/",
          data: {
            'id': id_detail,
            'kode': kode,
            'no_lhp': $('#no_lhp').val(),
            'tgl_lhp': $('#tgl_lhp').val()
          },
          //  dataType: "JSON",

          success: function (html) {
            console.log('update sukses');

          }

        });
        break;
      case '15':
        $.ajax({
          type: 'POST',
          url: site_url + "pendataan/update_permohonan/",
          data: {
            'id': id_detail,
            'kode': kode,
            'no_lhp': $('#no_lhp').val(),
            'tgl_lhp': $('#tgl_lhp').val()
          },
          //  dataType: "JSON",

          success: function (html) {
            console.log('update sukses');

          }

        });
        break;
      default:
      // code to be executed if n is different from case 1 and 2
    }
    console.log(id_detail);
    $('#' + id_detail).bootstrapToggle('off');
    $(".modal-append").html("");
    $('.modal').modal('hide');




    e.preventDefault();
  });








  /* validation*/
  $(".nik").on('input', function () {
    var value = $(this).val().replace(/[^0-9]*/g, '');
    $(this).val(value)

  });


  $(".numerical").on('input', function () {
    var value = $(this).val().replace(/[^0-9]*/g, '');
    value = value.replace(/\.{2,}/g, '.');
    value = value.replace(/\.,/g, ',');
    value = value.replace(/\,\./g, ',');
    value = value.replace(/\,{2,}/g, ',');
    value = value.replace(/\.[0-9]+\./g, '.');
    $(this).val(value)

  });

  $(".no_telp").on('input', function () {
    var value = $(this).val().replace(/[^0-9.,+,-]*/g, '');
    $(this).val(value)

  });

  $(".alow_text").on('input', function () {
    var value = $(this).val().replace(/[^0-9.,-,a-z,A-Z,']*/g, '');
    $(this).val(value)

  });

  /* simpan Hasil Verifikasi Pendaftaran*/

  $('body').on('click', '#simpan_pdt', function (event) {
    //cetak_url =  $(this).data('cetak');
    proses_url = $(this).data('verifikasi');
    // catatan = $('#catatan').val();
    no_pend = $('#nopel').val();
    //var no_hp = $('.no_hp').val();


    var arrData = [];
    // loop over each table row (tr)
    $("form .post").each(function () {
      console.log($(this).find('.status').data('nopen'));
      var obj = {};
      obj.no_pend_detail = $(this).find('.status').data('nopen');
      // obj.no_urut = $(this).find('.no_urut').val();
      if ($(this).find('.status:checked').val() == 'on') {
        obj.status_permohonan = '2';
      }
      else {
        obj.status_permohonan = '-2';
      }
      // obj.status_permohonan = $(this).find('.status:checked').val();
      obj.ket_detail = $(this).find('.ket_detail').val();
      obj.kode = $(this).find('.kode').val();
      //obj.nama        = nama;

      arrData.push(obj);
    });

    console.log(arrData);
    //console.log(no_pend);
    //console.log(catatan);
    $.ajax({
      url: proses_url,
      type: 'POST',
      dataType: 'json',
      data: {
        // master: catatan,
        detail: arrData,
        no_pend: no_pend

      },
    })
      .done(function (data) {
        // alert('Success' + data);
        // console.log(data);
        if (data.generate != '') {
          toastr[data.generate.alert](data.generate.data, data.generate.title);
        }
        toastr[data.sukses.alert](data.sukses.data, data.sukses.title);

        $("#simpan_pdt").attr('disabled', 'disabled');
        $('.postx').empty();
        $("#simpan_pdt").removeClass('btn-success');
        $("#simpan_pdt").addClass('btn-outline-success')
        $("#nopel").val('');
        $("#nopel").focus();
        pdt_layanan.ajax.reload(null, false);
      })
      .fail(function () {
        console.log("error");
      })
      .always(function () {
        console.log("complete");
      });


    // window.open(cetak_url+'/'+no_pend, '_blank');
    //window.location.href = site_url + '/pendataan/index';
    event.preventDefault();

  });

  pdt_layanan = $('#tb_pdt_pelayanan_new').DataTable({

    // "scrollX": true,
    // "processing": true, 
    // "serverSide": true, 
    // "order": [],
    "searching": false,
    "ordering": false,
    "info": false,
    "lengthChange": true,
    "pageLength": 10,
    // Load data for the table's content from an Ajax source
    "ajax": {
      //"url": "<?php echo site_url('tahun_ajaran/ajax_list')?>",
      "url": site_url + "pendataan/ajax_list_pdt_new",
      "type": "POST"
    },

    //Set column definition initialisation properties.

  });
  $('#tb_pdt_pelayanan_new_length').css({ opacity: 0 });

  $('#datepicker').change(function () {
    var id = $('#datepicker').val();
    $('#process').css('display', 'block');
    $.ajax({
      url: site_url + "pendataan/get_rpt_pendataan/",
      method: "POST",
      data: {
        'tanggal': id
      },
      async: true,
      dataType: 'json',
      success: function (data) {
        // console.log(data);
        $('#process').css('display', 'none');
        if ($.fn.DataTable.isDataTable('#rpt_pendataan')) {
          $('#rpt_pendataan').DataTable().destroy();
        }

        $('#rpt_pendataan').DataTable(data);
      }
    });
    return false;
  });

  $('.content-wrapper').on('click', '.detail', function (e) {
    e.preventDefault();
    // var tgl = $(this).data('tgl');
    var tgl = $('#datepicker').val();
    var kode = $(this).data('kode');
    $('#process').css('display', 'block');
    $.ajax({
      type: 'POST',
      url: site_url + "pendataan/get_detail_rpt_pendataan",
      data: { tgl: tgl, kode: kode },
      dataType: "JSON",
      success: function (data) {
        // console.log(data.kode);
        switch (data.kode) {
          case '01':
            $('#process').css('display', 'none');
            if ($.fn.DataTable.isDataTable('#detail_rpt01')) {
              $('#detail_rpt01').DataTable().destroy();
            }
            $('#detail_rpt01').DataTable(data);
            $('#modal_form_layanan01').modal('show');
            break;
          case '04':
            $('#process').css('display', 'none');
            if ($.fn.DataTable.isDataTable('#detail_rpt04')) {
              $('#detail_rpt04').DataTable().destroy();
            }
            $('#detail_rpt04').DataTable(data);
            $('#modal_form_layanan04').modal('show');
            break;
          case '05':
            $('#process').css('display', 'none');
            if ($.fn.DataTable.isDataTable('#detail_rpt05')) {
              $('#detail_rpt05').DataTable().destroy();
            }
            $('#detail_rpt05').DataTable(data);
            $('#modal_form_layanan05').modal('show');
            break;
          case '08':
            $('#process').css('display', 'none');
            if ($.fn.DataTable.isDataTable('#detail_rpt08')) {
              $('#detail_rpt08').DataTable().destroy();
            }
            $('#detail_rpt08').DataTable(data);
            $('#modal_form_layanan08').modal('show');
            break;
          case '10':
            $('#process').css('display', 'none');
            if ($.fn.DataTable.isDataTable('#detail_rpt10')) {
              $('#detail_rpt10').DataTable().destroy();
            }
            $('#detail_rpt10').DataTable(data);
            $('#modal_form_layanan10').modal('show');
            break;
          default:
            $('#process').css('display', 'none');
            if ($.fn.DataTable.isDataTable('#detail_rpt02')) {
              $('#detail_rpt02').DataTable().destroy();
            }
            $('#detail_rpt02').DataTable(data);
            $('#modal_form_layanan02').modal('show');
            break;
        }
      }
    });
  });

})



Number.prototype.pad = function (size) {
  var s = String(this);
  while (s.length < (size || 2)) { s = "0" + s; }
  return s;
}

