jQuery(document).ready(function ($) {
    $.ajax({
        url: site_url + "dhkp/chart",
        method: "GET",
        dataType: "JSON",
        success: function (data) {
            var label = [];
            var pelayanan = [];

            for (var i in data) {
                label.push(data[i].PROGRES);
                pelayanan.push(Number(data[i].JUMLAH));
            }
            var d = new Date();
            var n = d.getFullYear();
            charttahun = new Highcharts.chart('charttahun', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                credits: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                title: {
                    text: 'Progres Tahun ' + n
                },
                // subtitle: {
                //     text: '<b>Total Target : ' + rupiah + '</b>'
                // },
                tooltip: {
                    pointFormat: '{series.name}: <b> {point.y}</b> <br/> <br> Persentase : {point.percentage:.1f} %</br>'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                        }
                    }
                },
                series: [{
                    name: 'Jumlah',
                    colorByPoint: true,
                    data: [{
                        name: label[0],
                        y: pelayanan[0],
                        sliced: true,
                        selected: true
                    }, {
                        name: label[1],
                        y: pelayanan[1]
                    }, {
                        name: label[2],
                        y: pelayanan[2]
                    }, {
                        name: label[3],
                        y: pelayanan[3]
                    }]
                }]
            });

        }
    });

    $.ajax({
        url: site_url + "dhkp/chartbulan",
        method: "GET",
        dataType: "JSON",
        success: function (data) {
            var label = [];
            var pelayanan = [];

            for (var i in data) {
                label.push(data[i].PROGRES);
                pelayanan.push(Number(data[i].JUMLAH));
            }

            var month = new Array();
            month[0] = "Januari";
            month[1] = "Februari";
            month[2] = "Maret";
            month[3] = "April";
            month[4] = "Mei";
            month[5] = "Juni";
            month[6] = "Juli";
            month[7] = "Agustus";
            month[8] = "September";
            month[9] = "Oktober";
            month[10] = "November";
            month[11] = "Desember";

            var d = new Date();
            var n = month[d.getMonth()];
            chartbulan = new Highcharts.chart('chartbulan', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                credits: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                title: {
                    text: 'Progres Bulan ' + n
                },
                // subtitle: {
                //     text: '<b>Total Target : ' + rupiah + '</b>'
                // },
                tooltip: {
                    pointFormat: '{series.name}: <b> {point.y}</b> <br/> <br> Persentase : {point.percentage:.1f} %</br>'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                        }
                    }
                },
                series: [{
                    name: 'Jumlah',
                    colorByPoint: true,
                    data: [{
                        name: label[0],
                        y: pelayanan[0],
                        sliced: true,
                        selected: true
                    }, {
                        name: label[1],
                        y: pelayanan[1]
                    }, {
                        name: label[2],
                        y: pelayanan[2]
                    }, {
                        name: label[3],
                        y: pelayanan[3]
                    }]
                }]
            });

        }
    });

    $.ajax({
        url: site_url + "dhkp/chartbulanlalu",
        method: "GET",
        dataType: "JSON",
        success: function (data) {
            var label = [];
            var pelayanan = [];

            var month = new Array();
            month[0] = "Januari";
            month[1] = "Februari";
            month[2] = "Maret";
            month[3] = "April";
            month[4] = "Mei";
            month[5] = "Juni";
            month[6] = "Juli";
            month[7] = "Agustus";
            month[8] = "September";
            month[9] = "Oktober";
            month[10] = "November";
            month[11] = "Desember";

            var d = new Date();
            var n = month[d.getMonth() - 1];
            for (var i in data) {
                label.push(data[i].PROGRES);
                pelayanan.push(Number(data[i].JUMLAH));
            }
            chartbulanlalu = new Highcharts.chart('chartbulanlalu', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                credits: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                title: {
                    text: 'Progres Bulan ' + n
                },
                tooltip: {
                    pointFormat: '{series.name}: <b> {point.y}</b> <br/> <br> Persentase : {point.percentage:.1f} %</br>'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                        }
                    }
                },
                series: [{
                    name: 'Jumlah',
                    colorByPoint: true,
                    data: [{
                        name: label[0],
                        y: pelayanan[0],
                        sliced: true,
                        selected: true
                    }, {
                        name: label[1],
                        y: pelayanan[1]
                    }, {
                        name: label[2],
                        y: pelayanan[2]
                    }, {
                        name: label[3],
                        y: pelayanan[3]
                    }]
                }]
            });

        }
    });


})