-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 13, 2021 at 08:36 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `masadi`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `Billing` () RETURNS INT(11) BEGIN
	declare billing char (9);
	DECLARE Y CHAR(2);
	DECLARE M CHAR(2);
	DECLARE URUT INT;
	
	SET M = (SELECT DATE_FORMAT(CURDATE(),'%m'));
	SET Y = (SELECT DATE_FORMAT(CURDATE(),'%yy'));
	SET URUT = (SELECT count(idbilling) from transaksi WHERE MONTH(tglpenetapan)=M);
	
	set billing = CONCAT(Y,M,LPAD(urut+1,4,0));


	RETURN billing;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `NOMERSURAT` () RETURNS CHAR(16) CHARSET utf8mb4 BEGIN
	
	DECLARE NS CHAR(16);
	DECLARE M CHAR(2);
	DECLARE Y CHAR(4);
	DECLARE ROMAWI CHAR(2);
	DECLARE URUT INT;
	
	
	SET M = (SELECT DATE_FORMAT(CURDATE(),'%m'));
	SET Y = (SELECT DATE_FORMAT(CURDATE(),'%Y'));
	SET URUT = (SELECT count(nosurat) from transaksi WHERE MONTH(tglpenetapan)=M);
	SET ROMAWI = CASE 
	WHEN M = 01 THEN
		'I'
	ELSE
		'II'
END;


	set NS = CONCAT(LPAD(urut+1,4,0),'/AMC/',ROMAWI,'/',Y);

	RETURN NS;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `terbilang` (`v_angka` BIGINT) RETURNS VARCHAR(5000) CHARSET utf8mb4 Begin
    Declare sString varchar(30);
    Declare Bil1 varchar(255);
    Declare Bil2 varchar(255);
    Declare STot varchar(255);
    Declare X int;
    Declare Y int;
    Declare Z int;
    Declare Urai varchar(5000);
    SET sString = CAST(v_angka as char);
    SET Urai = '';
    SET X = 0;
    SET Y = 0;
 
    WHILE X <> LENGTH(v_angka) Do
 SET X = X + 1;
 SET sTot = MID(sString, X, 1);
     SET Y = Y + CAST(sTot as unsigned);
     SET Z = LENGTH(sString) - X + 1;
 CASE CAST(sTot as unsigned)
            WHEN 1 THEN
              BEGIN
                 IF (Z = 1 OR Z = 7 OR Z = 10 OR Z = 13) THEN
                  SET Bil1 = 'Satu ';
                 ELSEIF (z = 4) THEN
                  IF (x = 1) THEN
                   SET Bil1 = 'SE';
                  ELSE
                   SET Bil1 = 'Satu';
                  END IF;
                 ELSEIF (Z = 2 OR Z = 5 OR Z = 8 OR Z = 11 OR Z = 14) THEN
                  SET X = X + 1;
                  SET sTot = MID(sString, X, 1);
                  SET Z = LENGTH(sString) - X + 1;
                  SET Bil2 = '';
                    CASE CAST(sTot AS unsigned)
                       WHEN 0 THEN SET Bil1 = 'Sepuluh ';
                       WHEN 1 THEN SET Bil1 = 'Sebelas ';
                       WHEN 2 THEN SET Bil1 = 'Dua Belas ';
                       WHEN 3 THEN SET Bil1 = 'Tiga Belas ';
                       WHEN 4 THEN SET Bil1 = 'Empat Belas ';
                       WHEN 5 THEN SET Bil1 = 'Lima Belas ';
                       WHEN 6 THEN SET Bil1 = 'Enam Belas ';
                       WHEN 7 THEN SET Bil1 = 'Tujuh Belas ';
                       WHEN 8 THEN SET Bil1 = 'Delapan Belas ';
                       WHEN 9 THEN SET Bil1 = 'Sembilan Belas ';
                       ELSE BEGIN END;
                    END CASE;
                ELSE
                    SET Bil1 = 'SE';
                END IF;
            END;
        WHEN 2 THEN SET Bil1 = 'Dua ';
        WHEN 3 THEN SET Bil1 = 'Tiga ';
        WHEN 4 THEN SET Bil1 = 'Empat ';
        WHEN 5 THEN SET Bil1 = 'Lima ';
        WHEN 6 THEN SET Bil1 = 'Enam ';
        WHEN 7 THEN SET Bil1 = 'Tujuh ';
        WHEN 8 THEN SET Bil1 = 'Delapan ';
        WHEN 9 THEN SET Bil1 = 'Sembilan ';
        ELSE SET Bil1 = '';
    END CASE;
 
     IF CAST(sTot as unsigned) > 0 THEN
        IF (Z = 2 OR Z = 5 OR Z = 8 OR Z = 11 OR Z = 14) THEN
            SET Bil2 = 'Puluh ';
            ELSEIF (Z = 3 OR Z = 6 OR Z = 9 OR Z = 12 OR Z = 15) THEN
            SET Bil2 = 'Ratus ';
        ELSE
            SET Bil2 = '';
        END IF;
    ELSE
        SET Bil2 = '';
    END IF;
    IF Y > 0 THEN
        CASE Z
            WHEN 4 THEN BEGIN SET Bil2 = CONCAT(Bil2, 'Ribu '); SET Y = 0; END;
            WHEN 7 THEN BEGIN SET Bil2 = CONCAT(Bil2, 'Juta '); SET Y = 0; END;
            WHEN 10 THEN BEGIN SET Bil2 = CONCAT(Bil2, 'Milyar '); SET Y = 0; END;
            WHEN 13 THEN BEGIN SET Bil2 = CONCAT(Bil2, 'Trilyun '); SET Y = 0; END;
            ELSE BEGIN END;
        END CASE;
    END IF;
    SET Urai = CONCAT(Urai, Bil1, Bil2);
    END WHILE;
   RETURN Urai;
End$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `idarea` int(11) NOT NULL,
  `namaarea` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`idarea`, `namaarea`) VALUES
(1, 'PATUK'),
(3, 'KARANGMOJO'),
(4, 'PONJONG'),
(5, 'PLAYEN'),
(6, 'BANTUL'),
(7, 'KALASAN'),
(10, 'WONOSARI'),
(15, 'BERBAH');

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `idbarang` int(11) NOT NULL,
  `idpelanggan` int(11) DEFAULT NULL,
  `namabarang` varchar(255) DEFAULT NULL,
  `serial` varchar(255) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `hargasatuan` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `tglpembelian` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`idbarang`, `idpelanggan`, `namabarang`, `serial`, `jumlah`, `hargasatuan`, `harga`, `tglpembelian`) VALUES
(1, NULL, 'ROCKET M2', '12F3OP0', 2, NULL, 750000, '2021-02-11'),
(3, NULL, 'TENDA O3', 'T3ND40', 2, NULL, 450000, '2021-02-11'),
(6, 16, 'TENDA O3', 'TEN34', 2, 450000, 900000, '2021-02-12'),
(7, 16, 'MIKROTIK', 'MK345', 2, 750000, 1500000, '2021-02-12'),
(8, 1, 'AP RB950', 'MK90KK', 2, 850000, 1700000, '2021-02-12'),
(9, 1, 'PIPA', '', 2, 250000, 500000, '2021-02-12'),
(10, 2, 'AP TENDA O3', 'TE4O', 2, 400000, 800000, '2021-02-12'),
(11, 2, 'PIPA', '', 2, 200000, 400000, '2021-02-12'),
(12, 4, 'STIK', '', 3, 800000, 2400000, '2021-02-12'),
(14, 10, 'TENDA 03', 'TEN45', 1, 450000, 450000, '2021-02-12'),
(15, 10, 'LHG', 'LH43I', 1, 900000, 900000, '2021-02-12'),
(16, 1, 'spaner', '', 10, 50000, 500000, '2021-02-13');

-- --------------------------------------------------------

--
-- Stand-in structure for view `billing`
-- (See below for the actual view)
--
CREATE TABLE `billing` (
`billing` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `gajian`
--

CREATE TABLE `gajian` (
  `idgaji` int(255) NOT NULL,
  `idkaryawan` int(255) NOT NULL,
  `bulan` int(11) DEFAULT NULL,
  `tahun` int(255) DEFAULT NULL,
  `gajipokok` int(255) DEFAULT NULL,
  `tunjangan` int(255) DEFAULT NULL,
  `potongan` int(255) DEFAULT NULL,
  `gajibersih` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `gajian`
--

INSERT INTO `gajian` (`idgaji`, `idkaryawan`, `bulan`, `tahun`, `gajipokok`, `tunjangan`, `potongan`, `gajibersih`) VALUES
(2, 5, 1, 2021, 1700000, 0, 150000, 1550000),
(3, 2, 1, 2021, 4000000, 0, 350000, 3650000),
(4, 6, 1, 2021, 4500000, 500000, 100000, 4900000),
(6, 29, 1, 2021, 55000, 0, 0, 55000),
(7, 28, 2, 2021, 111000, 0, 0, 111000),
(8, 28, 1, 2021, 111000, 200000, 0, 311000);

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `idjabatan` int(255) NOT NULL,
  `namajabatan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`idjabatan`, `namajabatan`) VALUES
(1, 'Komisaris'),
(2, 'Direktur'),
(3, 'Manager'),
(4, 'Bendahara'),
(5, 'Customer Service'),
(6, 'Admin'),
(7, 'Teknisi'),
(8, 'Sales Freelance'),
(9, 'Sales');

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE `jenis` (
  `idjenis` int(2) NOT NULL,
  `namajenis` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`idjenis`, `namajenis`) VALUES
(1, 'Karyawan'),
(2, 'Sales Freelance');

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `idkaryawan` int(255) NOT NULL,
  `jeniskaryawan` int(11) DEFAULT NULL,
  `namakaryawan` varchar(255) DEFAULT NULL,
  `alamatkaryawan` varchar(255) DEFAULT NULL,
  `notelp` varchar(255) DEFAULT NULL,
  `jabatan` int(255) DEFAULT NULL,
  `pendidikan` int(255) DEFAULT NULL,
  `masakerja` date DEFAULT NULL,
  `gajipokok` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`idkaryawan`, `jeniskaryawan`, `namakaryawan`, `alamatkaryawan`, `notelp`, `jabatan`, `pendidikan`, `masakerja`, `gajipokok`) VALUES
(5, 1, 'Mervia', 'Karangmojo', '083911231', 7, 4, '2021-01-01', 1700000),
(6, 1, 'CATRIN', 'PERUM', '000909', 3, 6, '2019-01-31', 4500000),
(28, 2, 'IVA', 'PATUK', '1234', 2, 4, '2021-02-01', 0),
(29, 2, 'VIAN', 'WONOSARI', '00988', 1, 5, '2021-02-01', 0),
(30, 2, 'ADI', 'PATUK', '9909', 1, 5, '2021-01-01', 0),
(33, 2, 'ARI', 'GIRISUBO', '008822', 8, 2, '2021-02-01', 0),
(34, 1, 'JUMIRAN', 'SENDANG', '9039', 5, 4, '2021-02-01', 3000000);

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `idlevel` int(255) NOT NULL,
  `namalevel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`idlevel`, `namalevel`) VALUES
(1, 'Admin'),
(2, 'Karyawan'),
(3, 'Pelanggan');

-- --------------------------------------------------------

--
-- Stand-in structure for view `nomer`
-- (See below for the actual view)
--
CREATE TABLE `nomer` (
`nomer` char(16)
);

-- --------------------------------------------------------

--
-- Table structure for table `paketharga`
--

CREATE TABLE `paketharga` (
  `idpaket` int(255) NOT NULL,
  `namapaket` varchar(255) DEFAULT NULL,
  `hargapaket` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `paketharga`
--

INSERT INTO `paketharga` (`idpaket`, `namapaket`, `hargapaket`) VALUES
(1, 'UP TO 3 Mbps', 250000),
(2, 'UP TO 5 Mbps', 350000),
(3, 'UP TO 7 Mbps', 500000),
(4, 'UP TO 10 Mbps', 750000),
(5, 'UP TO 15 Mbps', 1000000),
(6, 'PEMASANGAN BARU', 0),
(9, 'COSTUM', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `idpelanggan` int(11) NOT NULL,
  `namapelanggan` varchar(255) DEFAULT NULL,
  `alamatpelanggan` varchar(255) DEFAULT NULL,
  `idarea` int(11) DEFAULT NULL,
  `notelp` char(255) DEFAULT NULL,
  `tgldaftar` date DEFAULT NULL,
  `idpaketharga` int(11) DEFAULT NULL,
  `hargapelanggan` int(255) DEFAULT NULL,
  `sales` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`idpelanggan`, `namapelanggan`, `alamatpelanggan`, `idarea`, `notelp`, `tgldaftar`, `idpaketharga`, `hargapelanggan`, `sales`) VALUES
(1, 'YOSEPHUS', 'KARANGMOJO', 3, '082320003832', '2021-01-13', 1, 250000, 28),
(2, 'NOVIANTO', 'SUDIMORO, KELOR', 3, '082320003832', '2021-01-13', 1, 250000, 28),
(3, 'BAHRUDIN', 'DLINGO', 2, '082320003832', '2021-01-13', 3, 500000, 28),
(4, 'MERVIA', 'MINOMARTANI', 7, '082320003832', '2021-01-14', 9, 1200000, 28),
(10, 'BRIANY', 'TAWARSARI', 10, '09768789', '2021-01-14', 9, 20000, 28),
(13, 'BURHAN', 'PROLIMAN', 4, '123456', '2021-01-15', 4, 750000, 29),
(14, 'ADI', 'PATUK', 1, '12345', '2021-01-18', 2, 350000, 29),
(15, 'BMT ILMI', 'playen', 5, '000000000', '2021-01-18', 2, 350000, 30),
(16, 'VENDI', 'PALIYAN', 5, '0987', '2021-01-18', 6, 10000, NULL),
(17, 'KOI', 'PATUNG', 10, '09887', '2021-01-18', 9, 200000, NULL),
(24, 'PAINO', 'BENDUNGAN', 3, '', '2021-01-19', 1, 250000, NULL),
(26, 'BONICA', 'SERUT', 1, '', '2021-01-19', 2, 350000, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan`
--

CREATE TABLE `pendidikan` (
  `idpendidikan` int(255) NOT NULL,
  `namapendidikan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pendidikan`
--

INSERT INTO `pendidikan` (`idpendidikan`, `namapendidikan`) VALUES
(1, 'SD'),
(2, 'SMP'),
(3, 'SMA/SMK'),
(4, 'D3'),
(5, 'S1'),
(6, 'S2');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `idtransaksi` int(100) NOT NULL,
  `idpelanggan` int(100) DEFAULT NULL,
  `tglpenetapan` date DEFAULT NULL,
  `nosurat` varchar(255) NOT NULL,
  `bulan` int(2) DEFAULT NULL,
  `tahun` int(4) DEFAULT NULL,
  `idbilling` int(255) DEFAULT NULL,
  `idpaketharga` int(255) DEFAULT NULL,
  `harga` int(100) DEFAULT NULL,
  `diskon` int(100) DEFAULT NULL,
  `subharga` int(100) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `tglpembayaran` date DEFAULT NULL,
  `statusbayar` int(1) DEFAULT NULL,
  `usertagihan` int(255) DEFAULT NULL,
  `useredittagihan` int(255) DEFAULT NULL,
  `tgledittagihan` date DEFAULT NULL,
  `tgldelbayar` date DEFAULT NULL,
  `userbayar` int(255) DEFAULT NULL,
  `userdelbayar` int(255) DEFAULT NULL,
  `penambahan` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`idtransaksi`, `idpelanggan`, `tglpenetapan`, `nosurat`, `bulan`, `tahun`, `idbilling`, `idpaketharga`, `harga`, `diskon`, `subharga`, `keterangan`, `tglpembayaran`, `statusbayar`, `usertagihan`, `useredittagihan`, `tgledittagihan`, `tgldelbayar`, `userbayar`, `userdelbayar`, `penambahan`) VALUES
(38, 1, '2021-01-19', '0001/AMC/I/2021', 1, 2021, 210140001, 4, 750000, NULL, 850000, NULL, '2021-01-28', 1, 3, 3, '2021-01-19', '2021-01-19', 3, 3, 100000),
(40, 2, '2021-01-19', '0003/AMC/I/2021', 1, 2021, 210110003, 1, 250000, NULL, 350000, NULL, NULL, NULL, 3, 3, '2021-01-19', NULL, NULL, NULL, 100000),
(41, 2, '2021-01-19', '0003/AMC/I/2021', 2, 2021, 210110004, 1, 250000, NULL, 250000, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 0),
(42, 1, '2021-01-19', '0004/AMC/I/2021', 2, 2021, 210140005, 4, 750000, NULL, 750000, NULL, '2021-02-13', 1, 3, NULL, NULL, NULL, 3, NULL, 0),
(43, 3, '2021-01-19', '0005/AMC/I/2021', 1, 2021, 210130006, 3, 500000, NULL, 500000, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 0),
(44, 3, '2021-01-19', '0006/AMC/I/2021', 2, 2021, 210130007, 3, 500000, NULL, 500000, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 0),
(45, 3, '2021-01-19', '0007/AMC/I/2021', 3, 2021, 210130008, 3, 500000, NULL, 500000, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 0),
(46, 1, '2021-01-19', '0008/AMC/I/2021', 3, 2021, 210140009, 4, 750000, NULL, 750000, NULL, '2021-02-13', 1, 3, NULL, NULL, NULL, 3, NULL, 0),
(47, 2, '2021-01-19', '0009/AMC/I/2021', 3, 2021, 210110010, 1, 250000, NULL, 250000, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 0),
(48, 4, '2021-01-19', '0010/AMC/I/2021', 1, 2021, 210190011, 9, 1200000, NULL, 1200000, NULL, '2021-02-13', 1, 3, NULL, NULL, NULL, 3, NULL, 0),
(52, 24, '2021-01-27', '0011/AMC/I/2021', 2, 2021, 210110012, 1, 250000, NULL, 250000, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 0),
(54, 26, '2021-02-02', '0001/AMC/II/2021', 2, 2021, 210220001, 2, 350000, NULL, 350000, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 0);

--
-- Triggers `transaksi`
--
DELIMITER $$
CREATE TRIGGER `Billing` BEFORE INSERT ON `transaksi` FOR EACH ROW BEGIN
	declare billing char (9);
	DECLARE Y CHAR(2);
	DECLARE M CHAR(2);
	DECLARE Z CHAR(2);
	DECLARE URUT INT;
	DECLARE A INT;
	
	SET M = (SELECT DATE_FORMAT(CURDATE(),'%m'));
	SET Y = (SELECT DATE_FORMAT(CURDATE(),'%yy'));
	SET Z = (new.idpaketharga);
	
	SET A = (SELECT max(idbilling) from transaksi WHERE MONTH(tglpenetapan)=M);
	
	if A is null then
		SET URUT = (SELECT count(idbilling) from transaksi WHERE MONTH(tglpenetapan)=M);
	else
		SET URUT = (SELECT max(SUBSTR(idbilling,6,4)) from transaksi WHERE MONTH(tglpenetapan)=M);
	end if ;
	
	set billing = CONCAT(Y,M,Z,LPAD(URUT+1,4,0));
set new.idbilling=billing;
end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `NOMORSURAT` BEFORE INSERT ON `transaksi` FOR EACH ROW BEGIN
	
	DECLARE NS CHAR(16);
	DECLARE M CHAR(2);
	DECLARE Y CHAR(4);
	DECLARE ROMAWI CHAR(2);
	DECLARE A INT;
	DECLARE URUT INT;
	
	
	SET M = (SELECT DATE_FORMAT(CURDATE(),'%m'));
	SET Y = (SELECT DATE_FORMAT(CURDATE(),'%Y'));
	SET A = (SELECT max(nosurat) from transaksi WHERE MONTH(tglpenetapan)=M);
	
	if A is null then
		SET URUT = (SELECT count(nosurat) from transaksi WHERE MONTH(tglpenetapan)=M);
	else
		SET URUT = (SELECT max(SUBSTR(nosurat,1,4)) from transaksi WHERE MONTH(tglpenetapan)=M);
	end if ;
	
	SET ROMAWI = CASE 
	WHEN M = 01 THEN
		'I'
		WHEN M = 02 THEN
		'II'
		WHEN M = 03 THEN
		'III'
		WHEN M = 04 THEN
		'IV'
		WHEN M = 05 THEN
		'V'
		WHEN M = 06 THEN
		'VI'
		WHEN M = 07 THEN
		'VII'
		WHEN M = 08 THEN
		'VIII'
		WHEN M = 09 THEN
		'IX'
		WHEN M = 10 THEN
		'X'
		WHEN M = 11 THEN
		'XI'
	ELSE
		'XII'
END;
	set NS = CONCAT(LPAD(urut+1,4,0),'/AMC/',ROMAWI,'/',Y);
	
	SET NEW.nosurat=NS;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `idusers` int(100) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `idpelanggan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`idusers`, `username`, `password`, `level`, `nama`, `idpelanggan`) VALUES
(2, 'kadis', '827ccb0eea8a706c4c34a16891f84e7b', 2, 'Yosephus', NULL),
(3, 'admin', '827ccb0eea8a706c4c34a16891f84e7b', 1, 'Ngadimin', NULL),
(4, 'bendahara', '827ccb0eea8a706c4c34a16891f84e7b', 2, 'Bendahara', NULL),
(5, 'yosephus', '827ccb0eea8a706c4c34a16891f84e7b', 3, NULL, 1),
(7, 'bahrudin', '827ccb0eea8a706c4c34a16891f84e7b', 3, NULL, 3),
(8, 'Ietha', 'e807f1fcf82d132f9bb018ca6738a19f', 1, 'NOFRITA', NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_barang`
-- (See below for the actual view)
--
CREATE TABLE `vw_barang` (
`namapelanggan` varchar(255)
,`alamatpelanggan` varchar(255)
,`namaarea` varchar(255)
,`namabarang` varchar(255)
,`idbarang` int(11)
,`idpelanggan` int(11)
,`serial` varchar(255)
,`jumlah` int(11)
,`hargasatuan` int(11)
,`harga` int(11)
,`tglpembelian` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_fee`
-- (See below for the actual view)
--
CREATE TABLE `vw_fee` (
`idkaryawan` int(255)
,`jeniskaryawan` int(11)
,`totalfee` decimal(65,0)
,`gajipokok` decimal(62,0)
,`jumpel` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_gajian`
-- (See below for the actual view)
--
CREATE TABLE `vw_gajian` (
`namakaryawan` varchar(255)
,`bulan` int(11)
,`gajipokok` int(255)
,`idgaji` int(255)
,`tunjangan` int(255)
,`potongan` int(255)
,`gajibersih` int(255)
,`terbilang` varchar(5000)
,`masakerja` date
,`namajabatan` varchar(255)
,`jabatan` int(255)
,`tahun` int(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_grafik`
-- (See below for the actual view)
--
CREATE TABLE `vw_grafik` (
`bulan` int(2)
,`nmbulan` varchar(9)
,`tagihan` decimal(65,0)
,`pembayaran` decimal(65,0)
,`kurangbayar` decimal(65,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_karyawan`
-- (See below for the actual view)
--
CREATE TABLE `vw_karyawan` (
`namapendidikan` varchar(255)
,`idkaryawan` int(255)
,`namakaryawan` varchar(255)
,`alamatkaryawan` varchar(255)
,`notelp` varchar(255)
,`jabatan` int(255)
,`pendidikan` int(255)
,`masakerja` date
,`gajipokok` int(255)
,`namajabatan` varchar(255)
,`jeniskaryawan` int(11)
,`karyawan` varchar(15)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_pelanggan`
-- (See below for the actual view)
--
CREATE TABLE `vw_pelanggan` (
`idpelanggan` int(11)
,`namapelanggan` varchar(255)
,`alamatpelanggan` varchar(255)
,`idarea` int(11)
,`namaarea` varchar(255)
,`notelp` char(255)
,`tgldaftar` date
,`namapaket` varchar(255)
,`hargapaket` int(255)
,`idpaketharga` int(11)
,`sales` int(2)
,`namasales` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_sales`
-- (See below for the actual view)
--
CREATE TABLE `vw_sales` (
`idkaryawan` int(255)
,`namakaryawan` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_selisih`
-- (See below for the actual view)
--
CREATE TABLE `vw_selisih` (
`sumpendapatan` decimal(65,0)
,`sumhargabarang` decimal(32,0)
,`sisa` decimal(65,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_sumbarang`
-- (See below for the actual view)
--
CREATE TABLE `vw_sumbarang` (
`sumhargabarang` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_suminvoice`
-- (See below for the actual view)
--
CREATE TABLE `vw_suminvoice` (
`sumpendapatan` decimal(65,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_tagihan`
-- (See below for the actual view)
--
CREATE TABLE `vw_tagihan` (
`idtransaksi` int(100)
,`idpelanggan` int(11)
,`namapelanggan` varchar(255)
,`alamatpelanggan` varchar(255)
,`namaarea` varchar(255)
,`tglpenetapan` date
,`nosurat` varchar(255)
,`bulan` int(2)
,`nmbulan` varchar(9)
,`tahun` int(4)
,`idbilling` int(255)
,`harga` int(100)
,`penambahan` int(255)
,`subharga` int(100)
,`terbilang` varchar(5000)
,`diskon` int(100)
,`namapaket` varchar(255)
,`tglpembayaran` date
,`statusbayar` int(1)
,`notelp` char(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_users`
-- (See below for the actual view)
--
CREATE TABLE `vw_users` (
`idusers` int(100)
,`username` varchar(255)
,`password` varchar(255)
,`level` int(11)
,`nama` varchar(255)
,`namalevel` varchar(255)
,`namapelanggan` varchar(255)
,`idpelanggan` int(11)
);

-- --------------------------------------------------------

--
-- Structure for view `billing`
--
DROP TABLE IF EXISTS `billing`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `billing`  AS SELECT `Billing`() AS `billing` ;

-- --------------------------------------------------------

--
-- Structure for view `nomer`
--
DROP TABLE IF EXISTS `nomer`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `nomer`  AS SELECT `NOMERSURAT`() AS `nomer` ;

-- --------------------------------------------------------

--
-- Structure for view `vw_barang`
--
DROP TABLE IF EXISTS `vw_barang`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_barang`  AS SELECT `pelanggan`.`namapelanggan` AS `namapelanggan`, `pelanggan`.`alamatpelanggan` AS `alamatpelanggan`, `area`.`namaarea` AS `namaarea`, `barang`.`namabarang` AS `namabarang`, `barang`.`idbarang` AS `idbarang`, `pelanggan`.`idpelanggan` AS `idpelanggan`, `barang`.`serial` AS `serial`, `barang`.`jumlah` AS `jumlah`, `barang`.`hargasatuan` AS `hargasatuan`, `barang`.`harga` AS `harga`, `barang`.`tglpembelian` AS `tglpembelian` FROM ((`barang` join `pelanggan` on(`pelanggan`.`idpelanggan` = `barang`.`idpelanggan`)) join `area` on(`pelanggan`.`idarea` = `area`.`idarea`)) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_fee`
--
DROP TABLE IF EXISTS `vw_fee`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_fee`  AS SELECT `karyawan`.`idkaryawan` AS `idkaryawan`, `karyawan`.`jeniskaryawan` AS `jeniskaryawan`, sum(`pelanggan`.`hargapelanggan`) AS `totalfee`, round(sum(`pelanggan`.`hargapelanggan`) * 5 / 100,0) AS `gajipokok`, count(`pelanggan`.`idpelanggan`) AS `jumpel` FROM (`pelanggan` join `karyawan` on(`pelanggan`.`sales` = `karyawan`.`idkaryawan`)) GROUP BY `karyawan`.`idkaryawan`, `karyawan`.`jeniskaryawan` ;

-- --------------------------------------------------------

--
-- Structure for view `vw_gajian`
--
DROP TABLE IF EXISTS `vw_gajian`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_gajian`  AS SELECT `vw_karyawan`.`namakaryawan` AS `namakaryawan`, `gajian`.`bulan` AS `bulan`, `gajian`.`gajipokok` AS `gajipokok`, `gajian`.`idgaji` AS `idgaji`, `gajian`.`tunjangan` AS `tunjangan`, `gajian`.`potongan` AS `potongan`, `gajian`.`gajibersih` AS `gajibersih`, `terbilang`(`gajian`.`gajibersih`) AS `terbilang`, `vw_karyawan`.`masakerja` AS `masakerja`, `vw_karyawan`.`namajabatan` AS `namajabatan`, `vw_karyawan`.`jabatan` AS `jabatan`, `gajian`.`tahun` AS `tahun` FROM (`gajian` join `vw_karyawan` on(`gajian`.`idkaryawan` = `vw_karyawan`.`idkaryawan`)) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_grafik`
--
DROP TABLE IF EXISTS `vw_grafik`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_grafik`  AS SELECT `a`.`bulan` AS `bulan`, `a`.`nmbulan` AS `nmbulan`, `a`.`tagihan` AS `tagihan`, `b`.`pembayaran` AS `pembayaran`, `c`.`kurangbayar` AS `kurangbayar` FROM (((select `transaksi`.`bulan` AS `bulan`,case when `transaksi`.`bulan` = 1 then 'Januari' when `transaksi`.`bulan` = 2 then 'Februari' when `transaksi`.`bulan` = 3 then 'Maret' when `transaksi`.`bulan` = 4 then 'April' when `transaksi`.`bulan` = 5 then 'Mei' when `transaksi`.`bulan` = 6 then 'Juni' when `transaksi`.`bulan` = 7 then 'Juli' when `transaksi`.`bulan` = 8 then 'Agustus' when `transaksi`.`bulan` = 9 then 'September' when `transaksi`.`bulan` = 10 then 'Oktober' when `transaksi`.`bulan` = 11 then 'November' else 'Desember' end AS `nmbulan`,sum(`transaksi`.`subharga`) AS `tagihan` from `transaksi` where `transaksi`.`tahun` = extract(year from current_timestamp()) group by `transaksi`.`bulan`) `a` left join (select `transaksi`.`bulan` AS `bulan`,case when `transaksi`.`bulan` = 1 then 'Januari' when `transaksi`.`bulan` = 2 then 'Februari' when `transaksi`.`bulan` = 3 then 'Maret' when `transaksi`.`bulan` = 4 then 'April' when `transaksi`.`bulan` = 5 then 'Mei' when `transaksi`.`bulan` = 6 then 'Juni' when `transaksi`.`bulan` = 7 then 'Juli' when `transaksi`.`bulan` = 8 then 'Agustus' when `transaksi`.`bulan` = 9 then 'September' when `transaksi`.`bulan` = 10 then 'Oktober' when `transaksi`.`bulan` = 11 then 'November' else 'Desember' end AS `nmbulan`,sum(`transaksi`.`subharga`) AS `pembayaran` from `transaksi` where `transaksi`.`tahun` = extract(year from current_timestamp()) and `transaksi`.`tglpembayaran` is not null group by `transaksi`.`bulan`) `b` on(`a`.`bulan` = `b`.`bulan`)) left join (select `transaksi`.`bulan` AS `bulan`,case when `transaksi`.`bulan` = 1 then 'Januari' when `transaksi`.`bulan` = 2 then 'Februari' when `transaksi`.`bulan` = 3 then 'Maret' when `transaksi`.`bulan` = 4 then 'April' when `transaksi`.`bulan` = 5 then 'Mei' when `transaksi`.`bulan` = 6 then 'Juni' when `transaksi`.`bulan` = 7 then 'Juli' when `transaksi`.`bulan` = 8 then 'Agustus' when `transaksi`.`bulan` = 9 then 'September' when `transaksi`.`bulan` = 10 then 'Oktober' when `transaksi`.`bulan` = 11 then 'November' else 'Desember' end AS `nmbulan`,sum(`transaksi`.`subharga`) AS `kurangbayar` from `transaksi` where `transaksi`.`tahun` = extract(year from current_timestamp()) and `transaksi`.`tglpembayaran` is null group by `transaksi`.`bulan`) `c` on(`a`.`bulan` = `c`.`bulan`)) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_karyawan`
--
DROP TABLE IF EXISTS `vw_karyawan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_karyawan`  AS SELECT `pendidikan`.`namapendidikan` AS `namapendidikan`, `karyawan`.`idkaryawan` AS `idkaryawan`, `karyawan`.`namakaryawan` AS `namakaryawan`, `karyawan`.`alamatkaryawan` AS `alamatkaryawan`, `karyawan`.`notelp` AS `notelp`, `karyawan`.`jabatan` AS `jabatan`, `karyawan`.`pendidikan` AS `pendidikan`, `karyawan`.`masakerja` AS `masakerja`, `karyawan`.`gajipokok` AS `gajipokok`, `jabatan`.`namajabatan` AS `namajabatan`, `karyawan`.`jeniskaryawan` AS `jeniskaryawan`, CASE WHEN `karyawan`.`jeniskaryawan` = 1 THEN 'Karyawan' WHEN `karyawan`.`jeniskaryawan` = 2 THEN 'Sales Freelance' ELSE '' END AS `karyawan` FROM ((`karyawan` join `jabatan` on(`karyawan`.`jabatan` = `jabatan`.`idjabatan`)) join `pendidikan` on(`karyawan`.`pendidikan` = `pendidikan`.`idpendidikan`)) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_pelanggan`
--
DROP TABLE IF EXISTS `vw_pelanggan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_pelanggan`  AS SELECT `pelanggan`.`idpelanggan` AS `idpelanggan`, `pelanggan`.`namapelanggan` AS `namapelanggan`, `pelanggan`.`alamatpelanggan` AS `alamatpelanggan`, `pelanggan`.`idarea` AS `idarea`, `area`.`namaarea` AS `namaarea`, `pelanggan`.`notelp` AS `notelp`, `pelanggan`.`tgldaftar` AS `tgldaftar`, `paketharga`.`namapaket` AS `namapaket`, `pelanggan`.`hargapelanggan` AS `hargapaket`, `pelanggan`.`idpaketharga` AS `idpaketharga`, `pelanggan`.`sales` AS `sales`, `karyawan`.`namakaryawan` AS `namasales` FROM (((`area` join `pelanggan` on(`pelanggan`.`idarea` = `area`.`idarea`)) left join `paketharga` on(`pelanggan`.`idpaketharga` = `paketharga`.`idpaket`)) left join `karyawan` on(`pelanggan`.`sales` = `karyawan`.`idkaryawan`)) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_sales`
--
DROP TABLE IF EXISTS `vw_sales`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_sales`  AS SELECT `karyawan`.`idkaryawan` AS `idkaryawan`, `karyawan`.`namakaryawan` AS `namakaryawan` FROM `karyawan` WHERE `karyawan`.`jeniskaryawan` = 2 ;

-- --------------------------------------------------------

--
-- Structure for view `vw_selisih`
--
DROP TABLE IF EXISTS `vw_selisih`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_selisih`  AS SELECT `vw_suminvoice`.`sumpendapatan` AS `sumpendapatan`, `vw_sumbarang`.`sumhargabarang` AS `sumhargabarang`, `vw_suminvoice`.`sumpendapatan`- `vw_sumbarang`.`sumhargabarang` AS `sisa` FROM (`vw_sumbarang` join `vw_suminvoice`) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_sumbarang`
--
DROP TABLE IF EXISTS `vw_sumbarang`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_sumbarang`  AS SELECT sum(`barang`.`harga`) AS `sumhargabarang` FROM `barang` ;

-- --------------------------------------------------------

--
-- Structure for view `vw_suminvoice`
--
DROP TABLE IF EXISTS `vw_suminvoice`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_suminvoice`  AS SELECT sum(`transaksi`.`subharga`) AS `sumpendapatan` FROM `transaksi` WHERE `transaksi`.`tglpembayaran` is not null ;

-- --------------------------------------------------------

--
-- Structure for view `vw_tagihan`
--
DROP TABLE IF EXISTS `vw_tagihan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_tagihan`  AS SELECT `transaksi`.`idtransaksi` AS `idtransaksi`, `vw_pelanggan`.`idpelanggan` AS `idpelanggan`, `vw_pelanggan`.`namapelanggan` AS `namapelanggan`, `vw_pelanggan`.`alamatpelanggan` AS `alamatpelanggan`, `vw_pelanggan`.`namaarea` AS `namaarea`, `transaksi`.`tglpenetapan` AS `tglpenetapan`, `transaksi`.`nosurat` AS `nosurat`, `transaksi`.`bulan` AS `bulan`, CASE WHEN `transaksi`.`bulan` = 1 THEN 'Januari' WHEN `transaksi`.`bulan` = 2 THEN 'Februari' WHEN `transaksi`.`bulan` = 3 THEN 'Maret' WHEN `transaksi`.`bulan` = 4 THEN 'April' WHEN `transaksi`.`bulan` = 5 THEN 'Mei' WHEN `transaksi`.`bulan` = 6 THEN 'Juni' WHEN `transaksi`.`bulan` = 7 THEN 'Juli' WHEN `transaksi`.`bulan` = 8 THEN 'Agustus' WHEN `transaksi`.`bulan` = 9 THEN 'September' WHEN `transaksi`.`bulan` = 10 THEN 'Oktober' WHEN `transaksi`.`bulan` = 11 THEN 'November' ELSE 'Desember' END AS `nmbulan`, `transaksi`.`tahun` AS `tahun`, `transaksi`.`idbilling` AS `idbilling`, `transaksi`.`harga` AS `harga`, `transaksi`.`penambahan` AS `penambahan`, `transaksi`.`subharga` AS `subharga`, `terbilang`(`transaksi`.`subharga`) AS `terbilang`, `transaksi`.`diskon` AS `diskon`, `paketharga`.`namapaket` AS `namapaket`, `transaksi`.`tglpembayaran` AS `tglpembayaran`, `transaksi`.`statusbayar` AS `statusbayar`, `vw_pelanggan`.`notelp` AS `notelp` FROM ((`transaksi` join `vw_pelanggan` on(`transaksi`.`idpelanggan` = `vw_pelanggan`.`idpelanggan`)) join `paketharga` on(`paketharga`.`idpaket` = `transaksi`.`idpaketharga`)) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_users`
--
DROP TABLE IF EXISTS `vw_users`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_users`  AS SELECT `users`.`idusers` AS `idusers`, `users`.`username` AS `username`, `users`.`password` AS `password`, `users`.`level` AS `level`, `users`.`nama` AS `nama`, `level`.`namalevel` AS `namalevel`, `pelanggan`.`namapelanggan` AS `namapelanggan`, `users`.`idpelanggan` AS `idpelanggan` FROM ((`level` join `users` on(`level`.`idlevel` = `users`.`level`)) left join `pelanggan` on(`users`.`idpelanggan` = `pelanggan`.`idpelanggan`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`idarea`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`idbarang`);

--
-- Indexes for table `gajian`
--
ALTER TABLE `gajian`
  ADD PRIMARY KEY (`idgaji`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`idjabatan`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`idjenis`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`idkaryawan`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`idlevel`);

--
-- Indexes for table `paketharga`
--
ALTER TABLE `paketharga`
  ADD PRIMARY KEY (`idpaket`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`idpelanggan`);

--
-- Indexes for table `pendidikan`
--
ALTER TABLE `pendidikan`
  ADD PRIMARY KEY (`idpendidikan`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`idtransaksi`,`nosurat`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idusers`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `idarea` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `idbarang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `gajian`
--
ALTER TABLE `gajian`
  MODIFY `idgaji` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `idjabatan` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `idkaryawan` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `idlevel` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `paketharga`
--
ALTER TABLE `paketharga`
  MODIFY `idpaket` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `idpelanggan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `pendidikan`
--
ALTER TABLE `pendidikan`
  MODIFY `idpendidikan` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `idtransaksi` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `idusers` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
